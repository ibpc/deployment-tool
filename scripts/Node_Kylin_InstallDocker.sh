#!/usr/bin/expect

set user
set password
set ip
set port 22

set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "yum -y install docker \r"
sleep 20
expect -re "\[(.*)]:"
send "exit \r"

