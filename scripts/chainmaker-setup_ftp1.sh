#!/usr/bin/expect

set user tll
set password 123456
set ip 192.168.6.109
set port 22
set name chainmaker-v2.3.0-wx-org1.chainmaker.org
set host_ip 192.168.6.111
set ftp_name user
set ftp_password tll_123456
set host_path git_chainmaker/chainmaker/gin_web/src/github.com/myusername/project/chainmaker-go/build/release/
set file_name chainmaker-v2.3.0-wx-org1.chainmaker.org-20230327215715-x86_64.tar.gz
set folder_name chainmaker-v2.3.0-wx-org1.chainmaker.org

set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "ftp $host_ip\r"
expect "Name*" 
send "$ftp_name\r"
expect "Password:" 
send "$ftp_password\r"
expect "ftp>" 
send "cd $host_path\r"
expect "ftp>" 
#interact
send "get $file_name\r"
expect "ftp>" 
send "quit\r"
expect -re "\[(.*)]:" 
send "tar -zxvf $file_name \ncd $name/bin && ./start.sh\r"
expect -re "\[(.*)]:" 
send "exit\r"
