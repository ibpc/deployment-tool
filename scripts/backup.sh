#!/usr/bin/env bash


CURRENT_PATH=$(pwd)
PROJECT_PATH=$(dirname "${CURRENT_PATH}")
BINARY_PATH=${PROJECT_PATH}/binary
BINARY_BUILD_PATH=${PROJECT_PATH}/binary/build


function main() {
    if [ -d $BINARY_BUILD_PATH ]; then
       cd "${BINARY_BUILD_PATH}"
         if [ -d crypto-config ]; then
             mkdir -p backup/backup_certs
             mv crypto-config  backup/backup_certs/crypto-config_$(date "+%Y%m%d%H%M%S")
         fi

         if [ -d config ]; then
              mkdir -p backup/backup_config
             mv config  backup/backup_config/config_$(date "+%Y%m%d%H%M%S")
           fi

         if [ -d release ]; then
              mkdir -p backup/backup_release
             mv release  backup/backup_release/release_$(date "+%Y%m%d%H%M%S")
         fi

       mkdir -p backup/backup_crypto_config_yml
       mkdir -p backup/backup_pkcs11_keys_yml
       mv crypto_config.yml  backup/backup_crypto_config_yml/yml_$(date "+%%Y%%m%%d%%H%%M%%S")
       mv pkcs11_keys.yml  backup/backup_pkcs11_keys_yml/keys_$(date "+%%Y%%m%%d%%H%%M%%S")
    fi
}

main
