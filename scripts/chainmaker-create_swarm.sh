#!/usr/bin/expect

set user
set password
set ip
set port 22
set token
set nodenum
set timeout 3 

spawn scp -rq chainmaker-go/scripts/docker/multi_node/config/node$nodenum $user@$ip:
expect "*password:" 
send "$password\r"
sleep 2
spawn scp -q docker-prepare/docker_image/docker_image.tar $user@$ip:
expect "*password:" 
send "$password\r"
sleep 10
spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "systemctl start docker \r"
sleep 3
expect -re "\[(.*)]:" 
send "docker load -i docker_image.tar\r"
expect -re "\[(.*)]:" 
send "mkdir -p data;cd data;mkdir -p data$nodenum;cd ../\r"
expect -re "\[(.*)]:" 
send "mkdir -p log;cd log;mkdir -p log$nodenum;cd ../\r"
expect -re "\[(.*)]:" 
send "$token\r"
expect -re "\[(.*)]:"
send "hostnamectl set-hostname hostname$nodenum\r"
expect -re "\[(.*)]:" 
send "exit\r"
