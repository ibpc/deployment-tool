#!/usr/bin/expect

set user root
set password 000000
set ip 192.168.206.133
set port 22
set timeout 1

spawn ssh -o StrictHostKeyChecking=no -v -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:"
send "exit\r"