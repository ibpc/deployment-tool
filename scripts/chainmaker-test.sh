export  CGO_ENABLED="1"

cd chainmaker-go/tools/cmc/testdata && rm -rf crypto-config
tar -zxf ../../../build/release/crypto-config-*.tar.gz && cd ..

go build

./cmc client contract user invoke \
--contract-name=T \
--method=P \
--sdk-conf-path=./testdata/sdk_config.yml \
--params="{\"k\":\"company1\",\"v\":\"wx\"}" \
--sync-result=true

./cmc client contract user get \
--contract-name=T \
--method=G \
--sdk-conf-path=./testdata/sdk_config.yml \
--params="{\"k\":\"company1\"}"
