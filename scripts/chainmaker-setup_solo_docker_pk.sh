#!/bin/bash

#set ip 192.168.1.35  
P2P_PORT=11301
  
RPC_PORT=12301
 
NODE_COUNT=4
  
#SERVER_NUM=1


set timeout 3 

cd chainmaker-go
cp -rf build/config scripts/docker/multi_node/
cd scripts/docker/multi_node
#sed -i "s%127.0.0.1%$ip%g" config/node*/chainmaker.yml
./create_solo_docker_compose_yml_pk.sh $P2P_PORT $RPC_PORT $NODE_COUNT ./config 1

