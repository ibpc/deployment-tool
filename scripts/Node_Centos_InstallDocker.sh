#!/usr/bin/expect

set user
set password
set ip
set port 22

set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
#expect -re "\[(.*)]:" 
#send "yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine \r"
expect -re "\[(.*)]:" 
send "yum install -y yum-utils \r"
expect -re "\[(.*)]:" 
send "yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo \r"
expect -re "\[(.*)]:" 
send "yum install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin \r"
sleep 90
expect -re "\[(.*)]:"
send "exit \r"
