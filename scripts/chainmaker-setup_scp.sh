#!/usr/bin/expect

set user
set password
set ip
set port 22
set name
set host_path
set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "rm -rf $name\r"
expect -re "\[(.*)]:" 
send "exit\r"
spawn scp -rq chainmaker-go/build/release/$host_path $user@$ip:$name.tar.gz
expect "*password:" 
send "$password\r"
sleep 10
spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "tar -zxf $name.tar.gz\r"
expect -re "\[(.*)]:" 
send "cd $name/bin \r"
expect -re "\[(.*)]:" 
send "./start.sh\r"
expect -re "\[(.*)]:" 
send "exit\r"
