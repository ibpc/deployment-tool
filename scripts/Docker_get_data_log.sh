#!/usr/bin/expect

set user
set password
set ip
set port 22
set nodenum
set timeout 5 

spawn scp -rq $user@$ip:data/data$nodenum/wx-org$nodenum.chainmaker.org ./chainmaker-go/scripts/docker/multi_node/multi_data/data$nodenum
expect "*password:" 
send "$password\r"
sleep 10
spawn scp -rq $user@$ip:log/log$nodenum/ ./chainmaker-go/scripts/docker/multi_node/multi_log/log$nodenum
expect "*password:" 
send "$password\r"
sleep 5

