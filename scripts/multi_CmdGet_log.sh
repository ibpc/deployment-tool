#!/usr/bin/expect

set user
set password
set ip
set port 22
set nodenum
set timeout 5 

spawn scp -rq $user@$ip:chainmaker-v2.3.*-wx-org$nodenum.chainmaker.org/log ./chainmaker-go/build/release/multi_log/log$nodenum
expect "*password:" 
send "$password\r"
sleep 5

