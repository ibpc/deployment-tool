#!/usr/bin/expect
set timeout 100

set consensus_type 
set log_level 
set hash_type 
set vm_go 
set vm_go_transport_protocol
set vm_go_log_level

cd chainmaker-go/scripts
spawn ./prepare_pk.sh 4 1

expect {
     "consensus type"           { send "$consensus_type\r"; exp_continue }
     "hash type"                { send "$hash_type\r"; exp_continue }
     "vm go transport protocol" { send "$hash_type\r"; exp_continue }
     "vm go log level"          { send "$vm_go_log_level\r" }
     "vm go"                    { send "$vm_go\r"; exp_continue }
     "log level"      { send "$log_level\r"; exp_continue }
}
expect eof
