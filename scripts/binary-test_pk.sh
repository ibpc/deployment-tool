cd binary/tools/cmc/testdata && rm -rf crypto-config
tar -zxf ../../../build/release/crypto-config-*.tar.gz && cd ..

./cmc client contract user invoke \
--contract-name=T \
--method=P \
--sdk-conf-path=./testdata/sdk_config_pk.yml \
--params="{\"k\":\"company1\",\"v\":\"wx\"}" \
--sync-result=true

./cmc client contract user get \
--contract-name=T \
--method=G \
--sdk-conf-path=./testdata/sdk_config_pk.yml \
--params="{\"k\":\"company1\"}"