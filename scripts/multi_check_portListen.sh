#!/usr/bin/expect

set user
set password
set ip
set port 22
set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "netstat -lptn | grep 1230\r"
expect -re "\[(.*)]:" 
send "exit\r"
