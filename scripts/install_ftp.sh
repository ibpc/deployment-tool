#!/bin/bash

#安装vsftpd-------------------------------------------
set cmd yum
set file_path git_chainmaker/chainmaker/gin_web/src/github.com/myusername/project/
set ftp_name user

vsftpd=/etc/vsftpd/vsftpd.conf
rpm -qa | grep "vsftpd"
if [ $? -eq 1 ]; then 
    $cmd install -y vsftpd ftp &> /tmp/vsftpd_install
    if [ $? -eq 0 ]; then 
        echo "vsftpd安装成功!"
        #配置VSFTPD配置文件-------------------------------
        ftp_path=/etc/vsftpd/
        ftp_config=/etc/vsftpd/vsftpd.conf
        cp $ftp_config  ${ftp_path}vsftpd.conf.bak
        sed -i '/anonymous_enable/s/YES/NO/g' $ftp_config
        sed -i '/local_enable/s/NO/YES/g' $ftp_config
        sed -i '19a local_root=/mnt/public/' $ftp_config
        sed -i '20a chroot_local_user=YES' $ftp_config
        sed -i '21a allow_writeable_chroot=YES' $ftp_config
        mkdir -p $file_path
        useradd $ftp_name
        chown $ftp_name  $file_path
        systemctl restart vsftpd
    else 
        echo "vsftpd安装失败,请查看/tmp/vsftpd_install"
    fi
else
    echo "vsftpd已安装!"
fi

