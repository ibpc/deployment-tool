#!/usr/bin/expect
set timeout 100

set P2P_PORT  61111   
set RPC_PORT  61111  
set consensus_type 1         
set log_level "INFO"           
set hash_type "SHA256"         
set vm_go "NO"          
set vm_go_transport_protocol test    
set vm_go_log_level test    
set node_cnt 4     
set chain_cnt 1    

cd chainmaker-go/scripts
spawn ./prepare.sh $node_cnt $chain_cnt $P2P_PORT $RPC_PORT

expect {
     "consensus type" { send "$consensus_type\r"; exp_continue }
     "hash type"      { send "$hash_type\r"; exp_continue }
     "transport protocol" { send "$vm_go_transport_protocol\r"; exp_continue }
     "vm go log level"          { send "$vm_go\r" }
     "vm go"                    { send "$vm_go\r"; exp_continue }
     "log level"      { send "$log_level\r"; exp_continue }
}
expect eof
