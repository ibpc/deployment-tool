#!/usr/bin/expect

set user lin
set password 123456
set ip 192.168.91.128
set port 22
set timeout 1

spawn ssh -o StrictHostKeyChecking=no -v -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:"
send "id\r"
expect -re "\[(.*)]:"
send "exit\r"
