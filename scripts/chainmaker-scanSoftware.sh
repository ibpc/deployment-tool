#!/usr/bin/expect

set user root
set password 000000
set ip 192.168.253.134
set port 22
set timeout 1

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:"
send "ldd --version\r"
expect -re "\[(.*)]:"
send "7za --help\r"
expect -re "\[(.*)]:"
send "docker --version\r"
expect -re "\[(.*)]:"
send "cat /etc/os-release\r"
expect -re "\[(.*)]:"
send "exit\r"