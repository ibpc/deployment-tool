#!/usr/bin/env bash

set -e

CURRENT_PATH=$(pwd)
PROJECT_PATH=$(dirname "${CURRENT_PATH}")
BINARY_PATH=${PROJECT_PATH}/binary
BIN_PATH=${PROJECT_PATH}/binary/bin
CONFIG_PATH=${PROJECT_PATH}/binary/config
MAIN_PATH=${PROJECT_PATH}/binary/main
SCRIPTS_PATH=${PROJECT_PATH}/binary/scripts
SCRIPTS_BIN_PATH=${PROJECT_PATH}/binary/scripts/bin
TOOLS_PATH=${PROJECT_PATH}/binary/tools
TOOLS_CRYPTOGEN_PATH=${TOOLS_PATH}/chainmaker-cryptogen
TOOLS_CRYPTOGEN_BIN_PATH=${TOOLS_PATH}/chainmaker-cryptogen/bin



function package(){

    mkdir -p $BINARY_PATH
    cd $BINARY_PATH

    mkdir $BIN_PATH
    mkdir $CONFIG_PATH
    mkdir $MAIN_PATH
    mkdir $SCRIPTS_PATH
    mkdir $SCRIPTS_BIN_PATH
    mkdir $TOOLS_PATH
    mkdir $TOOLS_CRYPTOGEN_PATH
    mkdir $TOOLS_CRYPTOGEN_BIN_PATH
    tar -zxf $PROJECT_PATH/chainmaker-v*-linux-*64.tar.gz -C $BIN_PATH
    tar -zxf $PROJECT_PATH/chainmaker-cryptogen-v*-linux-*64.tar.gz -C $TOOLS_CRYPTOGEN_BIN_PATH
    cp -r $PROJECT_PATH/chainmaker-go/config/config_tpl $CONFIG_PATH
    cp -r $PROJECT_PATH/chainmaker-go/config/config_tpl_pk  $CONFIG_PATH
    cp -r $PROJECT_PATH/chainmaker-go/config/config_tpl_pwk $CONFIG_PATH
    cp -r $PROJECT_PATH/chainmaker-go/main/libwasmer_runtime_c_api.so   $MAIN_PATH
    cp -r $PROJECT_PATH/chainmaker-go/main/prebuilt/linux/wxdec   $MAIN_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/bin/start.sh  $SCRIPTS_BIN_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/bin/stop.sh  $SCRIPTS_BIN_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/bin/restart.sh  $SCRIPTS_BIN_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/prepare.sh  $SCRIPTS_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/prepare_pk.sh  $SCRIPTS_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/prepare_pwk.sh  $SCRIPTS_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/cluster_quick_start.sh  $SCRIPTS_PATH
    cp $PROJECT_PATH/chainmaker-go/scripts/cluster_quick_stop.sh  $SCRIPTS_PATH
    cp $CURRENT_PATH/build_release.sh    $SCRIPTS_PATH
    cp -r $PROJECT_PATH/chainmaker-go/scripts/service    $SCRIPTS_PATH
    cp -r $PROJECT_PATH/chainmaker-go/tools/cmc          $TOOLS_PATH
    tar -zxf $PROJECT_PATH/cmc-v*-linux-*64.tar.gz -C $TOOLS_PATH/cmc
    cp -r $PROJECT_PATH/chainmaker-cryptogen/config $TOOLS_CRYPTOGEN_PATH
    
    chmod 777 $BINARY_PATH
    echo "wait build..."
    wait

}

package