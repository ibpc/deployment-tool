#!/usr/bin/expect

set user root
set password 000000
set ip 192.168.206.133
set port 22
set timeout 1

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:"
send "cat /proc/cpuinfo |grep 'model name'|uniq\r"
expect -re "\[(.*)]:"
send "cat /proc/cpuinfo |grep processor|sort -u|wc -l\r"
expect -re "\[(.*)]:"
send "cat /proc/cpuinfo |grep MHz|uniq && lscpu |grep MHz:\r"
expect -re "\[(.*)]:"
send "cat /proc/meminfo |grep MemTotal\r"
expect -re "\[(.*)]:" 
send "df -m | sed 1d | awk '{sum += \$4} END {print sum/1024}'\r"
expect -re "\[(.*)]:"
send "ulimit -n 100010 && ulimit -n\r"
expect -re "\[(.*)]:"
send "ps -ef | wc -l\r"
expect -re "\[(.*)]:"
send "sysctl kernel.pid_max\r"
expect -re "\[(.*)]:"
send "cat /etc/os-release\r"
expect -re "\[(.*)]:"
send "exit\r"