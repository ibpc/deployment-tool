#!/usr/bin/expect

set user
set password
set ip
set port 22
set num
set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "docker swarm leave\r"
expect -re "\[(.*)]:" 
send "rm -rf data\r"
expect -re "\[(.*)]:" 
send "rm -rf log\r"
expect -re "\[(.*)]:" 
send "rm -rf node$num\r"
expect -re "\[(.*)]:" 
send "exit\r"
