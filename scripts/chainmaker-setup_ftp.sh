#!/usr/bin/expect

set user
set password
set ip
set port 22
set host_ip
set ftp_name
set ftp_password
set file_path
set file_name
set folder_name

set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "ftp $host_ip\r"
expect "Name*" 
send "$ftp_name\r"
expect "Password:" 
send "$ftp_password\r"
expect "ftp>" 
send "cd $file_path\r"
expect "ftp>" 
#interact
send "get $file_name\r"
expect "ftp>" 
send "quit\r"
expect -re "\[(.*)]:" 
send "tar -zxvf $file_name \r"
expect -re "\[(.*)]:" 
send "cd $folder_name/bin \r"
expect -re "\[(.*)]:" 
send "./start.sh\r"
expect -re "\[(.*)]:" 
send "exit\r"
