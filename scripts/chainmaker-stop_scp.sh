#!/usr/bin/expect

set user
set password
set ip
set port 22
set name
set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "cd $name/bin \r"
expect -re "\[(.*)]:" 
send "./stop.sh\r"
#expect -re "\[(.*)]:" 
#send "cd ../../ \r"
#expect -re "\[(.*)]:" 
#send "rm -rf $name \r"
#expect -re "\[(.*)]:" 
#send "rm -rf $name.tar.gz \r"
expect -re "\[(.*)]:" 
send "exit\r"
