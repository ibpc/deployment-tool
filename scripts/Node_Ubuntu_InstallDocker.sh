#!/usr/bin/expect

set user
set password
set ip
set port 22

set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
#send "for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done \r"
#expect -re "\[(.*)]:" 
#send "apt-get update \r"
#sleep 30
#expect -re "\[(.*)]:" 
#send "rm /var/cache/apt/archives/lock \r"
#expect -re "\[(.*)]:" 
#send "rm /var/lib/dpkg/lock-frontend \r"
#expect -re "\[(.*)]:" 
#send "apt-get install -y ca-certificates curl gnupg \r"
#expect -re "\[(.*)]:" 
#send "install -m 0755 -d /etc/apt/keyrings \r"
#expect -re "\[(.*)]:" 
#send "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg \r"
#expect -re "\[(.*)]:" 
#send "chmod a+r /etc/apt/keyrings/docker.gpg \r"
#expect -re "\[(.*)]:" 
#send `echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
#			"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
#			sudo tee /etc/apt/sources.list.d/docker.list > /dev/null \r`
#expect -re "\[(.*)]:" 
#send "apt-get update \r"
#expect -re "\[(.*)]:" 
send "apt-get -y install docker.io \r"
sleep 20
expect -re "\[(.*)]:"
send "exit \r"

