#!/usr/bin/expect

set user
set password
set ip
set port 22
set timeout 3 

spawn ssh -o StrictHostKeyChecking=no -p $port $user@$ip
expect "*password:" 
send "$password\r"
expect -re "\[(.*)]:" 
send "ps -ef|grep chainmaker | grep -v grep\r"
expect -re "\[(.*)]:" 
send "exit\r"
