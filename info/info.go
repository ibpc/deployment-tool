package info

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/ini.v1"
)

type InitMessage struct {
	OKFlag    int    // 安装完成标志位, 1表示安装完成
	ErrorFlag int    // 安装出错标志位，1表示安装出错
	Log       string // 安装日志
}

// 结构体定义:系统硬件配置最低要求
type HardwareinfoMin struct {
	CpuFreqMin  float64 // CPU主频最低要求
	MemSizeMin  int64   // 内存大小最低要求
	DiskSizeMin float64 // 硬盘大小最低要求
	HandleMin   int64   // 句柄数最低要求
	ProcessMin  int64   // 进程数最低要求
}

// 结构体定义:一个节点的硬件配置信息
type Hardwareinfo struct {
	// 扫描到的系统硬件配置信息
	IpAddr       string
	ConnectFlag	 int
	OsInfo       string
	CpuName      string
	ProcessorNum int64
	CpuFreq      float64
	MemInfo      string
	MemSize      int64
	DiskSize     float64
	HandleNum    int64
	ProcessNum   int64
	ProcessMax   int64

	// 系统硬件配置最低要求
	HardwareMin HardwareinfoMin

	// 系统硬件配置是否满足最低要求，1表示满足，0表示不满足
	CpuFlag     int
	MemFlag     int
	DiskFlag    int
	HandleFlag  int
	ProcessFlag int

	// 系统硬件配置扫描结果
	CpuResult     string
	MemResult     string
	DiskResult    string
	HandleResult  string
	ProcessResult string
}

// 结构体定义:所有节点的系统硬件配置信息
type Hardware struct {
	// 主节点系统硬件配置信息
	HostHardware Hardwareinfo


	// 各子节点系统硬件配置信息
	NodeNum      int
	NodeHardware []Hardwareinfo
}

// 结构体定义:软件依赖版本最低要求
type OssoftinfoMin struct {
	GitVersionMin           string
	GccVersionMin           string
	GoVersionMin            string
	GoVersionMax            string
	GlibcVersionMin         string
	DockerVersionMin        string
	DockerComposeVersionMin string
}

// 结构体定义:一个节点的软件依赖信息
type Ossoftinfo struct {
	// 扫描到的软件依赖版本

	ConnectFlag			 int
	IpAddr               string
	GitVersion           string
	GccVersion           string
	GoVersion            string
	GlibcVersion         string
	DockerVersion        string
	DockerComposeVersion string
	SevenZVersion        string
	TreeVersion          string
	ExpectVersion        string
	ChainVersion         string

	// 软件依赖版本最低要求
	OssoftMin OssoftinfoMin

	// 软件依赖版本是否满足最低要求，1表示满足，0表示不满足
	GitFlag           int
	GccFlag           int
	GoFlag            int
	GlibcFlag         int
	DockerFlag        int
	DockerComposeFlag int
	SevenZFlag        int

	// 软件依赖版本扫描结果
	GitResult           string
	GccResult           string
	GoResult            string
	GlibcResult         string
	DockerResult        string
	DockerComposeResult string
	SevenZResult        string
}

// 结构体定义:所有节点的软件依赖信息
type Software struct {
	// 主节点系统软件信息
	HostSoftware Ossoftinfo

	// 各子节点系统硬件信息
	NodeNum      int
	NodeSoftware []Ossoftinfo
}

// 结构体定义:用户配置信息信息
type Userconfig struct {
	// 长安链版本号、git下载所需的用户名/密码
	ChainmakerVersion  string
	ChainmakerUsername string
	ChainmakerPassword string

	// 主节点的IP、port
	HostIpAddr string
	//HostDockerIp string
	HostP2PPort string
	HostRPCPort string

	// 主节点root密码，主要用于下载、安装依赖
	HostRootPassword string

	// 主节点FTP登录用户名/密码，FTP目录
	FtpUserName string
	FtpPassword string
	FtpPath     string

	// 子节点数量，子节点配置信息
	NodeNum    int
	NodeIpAddr [16]string
	//NodeDockerIp [16]string
	NodeP2PPort  [16]string
	NodeRPCPort  [16]string
	NodeFile     [16]string
	NodeFilePath [16]string // client节点的安装包路径
	NodeUser     [16]string
	NodePassword [16]string

	// 子节点选择信息
	NodeNum1    int
	NodeIpAddr1   [16]string
	NodeP2PPort1  [16]string
	NodeRPCPort1  [16]string
	NodeFile1     [16]string
	NodeFilePath1 [16]string // client节点的安装包路径
	NodeUser1     [16]string
	NodePassword1 [16]string

	DeploymentType         int    // DeploymentType, 0:命令行部署， 1:docker部署， 2：二进制部署
	CopyType               int    // 多点部署时拷贝方式, 0:scp方式， 1:ftp方式
	Chainmaker_explorer    int    // 区块链浏览器, 0:不启动， 1:启动
	Way_start_explore      int    // 部署浏览器的docker启动方式,0：常规启动（快速）, 1:增强evm参数解析启动（慢)
	Block_height           string // 块高度
	Block_size             string // 块大小
	RecommendedConfigLevel int    //0、1、2、3分别表示低、默认、较高、高配置

	// prepare.sh 所需的参数
	IdentityMode int // 0:PermissionWithCert模式, 1:Public模式 2:PermissionWithKey模式
	Node_cnt     string
	Chain_cnt    string
	//Server_node_cnt          string
	Consensus_type           string
	Log_level                string
	Hash_type                string
	Vm_go                    string
	Vm_go_transport_protocol string
	Vm_go_log_level          string
}

// 结构体定义:长安链推荐配置参数，对应RecommendedConfigLevel的4个级别
type RecommendedConfig struct {
	BlockTxCapacity  [4]string // 区块交易容量
	MaxTxpoolSize    [4]string // 交易池大小
	BlockInterval    [4]string // 出块间隔(ms)
	LogdbSegmentSize [4]string // 文件大小(Mb)
	HardMaxCacheSize [4]string // 缓存状态数据库大小(Mb)
}

// 结构体定义:子节点配置信息
type Node struct {
	NodeIpAddr string `json:"NodeIpAddr"`
	//NodeDockerIp string `json:"node_docker_ip"`
	NodeP2PPort  string `json:"NodeP2PPort"`
	NodeRPCPort  string `json:"NodeRPCPort"`
	NodeFile     string `json:"node_file"`
	NodeFilePath string `json:"node_file_path"` // client节点的安装包路径
	NodeUser     string `json:"NodeUser"`
	NodePassword string `json:"NodePassword"`

	// 子节点选择信息
	NodeIpAddr1   string
	NodeP2PPort1  string
	NodeRPCPort1  string
	NodeFile1     string
	NodeFilePath1 string // client节点的安装包路径
	NodeUser1     string
	NodePassword1 string
}

// 结构体定义:用户配置信息
type Userconfig_json struct {
	ChainmakerVersion  string `json:"ChainmakerVersion"`
	ChainmakerUsername string `json:"ChainmakerUsername"`
	ChainmakerPassword string `json:"ChainmakerPassword"`

	HostIpAddr string `json:"HostIpAddr"`
	//HostDockerIp     string     `json:"HostDockerIp"`
	HostUser         string     `json:"HostUser"`
	HostP2PPort      string     `json:"HostP2PPort"`
	HostRPCPort      string     `json:"HostRPCPort"`
	HostRootPassword string     `json:"HostRootPassword"`
	HostFile         [16]string `json:"host_file"`
	HostFilePath     [16]string `json:"host_file_path"` // 主节点的安装包路径
	FtpUserName      string     `json:"FtpUserName"`
	FtpPassword      string     `json:"FtpPassword"`
	FtpPath          string     `json:"FtpPath"`

	// DeploymentTypeStr      string `json:"DeploymentType"`
	DeploymentType int `json:"DeploymentType"`
	// CopyTypeStr            string `json:"CopyType"`
	CopyType int `json:"CopyType"`
	// IdentityModeStr        string `json:"identityMode"`
	IdentityMode int `json:"identityMode"`
	// Chainmaker_explorerStr string `json:"Chainmaker_explorer"`
	Chainmaker_explorer int `json:"Chainmaker_explorer"`
	// Way_start_exploreStr   string `json:"Way_start_explore"`
	Way_start_explore int `json:"Way_start_explore"`

	Consensus_type string `json:"consensus_type"`
	Log_level      string `json:"Log_level"`
	Hash_type      string `json:"Hash_type"`
	Vm_go          string `json:"Vm_go"`
	Node_cnt       string `json:"Node_cnt"`
	Chain_cnt      string `json:"Chain_cnt"`
	//Server_node_cnt          string `json:"Server_node_cnt"`
	Vm_go_transport_protocol string `json:"Vm_go_transport_protocol"`
	Vm_go_log_level          string `json:"Vm_go_log_level"`

	Block_height string `json:"Block_height"`
	Block_size   string `json:"Block_size"`
	// RecommendedConfigLevelStr string `json:"recommended_config_level"`
	RecommendedConfigLevel int `json:"recommended_config_level"`

	NodeNum int      `json:"node_num"`
	Nodes   [16]Node `json:"nodes"`

	Development_type string `json:"development_type"`
}

// 结构体定义:用户配置信息 StateSecret_PostHeader
type StateSecret_PostHeader struct {
	Config Userconfig_json `json:"form"`
}

// 结构体定义:子节点配置信息
type NodeConfig struct {
	IpAddr      string `json:"IpAddr"`
	NodeP2PPort string `json:"NodeP2PPort"`
	NodeRPCPort string `json:"NodeRPCPort"`
	User        string `json:"User"`
	Password    string `json:"Password"`
}

// 结构体定义:用户配置信息 NodeDetail_PostHeader
type NodeDetail_PostHeader struct {
	Type  string         `json:"type"`
	Host  NodeConfig     `json:"singleInfo"`
	Nodes [16]NodeConfig `json:"multiInfo"`
}

// 结构体定义：用户选择信息 
type NodeSelectConfig struct {
	IpAddr	string `json:"IpAddr"`
	Id		string `json:"id"`
}

// 结构体定义:用户配置信息 NodeSelect_PostHeader
type NodeSelect_PostHeader struct {
	Nodes [16]NodeSelectConfig	`json:"Info"`
}

//结构体定义：二进制文件下载地址
type Html_url struct {
	// html_url
	Chainmakerv0    string `json:"chainmaker-v2.3.0-linux-x86_64.tar.gz"`
	Chainmakerv0Arm string `json:"chainmaker-v2.3.0-linux-arm64.tar.gz"`
	Chainmakerv1    string `json:"chainmaker-v2.3.1-linux-x86_64.tar.gz"`
	Chainmakerv1Arm string `json:"chainmaker-v2.3.1-linux-arm64.tar.gz"`
	Chainmakerv2    string `json:"chainmaker-v2.3.2-linux-x86_64.tar.gz"`
	Chainmakerv2Arm string `json:"chainmaker-v2.3.2-linux-arm64.tar.gz"`

	Cmcv0    string `json:"cmc-v2.3.0-linux-x86_64.tar.gz"`
	Cmcv0Arm string `json:"cmc-v2.3.0-linux-arm64.tar.gz"`
	Cmcv1    string `json:"cmc-v2.3.1-linux-x86_64.tar.gz"`
	Cmcv1Arm string `json:"cmc-v2.3.1-linux-arm64.tar.gz"`
	Cmcv2    string `json:"cmc-v2.3.2-linux-x86_64.tar.gz"`
	Cmcv2Arm string `json:"cmc-v2.3.2-linux-arm64.tar.gz"`

	Cryptogen    string `json:"chainmaker-cryptogen-v2.3.0-linux-x86_64.tar.gz"`
	CryptogenArm string `json:"chainmaker-cryptogen-v2.3.0-linux-arm64.tar.gz"`
}

// 函数：静态配置系统硬件配置最低要求
func GetHardwareMin() HardwareinfoMin {
	var Hardware HardwareinfoMin
	Hardware.CpuFreqMin = 800     // 800Mhz
	Hardware.MemSizeMin = 4000000 // 4000000KB
	Hardware.DiskSizeMin = 10     // 10GB
	Hardware.HandleMin = 100      // 句柄数最小100
	Hardware.ProcessMin = 100     // 进程数最小100
	return Hardware
}

// 函数：静态配置软件依赖版本最低要求
func GetSoftMin(Ossoft *OssoftinfoMin) {
	Ossoft.GitVersionMin = "1.0.0"
	Ossoft.GccVersionMin = "7.3"
	Ossoft.GoVersionMin = "1.16"
	//1.18
	Ossoft.GoVersionMax = "1.20"
	Ossoft.GlibcVersionMin = "2.17"
	Ossoft.DockerVersionMin = "20.10.7"
	Ossoft.DockerComposeVersionMin = "1.29.2"
}

// 函数：读取config.ini文件，返回最低硬件配置
func GetHardwareMinFromIni(ChainmakerVersion string) HardwareinfoMin {
	cfg, err := ini.Load("config.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	section_name := fmt.Sprintf("%s", ChainmakerVersion)

	var Hardware HardwareinfoMin
	Hardware.CpuFreqMin, err = cfg.Section(section_name).Key("CpuFreqMin").Float64()
	Hardware.MemSizeMin, err = cfg.Section(section_name).Key("MemSizeMin").Int64()
	Hardware.DiskSizeMin, err = cfg.Section(section_name).Key("DiskSizeMin").Float64()
	Hardware.HandleMin, err = cfg.Section(section_name).Key("HandleMin").Int64()
	Hardware.ProcessMin, err = cfg.Section(section_name).Key("ProcessMin").Int64()

	return Hardware
}

// 函数：读取config.ini文件，返回最低软件配置
func GetSoftMinFromIni(ChainmakerVersion string) OssoftinfoMin {
	cfg, err := ini.Load("config.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	section_name := fmt.Sprintf("%s", ChainmakerVersion)

	var Ossoft OssoftinfoMin
	Ossoft.GitVersionMin = cfg.Section(section_name).Key("GitVersionMin").String()
	Ossoft.GccVersionMin = cfg.Section(section_name).Key("GccVersionMin").String()
	Ossoft.GoVersionMin = cfg.Section(section_name).Key("GoVersionMin").String()
	Ossoft.GoVersionMax = cfg.Section(section_name).Key("GoVersionMax").String()
	Ossoft.GlibcVersionMin = cfg.Section(section_name).Key("GlibcVersionMin").String()
	Ossoft.DockerVersionMin = cfg.Section(section_name).Key("DockerVersionMin").String()
	Ossoft.DockerComposeVersionMin = cfg.Section(section_name).Key("DockerComposeVersionMin").String()
	return Ossoft
}

// 函数：读取config.ini文件，返回长安链推荐配置
func GetRecommendedConfigFromIni() RecommendedConfig {
	cfg, err := ini.Load("config.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	var RecommendedConfig RecommendedConfig
	for i := 0; i < 4; i++ {
		section_name := fmt.Sprintf("RecommendedConfigLevel%d", i)
		RecommendedConfig.BlockTxCapacity[i] = cfg.Section(section_name).Key("BlockTxCapacity").String()
		RecommendedConfig.MaxTxpoolSize[i] = cfg.Section(section_name).Key("MaxTxpoolSize").String()
		RecommendedConfig.BlockInterval[i] = cfg.Section(section_name).Key("BlockInterval").String()
		RecommendedConfig.LogdbSegmentSize[i] = cfg.Section(section_name).Key("LogdbSegmentSize").String()
		RecommendedConfig.HardMaxCacheSize[i] = cfg.Section(section_name).Key("HardMaxCacheSize").String()
	}
	return RecommendedConfig
}

// 函数：读取user_config.ini文件，返回用户配置信息
func GetUserconfigFromIni() Userconfig_json {
	// 打开user_config.ini文件
	cfg, err := ini.Load("user_config.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	var Userconfig Userconfig_json
	// 读取配置信息
	Userconfig.ChainmakerVersion = cfg.Section("").Key("ChainmakerVersion").String()
	Userconfig.ChainmakerUsername = cfg.Section("").Key("ChainmakerUsername").String()
	Userconfig.ChainmakerPassword = cfg.Section("").Key("ChainmakerPassword").String()

	Userconfig.HostIpAddr = cfg.Section("").Key("HostIpAddr").String()
	//Userconfig.HostDockerIp = cfg.Section("").Key("HostDockerIp").String()
	Userconfig.HostP2PPort = cfg.Section("").Key("HostP2PPort").String()
	Userconfig.HostRPCPort = cfg.Section("").Key("HostRPCPort").String()
	Userconfig.HostRootPassword = cfg.Section("").Key("HostRootPassword").String()
	Userconfig.FtpUserName = cfg.Section("").Key("FtpUserName").String()
	Userconfig.FtpPassword = cfg.Section("").Key("FtpPassword").String()
	Userconfig.FtpPath = cfg.Section("").Key("FtpPath").String()

	Userconfig.NodeNum, err = cfg.Section("").Key("NodeNum").Int()
	for i := 0; i < Userconfig.NodeNum; i++ {
		section_name := fmt.Sprintf("Node%d", i)
		Userconfig.Nodes[i].NodeIpAddr = cfg.Section(section_name).Key("NodeIpAddr").String()
		//Userconfig.Nodes[i].NodeDockerIp = cfg.Section(section_name).Key("NodeDockerIp").String()
		Userconfig.Nodes[i].NodeP2PPort = cfg.Section(section_name).Key("NodeP2PPort").String()
		Userconfig.Nodes[i].NodeRPCPort = cfg.Section(section_name).Key("NodeRPCPort").String()
		Userconfig.Nodes[i].NodeUser = cfg.Section(section_name).Key("NodeUser").String()
		Userconfig.Nodes[i].NodePassword = cfg.Section(section_name).Key("NodePassword").String()
	}

	// 读取DeploymentType，支持类型：0:命令行部署， 1:docker部署, 2:二进制部署, 3:离线部署
	Userconfig.DeploymentType, err = cfg.Section("").Key("DeploymentType").Int()
	if Userconfig.DeploymentType > 3 {
		fmt.Printf("DeploymentType error: %d", Userconfig.DeploymentType)
		os.Exit(1)
	}

	// 读取CopyType，支持类型：0:scp方式， 1:ftp方式
	Userconfig.CopyType, err = cfg.Section("").Key("CopyType").Int()
	if Userconfig.CopyType > 1 {
		fmt.Printf("CopyType error: %d", Userconfig.CopyType)
		os.Exit(1)
	}

	// 读取IdentityMode，支持类型：0:PermissionWithCert模式, 1:Public模式 2:PermissionWithKey模式
	Userconfig.IdentityMode, err = cfg.Section("").Key("IdentityMode").Int()
	if Userconfig.IdentityMode > 2 {
		fmt.Printf("IdentityMode error: %d", Userconfig.IdentityMode)
		os.Exit(1)
	}

	// 读取Chainmaker_explorer，支持类型：0:不启动， 1:启动
	Userconfig.Chainmaker_explorer, err = cfg.Section("").Key("Chainmaker_explorer").Int()
	if Userconfig.Chainmaker_explorer > 1 {
		fmt.Printf("Chainmaker_explorer error: %d", Userconfig.Chainmaker_explorer)
		os.Exit(1)
	}

	// 读取Way_start_explore ，支持类型：0：常规启动（快速）, 1:增强evm参数解析启动（慢）
	Userconfig.Way_start_explore, err = cfg.Section("").Key("Way_start_explore ").Int()
	if Userconfig.Way_start_explore > 1 {
		fmt.Printf("Way_start_explore  error: %d", Userconfig.Way_start_explore)
		os.Exit(1)
	}
	Userconfig.Consensus_type = cfg.Section("").Key("Consensus_type").String()
	Userconfig.Log_level = cfg.Section("").Key("Log_level").String()
	Userconfig.Hash_type = cfg.Section("").Key("Hash_type").String()
	Userconfig.Vm_go = cfg.Section("").Key("Vm_go").String()
	Userconfig.Vm_go_transport_protocol = cfg.Section("").Key("Vm_go_transport_protocol").String()
	Userconfig.Vm_go_log_level = cfg.Section("").Key("Vm_go_log_level").String()
	Userconfig.Node_cnt = cfg.Section("").Key("Node_cnt").String()
	Userconfig.Chain_cnt = cfg.Section("").Key("Chain_cnt").String()
	//Userconfig.Server_node_cnt = cfg.Section("").Key("Server_node_cnt").String()
	Userconfig.Block_height = cfg.Section("").Key("Block_height").String()
	Userconfig.Block_size = cfg.Section("").Key("Block_size").String()
	Userconfig.RecommendedConfigLevel, err = cfg.Section("").Key("Recommended_config_level").Int()
	return Userconfig
}

// 函数：读取release-linux-all.json文件，读取二进制文件下载地址
func GetUrlFromJson() Html_url {
	jsonFile, err := os.Open("release-linux-all.json")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened release-linux-all.json")
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var Url Html_url

	json.Unmarshal(byteValue, &Url)

	fmt.Printf("byteValue: %s \n", byteValue)

	fmt.Printf("chainmaker-v2.3.0-linux-x86_64.tar.gz: %s \n", Url.Chainmakerv0)
	fmt.Printf("chainmaker-v2.3.0-linux-arm64.tar.gz: %s \n", Url.Chainmakerv0Arm)
	fmt.Printf("chainmaker-v2.3.1-linux-x86_64.tar.gz: %s \n", Url.Chainmakerv1)
	fmt.Printf("chainmaker-v2.3.1-linux-arm64.tar.gz: %s \n", Url.Chainmakerv1Arm)
	fmt.Printf("chainmaker-v2.3.2-linux-x86_64.tar.gz: %s \n", Url.Chainmakerv2)
	fmt.Printf("chainmaker-v2.3.2-linux-arm64.tar.gz: %s \n", Url.Chainmakerv2Arm)

	fmt.Printf("cmc-v2.3.0-linux-x86_64.tar.gz: %s \n", Url.Cmcv0)
	fmt.Printf("cmc-v2.3.0-linux-arm64.tar.gz: %s \n", Url.Cmcv0Arm)
	fmt.Printf("cmc-v2.3.1-linux-x86_64.tar.gz: %s \n", Url.Cmcv1)
	fmt.Printf("cmc-v2.3.1-linux-arm64.tar.gz: %s \n", Url.Cmcv1Arm)
	fmt.Printf("cmc-v2.3.2-linux-x86_64.tar.gz: %s \n", Url.Cmcv2)
	fmt.Printf("cmc-v2.3.2-linux-arm64.tar.gz: %s \n", Url.Cmcv2Arm)

	fmt.Printf("chainmaker-cryptogen-v2.3.0-linux-x86_64.tar.gz: %s \n", Url.Cryptogen)
	fmt.Printf("chainmaker-cryptogen-v2.3.0-linux-arm64.tar.gz: %s \n", Url.CryptogenArm)

	return Url
}

// 函数：根据json格式信息，返回用户配置信息
func GetUserconfigFromMap(data map[string]interface{}) Userconfig_json {
	var UserConfig1 Userconfig_json

	if data["ChainmakerVersion"] != nil {
		UserConfig1.ChainmakerVersion = data["ChainmakerVersion"].(string)
	}
	if data["ChainmakerUsername"] != nil {
		UserConfig1.ChainmakerUsername = data["ChainmakerUsername"].(string)
	}
	if data["ChainmakerPassword"] != nil {
		UserConfig1.ChainmakerPassword = data["ChainmakerPassword"].(string)
	}
	if data["HostIpAddr"] != nil {
		UserConfig1.HostIpAddr = data["HostIpAddr"].(string)
	}
	//if data["HostDockerIp"] != nil {
	//	UserConfig1.HostDockerIp = data["HostDockerIp"].(string)
	//}
	if data["HostP2PPort"] != nil {
		UserConfig1.HostP2PPort = data["HostP2PPort"].(string)
	}
	if data["HostRPCPort"] != nil {
		UserConfig1.HostRPCPort = data["HostRPCPort"].(string)
	}
	if data["HostRootPassword"] != nil {
		UserConfig1.HostRootPassword = data["HostRootPassword"].(string)
	}
	if data["FtpUserName"] != nil {
		UserConfig1.FtpUserName = data["FtpUserName"].(string)
	}
	if data["FtpPassword"] != nil {
		UserConfig1.FtpPassword = data["FtpPassword"].(string)
	}
	if data["FtpPath"] != nil {
		UserConfig1.FtpPath = data["FtpPath"].(string)
	}
	if data["DeploymentType"] != nil {
		UserConfig1.DeploymentType = data["DeploymentType"].(int)
	}
	if data["CopyType"] != nil {
		UserConfig1.CopyType = data["CopyType"].(int)
	}
	if data["IdentityMode"] != nil {
		UserConfig1.IdentityMode = data["IdentityMode"].(int)
	}
	if data["Chainmaker_explorer"] != nil {
		UserConfig1.Chainmaker_explorer = data["Chainmaker_explorer"].(int)
	}
	if data["Way_start_explore"] != nil {
		UserConfig1.Way_start_explore = data["Way_start_explore"].(int)
	}
	if data["Consensus_type"] != nil {
		UserConfig1.Consensus_type = data["Consensus_type"].(string)
	}
	if data["Log_level"] != nil {
		UserConfig1.Log_level = data["Log_level"].(string)
	}
	if data["Hash_type"] != nil {
		UserConfig1.Hash_type = data["Hash_type"].(string)
	}
	if data["Vm_go"] != nil {
		UserConfig1.Vm_go = data["Vm_go"].(string)
	}
	if data["Node_cnt"] != nil {
		UserConfig1.Node_cnt = data["Node_cnt"].(string)
	}
	if data["Chain_cnt"] != nil {
		UserConfig1.Chain_cnt = data["Chain_cnt"].(string)
	}
	//if data["Server_node_cnt"] != nil {
	//	UserConfig1.Server_node_cnt = data["Server_node_cnt"].(string)
	//}
	if data["Vm_go_transport_protocol"] != nil {
		UserConfig1.Vm_go_transport_protocol = data["Vm_go_transport_protocol"].(string)
	}
	if data["Vm_go_log_level"] != nil {
		UserConfig1.Vm_go_log_level = data["Vm_go_log_level"].(string)
	}
	if data["Block_height"] != nil {
		UserConfig1.Block_height = data["Block_height"].(string)
	}
	if data["Block_size"] != nil {
		UserConfig1.Block_size = data["Block_size"].(string)
	}
	if data["RecommendedConfigLevel"] != nil {
		UserConfig1.RecommendedConfigLevel = data["RecommendedConfigLevel"].(int)
	}
	if data["NodeNum"] != nil {
		UserConfig1.NodeNum = data["NodeNum"].(int)
	}

	for i := 0; i < UserConfig1.NodeNum; i++ {
		if data["NodeIpAddr"] != nil {
			UserConfig1.Nodes[i].NodeIpAddr = data["NodeIpAddr"].(string)
		}
		//if data["NodeDockerIp"] != nil {
		//	UserConfig1.Nodes[i].NodeDockerIp = data["NodeDockerIp"].(string)
		//}
		if data["NodeP2PPort"] != nil {
			UserConfig1.Nodes[i].NodeP2PPort = data["NodeP2PPort"].(string)
		}
		if data["NodeRPCPort"] != nil {
			UserConfig1.Nodes[i].NodeRPCPort = data["NodeRPCPort"].(string)
		}
		if data["NodeUser"] != nil {
			UserConfig1.Nodes[i].NodeUser = data["NodeUser"].(string)
		}
		if data["NodePassword"] != nil {
			UserConfig1.Nodes[i].NodePassword = data["NodePassword"].(string)
		}
	}

	return UserConfig1
}
