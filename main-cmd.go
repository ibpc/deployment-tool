package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os/exec"
	"project/command"
	"project/info"
	"project/install"
	"project/scan"
	"time"
)

// var Software info.Ossoftinfo
// var SoftwareMin info.OssoftinfoMin

var UserconfigJson info.Userconfig_json

var HardwareAll info.Hardware
var SoftwareAll info.Software
var Url info.Html_url

// 执行一条命令，并返回结果
func CommandExec(command string) []byte {
	//需要执行命令： command
	cmd := exec.Command("/bin/bash", "-c", command)
	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	// 获取输入
	output, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Println("无法获取命令的标准输出管道", err.Error())
		return nil
	}
	// 执行Linux命令
	if err := cmd.Start(); err != nil {
		fmt.Println("命令执行失败，请检查命令输入是否有误", err.Error())
		return nil
	}
	// 读取输出
	result, err := ioutil.ReadAll(output)
	if err != nil {
		fmt.Println("打印异常，请检查")
		return nil
	}
	if err := cmd.Wait(); err != nil {
		fmt.Println("Wait", err.Error(), stderr.String())
		return nil
	}
	return result
}

// 命令行暂停，等待用户点击继续
func pause() {
	var s string
	fmt.Printf("---------------------------------------部署：执行完成------------------------------------------------\n")
	fmt.Printf("输入 c 继续下一步部署:\n")
	fmt.Scan(&s)
	if s == "c" {

	} else {
		pause()
	}
}

// 打印硬件信息
func HardwarePrint(Hardware info.Hardwareinfo) int {
	fmt.Printf("CPU信息:\n")
	fmt.Printf("	Name: %s\n", Hardware.CpuName)
	fmt.Printf("	Processor Num: %d\n", Hardware.ProcessorNum)
	fmt.Printf("	Freq: %.3fMhz\n", Hardware.CpuFreq)

	if Hardware.CpuFreq > Hardware.HardwareMin.CpuFreqMin {
		fmt.Printf("    \033[1;32;40m CPU : %.fMhz (>= %.fMhz) \033[0m\n", Hardware.CpuFreq, Hardware.HardwareMin.CpuFreqMin)
	} else {
		fmt.Printf("    \033[1;31;40m CPU : %.fMhz (< %.fMhz) err\033[0m\n", Hardware.CpuFreq, Hardware.HardwareMin.CpuFreqMin)
	}

	fmt.Printf("内存信息:\n")
	fmt.Printf("	Total size: %.3fGB\n", float64(Hardware.MemSize)/1024/1024)
	if Hardware.MemSize > Hardware.HardwareMin.MemSizeMin {
		fmt.Printf("	\033[1;32;40m Mem : %.fGB (>= %.fGB) \033[0m\n", float64(Hardware.MemSize)/1024/1024, float64(Hardware.HardwareMin.MemSizeMin)/1024/1024)
	} else {
		fmt.Printf("	\033[1;31;40m Mem : %.fGB (< %.fGB) err\033[0m\n", float64(Hardware.MemSize)/1024/1024, float64(Hardware.HardwareMin.MemSizeMin)/1024/1024)
	}

	fmt.Printf("磁盘信息:\n")
	fmt.Printf("	Remain size: %.3fGB\n", Hardware.DiskSize)
	if Hardware.DiskSize > Hardware.HardwareMin.DiskSizeMin {
		fmt.Printf("	\033[1;32;40m Disk : %.fGB (>= %.fGB) \033[0m\n", Hardware.DiskSize, Hardware.HardwareMin.DiskSizeMin)
	} else {
		fmt.Printf("	\033[1;31;40m Disk : %.fGB (< %.fGB) err\033[0m\n", Hardware.DiskSize, Hardware.HardwareMin.DiskSizeMin)
	}

	fmt.Printf("文件句柄数信息:\n")
	fmt.Printf("	句柄数: %d\n", Hardware.HandleNum)
	if Hardware.HandleNum > Hardware.HardwareMin.HandleMin {
		fmt.Printf("	\033[1;32;40m 句柄数 : %d (>= %d) \033[0m\n", Hardware.HandleNum, Hardware.HardwareMin.HandleMin)
	} else {
		fmt.Printf("	\033[1;31;40m 句柄数 : %d (< %d) err\033[0m\n", Hardware.HandleNum, Hardware.HardwareMin.HandleMin)
	}

	fmt.Printf("进程数信息:\n")
	fmt.Printf("	当前进程数/最大进程数: %d/%d\n", Hardware.ProcessNum, Hardware.ProcessMax)
	if Hardware.ProcessMax-Hardware.ProcessNum > Hardware.HardwareMin.ProcessMin {
		fmt.Printf("	\033[1;32;40m 进程数 : %d (>= %d) \033[0m\n", Hardware.ProcessMax-Hardware.ProcessNum, Hardware.HardwareMin.ProcessMin)
	} else {
		fmt.Printf("	\033[1;31;40m 进程数 : %d (< %d) err\033[0m\n", Hardware.ProcessMax-Hardware.ProcessNum, Hardware.HardwareMin.ProcessMin)
	}

	fmt.Printf("操作系统信息:\n")
	fmt.Printf("	操作系统: %s\n", Hardware.OsInfo)

	return 0
}

// 读取子节点系统类型
func D_HardwarePrint(Hardware info.Hardwareinfo) (int, string) {
	st := Hardware.OsInfo
	return 0, st
}

// 打印软件信息
func SoftwarePrint(Software info.Ossoftinfo) int {
	flag := scan.CheckVersion(Software.GitVersion, Software.OssoftMin.GitVersionMin, "")
	if flag == 1 {
		fmt.Printf("	\033[1;32;40m Git Version %s (>= %s) \033[0m\n", Software.GitVersion, Software.OssoftMin.GitVersionMin)
	} else {
		fmt.Printf("	\033[1;31;40m Git Version %s (< %s) err\033[0m\n", Software.GitVersion, Software.OssoftMin.GitVersionMin)
	}

	flag = scan.CheckVersion(Software.GoVersion, Software.OssoftMin.GoVersionMin, Software.OssoftMin.GoVersionMax)
	if flag == 1 {
		fmt.Printf("	\033[1;32;40m Golang Version %s (>= %s && <= %s) \033[0m\n", Software.GoVersion, Software.OssoftMin.GoVersionMin, Software.OssoftMin.GoVersionMax)
	} else {
		fmt.Printf("	\033[1;31;40m Golang Version %s (NOT IN %s - %s) err\033[0m\n", Software.GoVersion, Software.OssoftMin.GoVersionMin, Software.OssoftMin.GoVersionMax)
	}

	flag = scan.CheckVersion(Software.GccVersion, Software.OssoftMin.GccVersionMin, "")
	if flag == 1 {
		fmt.Printf("	\033[1;32;40m Gcc Version %s (>= %s) \033[0m\n", Software.GccVersion, Software.OssoftMin.GccVersionMin)
	} else {
		fmt.Printf("	\033[1;31;40m Gcc Version %s (< %s) err\033[0m\n", Software.GccVersion, Software.OssoftMin.GccVersionMin)
	}

	flag = scan.CheckVersion(Software.GlibcVersion, Software.OssoftMin.GlibcVersionMin, "")
	if flag == 1 {
		fmt.Printf("	\033[1;32;40m Glibc Version %s (>= %s) \033[0m\n", Software.GlibcVersion, Software.OssoftMin.GlibcVersionMin)
	} else {
		fmt.Printf("	\033[1;31;40m Glibc Version %s (< %s) err\033[0m\n", Software.GlibcVersion, Software.OssoftMin.GlibcVersionMin)
	}

	//根据配置文件判断并展示docker和docker-compose版本信息
	CheckVersion_dockerAcompose(SoftwareAll.HostSoftware)

	//fmt.Printf("7z Version:%s\n", Software.SevenZVersion)
	if Software.SevenZVersion != "" {
		fmt.Printf("	\033[1;32;40m 7z Version %s \033[0m\n", Software.SevenZVersion)
	} else {
		fmt.Printf("	\033[1;31;40m 7z: command not found... err\033[0m\n")
	}

	if Software.TreeVersion != "" {
		fmt.Printf("	\033[1;32;40m tree Version %s \033[0m\n", Software.TreeVersion)
	} else {
		fmt.Printf("	\033[1;31;40m tree: command not found... err\033[0m\n")
	}

	if Software.ExpectVersion != "" {
		fmt.Printf("	\033[1;32;40m expect Version %s \033[0m\n", Software.ExpectVersion)
	} else {
		fmt.Printf("	\033[1;31;40m expect: command not found... err\033[0m\n")
	}

	return 0
}

func CheckVersion_dockerAcompose(Software info.Ossoftinfo) int {
	var a bool
	var b bool
	if UserconfigJson.DeploymentType == 1 {
		a = true
	} else {
		a = false
	}
	if UserconfigJson.Chainmaker_explorer == 1 {
		b = true
	} else {
		b = false
	}
	if a || b {
		flag := scan.CheckVersion(Software.DockerVersion, Software.OssoftMin.DockerVersionMin, "")
		if flag == 1 {
			fmt.Printf("	\033[1;32;40m Docker Version %s (>= %s) \033[0m\n", Software.DockerVersion, Software.OssoftMin.DockerVersionMin)
		} else {
			fmt.Printf("	\033[1;31;40m Docker Version %s (< %s) err\033[0m\n", Software.DockerVersion, Software.OssoftMin.DockerVersionMin)
		}

		flag = scan.CheckVersion(Software.DockerComposeVersion, Software.OssoftMin.DockerComposeVersionMin, "")
		if flag == 1 {
			fmt.Printf("	\033[1;32;40m DockerCompose Version %s (> = %s) \033[0m\n", Software.DockerComposeVersion, Software.OssoftMin.DockerComposeVersionMin)
		} else {
			fmt.Printf("	\033[1;31;40m DockerCompose Version %s (< %s) \033[0m\n", Software.DockerComposeVersion, Software.OssoftMin.DockerComposeVersionMin)
		}
	}
	return 0
}

//打印软件信息（二进制部署）
func SoftwarePrint_binary(Software info.Ossoftinfo) int {

	flag := scan.CheckVersion(Software.GlibcVersion, Software.OssoftMin.GlibcVersionMin, "")
	if flag == 1 {
		fmt.Printf("	\033[1;32;40m Glibc Version %s (>= %s) \033[0m\n", Software.GlibcVersion, Software.OssoftMin.GlibcVersionMin)
	} else {
		fmt.Printf("	\033[1;31;40m Glibc Version %s (< %s) err\033[0m\n", Software.GlibcVersion, Software.OssoftMin.GlibcVersionMin)
	}

	if Software.SevenZVersion != "" {
		fmt.Printf("	\033[1;32;40m 7z Version %s \033[0m\n", Software.SevenZVersion)
	} else {
		fmt.Printf("	\033[1;31;40m 7z: command not found... err\033[0m\n")
	}

	if Software.ExpectVersion != "" {
		fmt.Printf("	\033[1;32;40m expect Version %s \033[0m\n", Software.ExpectVersion)
	} else {
		fmt.Printf("	\033[1;31;40m expect: command not found... err\033[0m\n")
	}

	return 0
}

// 测试函数
func main_test() {

	UserconfigJson = info.GetUserconfigFromIni()
	HardwareAll.NodeHardware = make([]info.Hardwareinfo, 1)
	SoftwareAll.NodeSoftware = make([]info.Ossoftinfo, 1)

	HardwareAll.HostHardware = scan.GetHardware(UserconfigJson)
	// fmt.Printf("Host:Name: %s\n", HardwareAll.HostHardware.CpuName)
	// fmt.Printf("	Processor Num: %d\n", HardwareAll.HostHardware.ProcessorNum)
	// fmt.Printf("	Freq: %f\n", HardwareAll.HostHardware.CpuFreq)

	scan.ScanNodeHardware(UserconfigJson.Nodes[0], &HardwareAll.NodeHardware[0])
	fmt.Printf("[test]OsInfo:%s\n", HardwareAll.NodeHardware[0].OsInfo)
	fmt.Printf("[test]NodeHardware[0]:\n%+v\n", HardwareAll.NodeHardware[0])

	scan.ScanNodeSoftware(UserconfigJson, UserconfigJson.Nodes[0], &SoftwareAll.NodeSoftware[0])
	fmt.Printf("[test]GlibcVersion:%s\n", SoftwareAll.NodeSoftware[0].GlibcVersion)
	fmt.Printf("[test]NodeSoftware[0]:\n%+v\n", SoftwareAll.NodeSoftware[0])

}

// 主函数，通过命令行一键式部署长安链
func main() {
	install.Update_shell()

	// TODO扫描expect是否安装，自动安装expect，如果安装失败，直接报错退出

	fmt.Println(scan.ScanAppExpect())

	// 读取ini配置文件，获取用户配置
	UserconfigJson = info.GetUserconfigFromIni()

	//初始化子节点
	HardwareAll.NodeNum = UserconfigJson.NodeNum
	HardwareAll.NodeHardware = make([]info.Hardwareinfo, HardwareAll.NodeNum)
	SoftwareAll.NodeNum = UserconfigJson.NodeNum
	SoftwareAll.NodeSoftware = make([]info.Ossoftinfo, SoftwareAll.NodeNum)

	fmt.Printf("主节点(%s)\n", UserconfigJson.HostIpAddr)
	fmt.Printf("子节点数量：%d\n", UserconfigJson.NodeNum)

	// 测试子节点能否ssh连通
	for i := 0; i < UserconfigJson.NodeNum; i++ {
		fmt.Printf("    子节点[%d](%s)\n", i, UserconfigJson.Nodes[i].NodeIpAddr)
		cmd := fmt.Sprintf("ping -c 2 %s", UserconfigJson.Nodes[i].NodeIpAddr)
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			fmt.Printf("    \033[1;31;40m子节点[%d](%s)无法ping连接 err\033[0m\n", i, UserconfigJson.Nodes[i].NodeIpAddr)
		} else {
			fmt.Printf("    子节点[%d](%s)可以ping连接\n", i, UserconfigJson.Nodes[i].NodeIpAddr)
		}

		flag := scan.ConnectNode(UserconfigJson.Nodes[i])
		if flag == -1 {
			fmt.Printf("    \033[1;31;40m子节点[%d](%s)无法ssh连接 err\033[0m\n", i, UserconfigJson.Nodes[i].NodeIpAddr)
		} else {
			fmt.Printf("    子节点[%d](%s)可以ssh连接\n", i, UserconfigJson.Nodes[i].NodeIpAddr)
		}

	}

	if UserconfigJson.DeploymentType > 1 {
		//二进制部署长安链
		fmt.Printf("---------------------------------------部署step1.1 扫描硬件配置------------------------------------------------\n")
		// 扫描主节点硬件信息
		HardwareAll.HostHardware = scan.GetHardware(UserconfigJson)
		// 读取ini配置文件，获取硬件配置最低要求
		HardwareAll.HostHardware.HardwareMin = info.GetHardwareMinFromIni(UserconfigJson.ChainmakerVersion)
		fmt.Printf("\033[1;34;40mHost硬件信息:\033[0m\n")
		HardwarePrint(HardwareAll.HostHardware)

		for i := 0; i < UserconfigJson.NodeNum; i++ {
			// 扫描子节点硬件信息
			scan.ScanNodeHardware(UserconfigJson.Nodes[i], &HardwareAll.NodeHardware[i])
			// 子节点硬件配置最低要求
			HardwareAll.NodeHardware[i].HardwareMin = HardwareAll.HostHardware.HardwareMin
			fmt.Printf("\033[1;34;40mNode[%d]硬件信息:\033[0m\n", i)
			HardwarePrint(HardwareAll.NodeHardware[i])
		}
		pause()

		fmt.Printf("---------------------------------------部署step1.2 扫描软件配置------------------------------------------------\n")
	binary_scan:
		SoftwareAll.HostSoftware = scan.GetSoftware_binary(UserconfigJson)
		SoftwareAll.HostSoftware.OssoftMin = info.GetSoftMinFromIni(UserconfigJson.ChainmakerVersion)
		fmt.Printf("\033[1;34;40mHost软件信息:\033[0m\n")
		SoftwarePrint_binary(SoftwareAll.HostSoftware)

	binary_print:
		var str string
		fmt.Printf("输入 r 重复扫描(用户自行安装软件依赖，配置系统环境变量); 输入 c 继续下一步部署(工具自动安装):\n")
		fmt.Scan(&str)
		if str == "r" {
			goto binary_scan
		} else if str == "c" {

		} else {
			goto binary_print
		}

		var InitLog string

		fmt.Printf("---------------------------------------部署step1.3 安装依赖------------------------------------------------\n")
		install.SetUserconfigFromIni()
		install.Binary_intapp(UserconfigJson, &InitLog)
		pause()

		if UserconfigJson.DeploymentType == 2 {
			fmt.Printf("---------------------------------------部署step2 下载长安链------------------------------------------------\n")

			install.SetUserconfigFromIni()
			install.Binary_download()
			pause()

			fmt.Printf("---------------------------------------部署step3.1生成二进制部署脚本----------------------------------------\n")

			install.Binary_generate_build_release()
			install.Binary(SoftwareAll.HostSoftware)
			Url = info.GetUrlFromJson()
			install.Release_load(Url)
			install.Binary_build()
			pause()
		} else {
			fmt.Printf("---------------------------------------部署step2 扫描离线部署配置文件------------------------------------------------\n")

		offline_scan:
			install.SetUserconfigFromIni()
			install.Offline()

		offline_print:
			var str string
			fmt.Printf("输入 r 重复扫描离线部署配置文件; 输入 c 继续下一步部署(工具自动安装):\n")
			fmt.Scan(&str)
			if str == "r" {
				goto offline_scan
			} else if str == "c" {

			} else {
				goto offline_print
			}

			fmt.Printf("---------------------------------------部署step3.1生成二进制部署脚本----------------------------------------\n")

			install.Binary_generate_build_release()
			install.Offline_build(SoftwareAll.HostSoftware)
			install.Binary_build()

		}

		if UserconfigJson.NodeNum > 0 {
			// 多点部署
			if UserconfigJson.CopyType == 0 {
				fmt.Printf("---------------------------------------部署step3.2 二进制部署长安链(scp方式)----------------------------------------\n")
				install.Binary_generate_shell_scp()
				install.Binary_setup_scp()
			} else {
				fmt.Printf("---------------------------------------部署step3.3 二进制部署长安链(ftp方式)-----------------------------------------\n")
				install.Binary_generate_shell_ftp()
				install.Binary_setup_ftp()
			}
			pause()

			fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
			install.Binary_test()
			pause()

			fmt.Printf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
			install.Binary_result()
			install.Chainmaker_explorer_dockerStart()   //根据用户配置文件是否选择启用区块链浏览器，若选择启用，则下载运行浏览器
			install.Chainmaker_explorer_AotoSubscribe() //区块链浏览器自动订阅
			pause()
			fmt.Printf("\n是否关闭长安链? 输入y关闭, 输入n不关闭\n")
			var s string
			fmt.Scan(&s)
			if s == "y" {
				if UserconfigJson.CopyType == 0 {
					install.Binary_generate_shell_scp_stop()
					install.Binary_stop_scp()
				}
			}
			install.Chainmaker_explorer_dockerStop() //停止区块链浏览器
			// TODO是否关闭所有的长安链
		} else {
			fmt.Printf("---------------------------------------部署step4 启动长安链------------------------------------------------\n")
			install.Binary_start()
			time.Sleep(10 * time.Second) // 等待10s，保证启动完成
			pause()

			fmt.Printf("---------------------------------------部署step5 查看节点启动情况------------------------------------------------\n")
			install.Binary_display(UserconfigJson)
			pause()

			fmt.Printf("---------------------------------------部署示例合约------------------------------------------------\n")
			install.Binary_test()
			pause()

			fmt.Printf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
			install.Binary_result()
			install.Chainmaker_explorer_dockerStart()   //根据用户配置文件是否选择启用区块链浏览器，若选择启用，则下载运行浏览器
			install.Chainmaker_explorer_AotoSubscribe() //区块链浏览器自动订阅
			pause()
			fmt.Printf("\n是否关闭长安链? 输入y关闭, 输入n不关闭\n")
			var s string
			fmt.Scan(&s)
			if s == "y" {
				install.Binary_stop()
			}
			install.Chainmaker_explorer_dockerStop() //停止区块链浏览器
			// TODO是否关闭所有的长安链

		}

	} else {

		// docker多机部署，测试子节点是否有sudo权限
		if UserconfigJson.DeploymentType == 1 {
			for i := 0; i < UserconfigJson.NodeNum; i++ {
				flag := scan.TestNode_sduoPermission(UserconfigJson.Nodes[i])
				if flag == -1 {
					fmt.Printf("    \033[1;31;40m子节点[%d](%s)没有权限,不可进行docker多机部署,请输入root用户/密码后重新部署\033[0m\n", i, UserconfigJson.Nodes[i].NodeIpAddr)
				} else {
					fmt.Printf("    子节点[%d](%s)可以进行docker多机部署\n", i, UserconfigJson.Nodes[i].NodeIpAddr)
				}

			}
		}
		//命令行部署长安链
		fmt.Printf("---------------------------------------部署step1.1 扫描硬件配置------------------------------------------------\n")
		// 扫描主节点硬件信息
		HardwareAll.HostHardware = scan.GetHardware(UserconfigJson)
		// 读取ini配置文件，获取硬件配置最低要求
		HardwareAll.HostHardware.HardwareMin = info.GetHardwareMinFromIni(UserconfigJson.ChainmakerVersion)
		fmt.Printf("\033[1;34;40mHost硬件信息:\033[0m\n")
		HardwarePrint(HardwareAll.HostHardware)

		for i := 0; i < UserconfigJson.NodeNum; i++ {
			// 扫描子节点硬件信息
			scan.ScanNodeHardware(UserconfigJson.Nodes[i], &HardwareAll.NodeHardware[i])
			// 子节点硬件配置最低要求
			HardwareAll.NodeHardware[i].HardwareMin = HardwareAll.HostHardware.HardwareMin
			fmt.Printf("\033[1;34;40mNode[%d]硬件信息:\033[0m\n", i)
			HardwarePrint(HardwareAll.NodeHardware[i])
		}

		pause()

		fmt.Printf("---------------------------------------部署step1.2 扫描软件配置------------------------------------------------\n")
	flag_scan:
		SoftwareAll.HostSoftware = scan.GetSoftware(UserconfigJson)
		SoftwareAll.HostSoftware.OssoftMin = info.GetSoftMinFromIni(UserconfigJson.ChainmakerVersion)
		fmt.Printf("\033[1;34;40mHost软件信息:\033[0m\n")
		SoftwarePrint(SoftwareAll.HostSoftware)

		for i := 0; i < UserconfigJson.NodeNum; i++ {
			// 扫描子节点软件信息
			scan.ScanNodeSoftware(UserconfigJson, UserconfigJson.Nodes[i], &SoftwareAll.NodeSoftware[i])
			// 子节点软件配置最低要求
			SoftwareAll.NodeSoftware[i].OssoftMin = SoftwareAll.HostSoftware.OssoftMin
			fmt.Printf("\033[1;34;40mNode[%d]软件信息:\033[0m\n", i)
			fmt.Printf("	Glibc Version:%s\n", SoftwareAll.NodeSoftware[i].GlibcVersion)
			if SoftwareAll.NodeSoftware[i].GlibcVersion < SoftwareAll.HostSoftware.GlibcVersion {
				fmt.Printf("	\033[1;31;40m Node[%d] Glibc is NOT EQUAL to Host Glibc \033[0m \n", i)
			} else {
				fmt.Printf("	\033[1;32;40m Glibc is equal to Host \033[0m \n")
			}
		}
	flag_print:
		var str string
		fmt.Printf("输入 r 重复扫描(用户自行安装软件依赖，配置系统环境变量); 输入 c 继续下一步部署(工具自动安装):\n")
		fmt.Scan(&str)
		if str == "r" {
			goto flag_scan
		} else if str == "c" {

		} else {
			goto flag_print
		}

		var InitLog string

		fmt.Printf("---------------------------------------部署step2 安装依赖------------------------------------------------\n")
		install.SetUserconfigFromIni()
		install.Test_initApp(UserconfigJson, &InitLog)
		pause()

		fmt.Printf("---------------------------------------部署step3.1 下载长安链------------------------------------------------\n")
		install.Chain_download()
		pause()

		fmt.Printf("---------------------------------------部署step3.2 编译长安链------------------------------------------------\n")
		install.Chain_build()
		pause()

		if UserconfigJson.DeploymentType == 0 {
			// 命令行部署长安链
			if UserconfigJson.NodeNum > 0 {
				// 多点部署
				if UserconfigJson.CopyType == 0 {
					fmt.Printf("---------------------------------------部署step3.3 命令行部署长安链(scp方式)----------------------------------------\n")
					install.Chain_generate_shell_scp()
					pause()
					fmt.Printf("---------------------------------------部署step4 启动长安链------------------------------------------------\n")
					install.Chain_setup_scp()
					install.Generate_shell_cmdMulti_data_log() //命令行多机部署时，将各个子节点的log拷贝到主节点上
				} else {
					fmt.Printf("---------------------------------------部署step3.3 命令行部署长安链(ftp方式)-----------------------------------------\n")
					install.Chain_generate_shell_ftp()
					pause()
					fmt.Printf("---------------------------------------部署step4 启动长安链------------------------------------------------\n")
					install.Chain_setup_ftp()
				}
				pause()
				fmt.Printf("---------------------------------------部署step5 查看节点启动情况------------------------------------------------\n")
				install.Check_multi_process()
				install.Check_port_listening()
				pause()
				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				install.Chain_test()
				pause()

			} else {
				fmt.Printf("---------------------------------------部署step4 启动长安链------------------------------------------------\n")
				install.Chain_start()
				time.Sleep(10 * time.Second) // 等待10s，保证启动完成
				pause()

				fmt.Printf("---------------------------------------部署step5 查看节点启动情况------------------------------------------------\n")
				install.Chain_display(UserconfigJson)
				pause()

				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				install.Chain_test()
				pause()
			}

		} else {
			// docker部署长安链
			fmt.Printf("---------------------------------------部署step3.3 docker部署长安链------------------------------------------------\n")
			command.CommandExec("chmod -R 777 ./chainmaker-go")
			install.ChangeVersion()       //修改docker容器内长安链的版本号和下载长安链的版本号一致
			install.Docker_file_prepare() //将Docker-prepare下的文件拷贝到multi_node下
			fmt.Printf("启动docker服务...\n")
			command.CommandExec("systemctl stop firewalld.service") //linux启动docker服务可能会需要先关闭防火墙
			command.CommandExec("systemctl start docker")
			//install.Installdocker_compose(Userconfig) //若未安装compose,则安装docker-compose
			//docker多点部署
			if UserconfigJson.NodeNum > 0 {
				// 多点部署
				//install.Docker_file_prepare()                       //更换create_docker_compose_yml.sh和tpl_docker-compose_services.yml
				//command.CommandExec("chmod -R 777 ./chainmaker-go") //赋予权限
				fmt.Printf("正在为子节点配置docker环境...\033[1;34;40m耗时较长,请耐心等待\033[0m\n")
				install.Check_host_docker_image() //检查主节点是否存在镜像文件，如果没有则需要先拉取镜像
				install.Node_U_install_docker_shell()
				install.Node_C_install_docker_shell()
				install.Node_K_install_docker_shell()
				//docker多机部署为各个子节点安装docker（不安装docker则无法加入swarm集群）
				/*		for i := 0; i < UserconfigJson.NodeNum; i++ {
						if UserconfigJson.DeploymentType == 1 {
							_, b := D_HardwarePrint(HardwareAll.NodeHardware[i])
							if strings.Contains(b, "Ubuntu") {
								cmd := fmt.Sprintf("./scripts/Node_Ubuntu_InstallDocker%d.sh", i+1)
								command.CommandExec(cmd)
							} else if strings.Contains(b, "CentOS") {
								cmd := fmt.Sprintf("./scripts/Node_Centos_InstallDocker%d.sh", i+1)
								command.CommandExec(cmd)
							} else if strings.Contains(b, "Kylin") {
								cmd := fmt.Sprintf("./scripts/Node_Kylin_InstallDocker%d.sh", i+1)
								command.CommandExec(cmd)
							}
						}
					}*/
				install.Instal_docker_for_node()                    //docker多机部署为各个子节点安装docker（不安装docker则无法加入swarm集群）
				install.Pack_docker_image()                         //打包镜像文件，再通过scp传到各个子节点上，各个子节点有docker image
				command.CommandExec("chmod -R 777 ./chainmaker-go") //解除chainmaker-go文件夹下只读权限，保证./create_docker_compose_yml.sh脚本顺利执行
				install.Chain_setup_docker()
				fmt.Printf("正在创建swarm集群...\n")
				install.Chain_generate_shell_swarm()
				install.Join_docker_swarm()
				fmt.Printf("加入集群完成\n")
				install.Chain_generate_shell_docker()
				//install.Chain_setup_docker()
				pause()
				fmt.Printf("---------------------------------------部署step4 启动长安链------------------------------------------------\n")
				command.CommandExec("chmod -R 777 ./chainmaker-go")
				install.Chain_start_docker()
				install.Generate_shell_dockerMulti_data_log() //docker多点部署时，将容器的data和log拷贝到主节点上
				pause()
			} else {
				//docker单点部署，部署在主节点上
				command.CommandExec("chmod -R 777 ./chainmaker-go")
				install.Chain_generate_shell_solo_docker()
				install.Chain_setup_solo_docker()
				pause()
				fmt.Printf("---------------------------------------部署step4 启动长安链------------------------------------------------\n")
				command.CommandExec("chmod -R 777 ./chainmaker-go")
				install.Chain_start_solo_docker()
				pause()
			}
			fmt.Printf("---------------------------------------部署step5 查看节点启动情况------------------------------------------------\n")
			if UserconfigJson.NodeNum > 0 {
				install.Check_multi_process()
				install.Check_port_listening()
				//// 检查节点是否有ERROR日志
				//cmd := string("cat chainmaker-go/scripts/docker/multi_node/multi_log/log*/log*/system.log |grep \"ERROR\\|put block\\|all necessary\"")
				/*result := command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR日志为空\n")
				}
				fmt.Printf("%s\n", result)*/
				pause()
				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				install.Chain_test()
				pause()
			} else {
				install.Chain_display(UserconfigJson)
				pause()
				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				install.Chain_test()
				pause()
			}
		}
		fmt.Printf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
		install.Chain_result()
		install.Chainmaker_explorer_dockerStart() //根据用户配置文件是否选择启用区块链浏览器，若选择启用，则下载运行浏览器
		//fmt.Printf("正在订阅链...\n")
		//time.Sleep(time.Second * 20)                //设置20秒延时，确保浏览器完全启动
		install.Chainmaker_explorer_AotoSubscribe() //区块链浏览器自动订阅
		pause()
		fmt.Printf("\n是否关闭长安链? 输入y关闭, 输入n不关闭\n")
		if UserconfigJson.DeploymentType == 0 {
			var s string
			fmt.Scan(&s)
			if s == "y" {
				install.Chain_stop()
			}
		} else {
			var s string
			fmt.Scan(&s)
			if s == "y" {
				if UserconfigJson.NodeNum > 0 {
					install.Multi_docker_chain_stop()
				} else {
					install.Chain_stop_solo_docker()
				}
			}
		}
		install.Chainmaker_explorer_dockerStop() //停止区块链浏览器
		// TODO是否关闭所有的长安链

	}
}
