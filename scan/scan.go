package scan

import (
	"encoding/json"
	"fmt"
	"project/command"
	"project/info"
	"strconv"
	"strings"
)

// 一个节点的软硬件信息
var Hardware info.Hardwareinfo
var Software info.Ossoftinfo
var HardwareMin info.HardwareinfoMin
var SoftwareMin info.OssoftinfoMin

// 所有节点的软硬件信息
var HardwareAll info.Hardware
var SoftwareAll info.Software

// 在command中查找start和end，若找到则返回start与end之间的子字符串；否则返回空字符串
func GetSubString(command string, start string, end string) string {
	index1 := strings.Index(string(command), start)
	if index1 == -1 {
		return ""
	} else {
		index2 := strings.Index(string(command[index1:]), end)
		if index2 == -1 {
			return ""
		} else {
			index2 += index1
			index1 += len(start)
			//fmt.Printf("GetSubString %s %s %s index = %d %d\n", command, start, end, index1, index2)
			//fmt.Printf("[debug]GetSubString index = %d %d\n", index1, index2)

			s := string(command[index1:index2])
			return s
		}
	}
}

// 比较版本号是否满足要求，入参要求格式xx.xx.xx
func CheckVersion(version string, min string, max string) int {
	var ver1 int
	var ver2 int // 版本号前2各字段
	var min1 int
	var min2 int
	var max1 int
	var max2 int // 要求最低/最高版本号前2各字段
	var flag int // 版本号是否满足要求

	flag = 1

	// 计算版本号前2各字段
	index1 := strings.Index(version, ".")
	if index1 == -1 {
		fmt.Printf("CheckVersion version is ERROR %s\n", version)
		return -1
	} else {
		ver1, _ = strconv.Atoi(version[:index1])
	}

	index2 := strings.Index(version[index1+1:], ".")
	if index2 != -1 {
		ver2, _ = strconv.Atoi(version[index1+1 : index1+index2+1])
	} else {
		ver2, _ = strconv.Atoi(version[index1+1:])
	}

	// 最低版本号前2各字段
	if min != "" {
		index1 = strings.Index(min, ".")
		if index1 == -1 {
			fmt.Printf("CheckVersion min is ERROR %s\n", min)
			return -1
		} else {
			min1, _ = strconv.Atoi(min[:index1])
		}

		index2 = strings.Index(min[index1+1:], ".")
		if index2 != -1 {
			min2, _ = strconv.Atoi(min[index1+1 : index1+index2+1])
		} else {
			min2, _ = strconv.Atoi(min[index1+1:])
		}
	} else {
		fmt.Printf("CheckVersion min is NULL\n")
		return -1
	}

	// 最高版本号前2各字段
	if max != "" {
		index1 = strings.Index(max, ".")
		if index1 == -1 {
			fmt.Printf("CheckVersion max is ERROR %s\n", max)
			return -1
		} else {
			max1, _ = strconv.Atoi(max[:index1])
		}
		index2 = strings.Index(max[index1+1:], ".")
		if index2 != -1 {
			max2, _ = strconv.Atoi(max[index1+1 : index1+index2+1])
		} else {
			max2, _ = strconv.Atoi(max[index1+1:])
		}
	} else {
		max1 = 0
		max2 = 0 // 最大版本号为空
	}

	// fmt.Printf("CheckVersion:%s %s %s, %d %d, %d %d, %d %d \n", version, min, max,
	// 	ver1, ver2, min1, min2, max1, max2)

	// 比较版本号
	if ver1 < min1 {
		flag = 0
	} else if ver1 == min1 && ver2 < min2 {
		flag = 0
	}

	if max1 != 0 && max2 != 0 {
		if ver1 > max1 {
			flag = 0
		} else if ver1 == max1 && ver2 > max2 {
			flag = 0
		}
	}

	return flag
}

func scanCpu() int {
	// 扫描CPU名称
	cmd := string("cat /proc/cpuinfo |grep 'model name'|uniq")
	result := command.CommandExec(cmd)
	result1 := result
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过":"与"\n"，定位CPU名称字符串的位置
	start := strings.Index(string(result), ": ")
	end := strings.Index(string(result), "\n")
	Hardware.CpuName = string(result[start+2 : end])

	// 扫描CPU数量
	cmd = string("cat /proc/cpuinfo |grep processor|sort -u|wc -l")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过"\n"，定位CPU数量字符串的位置
	end = strings.Index(string(result), "\n")
	// 解析字符串，获取CPU数量
	ProcessorNum, err := strconv.ParseInt(string(result[:end]), 10, 64)
	if err == nil {
		Hardware.ProcessorNum = ProcessorNum
	} else {
		fmt.Printf("err: %s", err)
	}

	// 扫描CPU频率
	start1 := strings.Index(string(result1), "ARM")
	if start1 == -1 {
		cmd = string("cat /proc/cpuinfo |grep MHz|uniq")
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}
		// 通过":"与"\n"，定位CPU频率字符串的位置
		start = strings.Index(string(result), ": ")
		end = strings.Index(string(result), "\n")
		// 解析字符串，获取CPU频率
		CpuFreq, err := strconv.ParseFloat(string(result[start+2:end]), 64)
		if err == nil {
			Hardware.CpuFreq = CpuFreq
		} else {
			fmt.Printf("err: %s", err)
		}
	} else {
		cmd = string(`lscpu |grep "CPU max MHz"`)
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}
		// 通过":"与"\n"，定位CPU频率字符串的位置
		start = strings.Index(string(result), ": ")
		end = strings.Index(string(result), "\n")
		// 解析字符串，获取CPU频率
		CpuFreq, err := strconv.ParseFloat(string(result[start+22:end]), 64)
		if err == nil {
			Hardware.CpuFreq = CpuFreq
		} else {
			fmt.Printf("err: %s", err)
		}
	}

	return 0
}

func scanMem() int {
	// 扫描Mem大小
	cmd := string("cat /proc/meminfo |grep MemTotal")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过":"与"\n"，定位Mem大小字符串的位置
	start := strings.Index(string(result), ": ")
	end := strings.Index(string(result), "kB")
	s1 := string(result[start+1 : end])
	// 去除字符串前后的空格
	s2 := strings.Replace(s1, " ", "", -1)
	// 解析字符串，获取Mem大小
	MemSize, err := strconv.ParseInt(s2, 10, 64)
	if err == nil {
		Hardware.MemSize = MemSize
	} else {
		fmt.Printf("err: %s", err)
	}

	return 0
}

func scanDisk() int {
	// 扫描Disk大小
	cmd := string("df -m | sed 1d | awk '{sum += $4} END {print sum/1024}'")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过end 去除字符串中的换行符
	end := strings.Index(string(result), "\n")
	// 解析字符串，硬盘剩余大小
	DiskSize, err := strconv.ParseFloat(string(result[:end]), 64)
	if err == nil {
		Hardware.DiskSize = DiskSize
	} else {
		fmt.Printf("err: %s", err)
	}

	return 0
}

func scanHandle() int {
	// 扫描句柄大小
	cmd := string("ulimit -n 100010 && ulimit -n")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过end 去除字符串中的换行符
	end := strings.Index(string(result), "\n")
	// 解析字符串，句柄数量
	Size, err := strconv.ParseInt(string(result[:end]), 10, 64)
	if err == nil {
		Hardware.HandleNum = Size
	} else {
		fmt.Printf("err: %s", err)
	}

	return 0
}

func scanProcess() int {
	// 扫描进程数量
	cmd := string("ps -ef | wc -l")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过end 去除字符串中的换行符
	end := strings.Index(string(result), "\n")
	// 解析字符串，进程数量
	Size, err := strconv.ParseInt(string(result[:end]), 10, 64)
	if err == nil {
		Hardware.ProcessNum = Size
	} else {
		fmt.Printf("err: %s", err)
	}

	// 扫描进程数量最大值
	cmd = string("sysctl kernel.pid_max")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过"="与"\n"，定位Mem大小字符串的位置
	start := strings.Index(string(result), "= ")
	end = strings.Index(string(result), "\n")
	s1 := string(result[start+2 : end])
	// 解析字符串，进程数量最大值
	Size, err = strconv.ParseInt(s1, 10, 64)
	if err == nil {
		Hardware.ProcessMax = Size
	} else {
		fmt.Printf("err: %s", err)
	}

	return 0
}

func scanSystem_old() int {
	// 扫描Linux版本
	cmd := string("lsb_release -d")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 通过":"与"\n"，定位Linux版本字符串的位置
	start := strings.Index(string(result), ":")
	end := strings.Index(string(result), "\n")
	s1 := string(result[start+1 : end])
	// 去除字符串前后的空格与Tab
	s2 := strings.Replace(s1, "	", "", -1)
	Hardware.OsInfo = s2

	// fmt.Printf("system %s \n", Hardware.OsInfo)

	return 0
}

func scanSystem() int {
	// 扫描Linux版本
	cmd := string("cat /etc/os-release")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 通过"PRETTY_NAME:"与"\n"，定位Linux版本字符串的位置
	start := strings.Index(string(result), "PRETTY_NAME=")
	end := strings.Index(string(result[start:]), "\n")
	s1 := string(result[start+12 : start+end])
	// 去除字符串前后的空格与Tab
	// s2 := strings.Replace(s1, "	", "", -1)
	Hardware.OsInfo = s1

	// fmt.Printf("system %s \n", Hardware.OsInfo)

	return 0
}

func scanAppGit() int {
	// 扫描Git版本
	cmd := string("git --version")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m Git尚未安装,请等待,将自动安装 \033[0m\n")
		return -1
	}
	// 通过"git version"与"\n"，定位字符串的位置
	Software.GitVersion = GetSubString(string(result), "git version ", "\n")

	return 0
}

func scanAppGolang() int {
	// 扫描Golang版本
	cmd := string("go version")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m Golang尚未安装,请等待,将自动安装 \033[0m\n")
		return -1
	}
	// 通过"go"与"\n"，定位字符串的位置
	s1 := GetSubString(string(result), "version go", "\n")
	s2 := s1[:4]
	Software.GoVersion = s2

	return 0
}

func scanAppGcc() int {
	// 扫描Gcc版本
	cmd := string("gcc --version")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m Gcc尚未安装,请等待,将自动安装 \033[0m\n")
		return -1
	}
	// 通过")"与"\n"，定位字符串的位置
	Software.GccVersion = GetSubString(string(result), ") ", "\n")

	return 0
}

func scanAppGlibc() int {
	// 扫描Glibc版本
	cmd := string("ldd --version")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m Glibc尚未安装,请等待,将自动安装 \033[0m\n")
		return -1
	}
	// 通过") "与"\n"，定位字符串的位置
	Software.GlibcVersion = GetSubString(string(result), ") ", "\n")

	return 0
}

func scanAppDocker() int {
	// 扫描Docker版本
	cmd := string("docker --version")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m Docker尚未安装,如果是docker部署或启动区块链浏览器,将自动安装 \033[0m\n")
		return -1
	}
	// 通过"version "与","，定位字符串的位置
	Software.DockerVersion = GetSubString(string(result), "version ", ",")
	return 0
}

func scanAppDockerCompose() int {
	// 扫描Docker-compose版本
	cmd := string("docker-compose --version")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m Docker-compose尚未安装,如果是docker部署或启动区块链浏览器,将自动安装 \033[0m\n")
		return -1
	}
	// 通过"version "与","，定位字符串的位置
	Software.DockerComposeVersion = GetSubString(string(result), "version ", ",")
	return 0
}

func scanApp7z() int {
	// 扫描7z版本
	cmd := string("7za --help")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m 7z尚未安装,请等待,将自动安装 \033[0m\n")
		return -1
	}
	// 通过"version "与" ("，定位字符串的位置
	Software.SevenZVersion = GetSubString(string(result), "Version ", " (")
	return 0
}

func scanAppTree() int {
	// 扫描tree版本
	cmd := string("tree --version")
	result := command.CommandExec(cmd)
	if result == nil {
		//fmt.Printf("ERROR to exec %s\n", cmd)
		fmt.Printf("\033[1;32;40m Tree尚未安装,请等待,将自动安装 \033[0m\n")
		return -1
	}
	// 通过"version "与" ("，定位字符串的位置
	Software.TreeVersion = GetSubString(string(result), "tree", "(")
	return 0
}

func ScanAppExpect() int {
	// 扫描expect版本
	cmd := string("which expect")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s \033[1;31;40mExpect needs to be installed\033[0m\n", cmd)
		return -1
	}

	exist := strings.Contains(string(result), "no expect")

	if exist == false {
		Software.ExpectVersion = "exist"
	}

	return 0
}

func ScanChainVersion() int {
	// 扫描chainmaker二进制文件版本
	cmd := string("cd binary/bin && ./chainmaker version")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过"ChainMaker Version:"与"\n"，定位版本号字符串的位置
	s1 := GetSubString(string(result), "ChainMaker Version: ", "\n")
	Software.ChainVersion = s1
	return 0
}

// 扫描主节点硬件信息
func ScanHostHardware() {
	scanCpu()
	scanMem()
	scanDisk()
	scanHandle()
	scanProcess()
	scanSystem()

	if Hardware.CpuFreq > HardwareMin.CpuFreqMin {
		Hardware.CpuResult = fmt.Sprintf("\033[1;32;40m CPU : %fMHz (>= %fMHz) \033[0m\n", Hardware.CpuFreq, HardwareMin.CpuFreqMin)
		Hardware.CpuFlag = 1
	} else {
		Hardware.CpuResult = fmt.Sprintf("\033[1;31;40m CPU : %fMHz (< %fMHz) err\033[0m\n", Hardware.CpuFreq, HardwareMin.CpuFreqMin)
	}

	if Hardware.MemSize > HardwareMin.MemSizeMin {
		Hardware.MemResult = fmt.Sprintf("\033[1;32;40m Mem : %dKB (>= %dKB) \033[0m\n", Hardware.MemSize, HardwareMin.MemSizeMin)
		Hardware.MemFlag = 1
	} else {
		Hardware.MemResult = fmt.Sprintf("\033[1;31;40m Mem : %dKB (< %dKB) err\033[0m\n", Hardware.MemSize, HardwareMin.MemSizeMin)
	}

	if Hardware.DiskSize > HardwareMin.DiskSizeMin {
		Hardware.DiskResult = fmt.Sprintf("\033[1;32;40m Disk : %fGB (>= %fGB) \033[0m\n", Hardware.DiskSize, HardwareMin.DiskSizeMin)
		Hardware.DiskFlag = 1
	} else {
		Hardware.DiskResult = fmt.Sprintf("\033[1;31;40m Disk : %fGB (< %fGB) err\033[0m\n", Hardware.DiskSize, HardwareMin.DiskSizeMin)
	}

	if Hardware.HandleNum > HardwareMin.HandleMin {
		Hardware.HandleResult = fmt.Sprintf("\033[1;32;40m 句柄数 : %d (>= %d) \033[0m\n", Hardware.HandleNum, HardwareMin.HandleMin)
		Hardware.HandleFlag = 1
	} else {
		Hardware.HandleResult = fmt.Sprintf("\033[1;31;40m 句柄数 : %d (< %d) err\033[0m\n", Hardware.HandleNum, HardwareMin.HandleMin)
	}

	if Hardware.ProcessMax-Hardware.ProcessNum > HardwareMin.ProcessMin {
		Hardware.ProcessResult = fmt.Sprintf("\033[1;32;40m 进程数 : %d (>= %d) \033[0m\n", Hardware.ProcessMax-Hardware.ProcessNum, HardwareMin.ProcessMin)
		Hardware.ProcessFlag = 1
	} else {
		Hardware.ProcessResult = fmt.Sprintf("\033[1;31;40m 进程数 : %d (< %d) err\033[0m\n", Hardware.ProcessMax-Hardware.ProcessNum, HardwareMin.ProcessMin)
	}
}

// 扫描主节点软件信息
func ScanHostSoftware() {
	scanAppGit()
	scanAppGolang()
	scanAppGcc()
	scanAppGlibc()
	scanAppDocker()
	scanAppDockerCompose()
	scanApp7z()
	scanAppTree()

	flag := CheckVersion(Software.GitVersion, SoftwareMin.GitVersionMin, "")
	if flag == 1 {
		Software.GitResult = fmt.Sprintf("	\033[1;32;40m Git Version %s (>= %s) \033[0m\n", Software.GitVersion, SoftwareMin.GitVersionMin)
		Software.GitFlag = 1
	} else {
		Software.GitResult = fmt.Sprintf("	\033[1;31;40m Git Version %s (< %s) err\033[0m\n", Software.GitVersion, SoftwareMin.GitVersionMin)
	}

	flag = CheckVersion(Software.GoVersion, SoftwareMin.GoVersionMin, SoftwareMin.GoVersionMax)
	if flag == 1 {
		Software.GoResult = fmt.Sprintf("	\033[1;32;40m Golang Version %s (>= %s && <= %s) \033[0m\n", Software.GoVersion, SoftwareMin.GoVersionMin, SoftwareMin.GoVersionMax)
		Software.GoFlag = 1
	} else {
		Software.GoResult = fmt.Sprintf("	\033[1;31;40m Golang Version %s (NOT IN %s - %s) err\033[0m\n", Software.GoVersion, SoftwareMin.GoVersionMin, SoftwareMin.GoVersionMax)
	}

	flag = CheckVersion(Software.GccVersion, SoftwareMin.GccVersionMin, "")
	if flag == 1 {
		Software.GccResult = fmt.Sprintf("	\033[1;32;40m Gcc Version %s (>= %s) \033[0m\n", Software.GccVersion, SoftwareMin.GccVersionMin)
		Software.GccFlag = 1
	} else {
		Software.GccResult = fmt.Sprintf("	\033[1;31;40m Gcc Version %s (< %s) err\033[0m\n", Software.GccVersion, SoftwareMin.GccVersionMin)
	}

	flag = CheckVersion(Software.GlibcVersion, SoftwareMin.GlibcVersionMin, "")
	if flag == 1 {
		Software.GlibcResult = fmt.Sprintf("	\033[1;32;40m Glibc Version %s (>= %s) \033[0m\n", Software.GlibcVersion, SoftwareMin.GlibcVersionMin)
		Software.GlibcFlag = 1
	} else {
		Software.GlibcResult = fmt.Sprintf("	\033[1;31;40m Glibc Version %s (< %s) err\033[0m\n", Software.GlibcVersion, SoftwareMin.GlibcVersionMin)
	}

	flag = CheckVersion(Software.DockerVersion, SoftwareMin.DockerVersionMin, "")
	if flag == 1 {
		Software.DockerResult = fmt.Sprintf("	\033[1;32;40m Docker Version %s (>= %s) \033[0m\n", Software.DockerVersion, SoftwareMin.DockerVersionMin)
		Software.DockerFlag = 1
	} else {
		Software.DockerResult = fmt.Sprintf("	\033[1;31;40m Docker Version %s (< %s) err\033[0m\n", Software.DockerVersion, SoftwareMin.DockerVersionMin)
	}

	flag = CheckVersion(Software.DockerComposeVersion, SoftwareMin.DockerComposeVersionMin, "")
	if flag == 1 {
		Software.DockerComposeResult = fmt.Sprintf("	\033[1;32;40m DockerCompose Version %s (>= %s) \033[0m\n", Software.DockerComposeVersion, SoftwareMin.DockerComposeVersionMin)
		Software.DockerComposeFlag = 1
	} else {
		Software.DockerComposeResult = fmt.Sprintf("	\033[1;31;40m DockerCompose Version %s (< %s) err\033[0m\n", SoftwareMin.DockerComposeVersionMin)
	}

	if Software.SevenZVersion != "" {
		Software.SevenZResult = fmt.Sprintf("	\033[1;32;40m 7z is OK for 长安链 \033[0m\n")
		Software.SevenZFlag = 1
	} else {
		Software.SevenZResult = fmt.Sprintf("	\033[1;32;40m 7z is NOT OK for 长安链,等待安装 \033[0m\n")
	}

	if Software.TreeVersion != "" {
		Software.SevenZResult = fmt.Sprintf("	\033[1;32;40m Tree is OK for 长安链 \033[0m\n")
		Software.SevenZFlag = 1
	} else {
		Software.SevenZResult = fmt.Sprintf("	\033[1;32;40m Tree is NOT OK for 长安链,等待安装 \033[0m\n")
	}

}

//扫描主节点软件信息（二进制部署）
func ScanHostSoftware_binary() {

	scanAppGlibc()
	scanApp7z()
	ScanAppExpect()

	flag := CheckVersion(Software.GlibcVersion, SoftwareMin.GlibcVersionMin, "")
	if flag == 1 {
		Software.GlibcResult = fmt.Sprintf("	\033[1;32;40m Glibc Version %s (>= %s) \033[0m\n", Software.GlibcVersion, SoftwareMin.GlibcVersionMin)
		Software.GlibcFlag = 1
	} else {
		Software.GlibcResult = fmt.Sprintf("	\033[1;31;40m Glibc Version %s (< %s) err\033[0m\n", Software.GlibcVersion, SoftwareMin.GlibcVersionMin)
	}

	/*	if Software.SevenZVersion != "" {
			Software.SevenZResult = fmt.Sprintf("	\033[1;32;40m 7z Version %s \033[0m\n", Software.SevenZVersion)
			Software.SevenZFlag = 1
		} else {
			Software.SevenZResult = fmt.Sprintf("	\033[1;31;40m 7z: command not found... err\033[0m\n")
		}
	*/

	if Software.SevenZVersion != "" {
		Software.SevenZResult = fmt.Sprintf("	\033[1;32;40m 7z is OK for 长安链 \033[0m\n")
		Software.SevenZFlag = 1
	} else {
		Software.SevenZResult = fmt.Sprintf("	\033[1;32;40m 7z is NOT OK for 长安链,等待安装 \033[0m\n")
	}

}

// 检查子节点能否ssh连通
func ConnectNode(node info.Node) int {
	// 修改扫描脚本中的user
	cmd := fmt.Sprintf("sed -i '/.*set user*/cset user %s' ./scripts/chainmaker-connectNode.sh", node.NodeUser)
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 修改扫描脚本中的password
	cmd = fmt.Sprintf("sed -i '/.*set password*/cset password %s' ./scripts/chainmaker-connectNode.sh", node.NodePassword)
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 修改扫描脚本中的ip
	cmd = fmt.Sprintf("sed -i '/.*set ip*/cset ip %s' ./scripts/chainmaker-connectNode.sh", node.NodeIpAddr)
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 赋予脚本可执行权限
	cmd = fmt.Sprintf("chmod -R 777 scripts/")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 检查连接子节点
	cmd = fmt.Sprintf("./scripts/chainmaker-connectNode.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 通过"Connection established"，判断能否连接
	Connection := strings.Contains(string(result), "Connection established")
	Authentication := strings.Contains(string(result), "Authentication succeeded")
	if Connection == true && Authentication == true {
		return 0
	}

	return -1
}

// 检查子节点是否有sudo权限
func TestNode_sduoPermission(node info.Node) int {
	// 修改扫描脚本中的user
	cmd := fmt.Sprintf("sed -i '/.*set user*/cset user %s' ./scripts/TestNode_sudoPermission.sh", node.NodeUser)
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 修改扫描脚本中的password
	cmd = fmt.Sprintf("sed -i '/.*set password*/cset password %s' ./scripts/TestNode_sudoPermission.sh", node.NodePassword)
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 修改扫描脚本中的ip
	cmd = fmt.Sprintf("sed -i '/.*set ip*/cset ip %s' ./scripts/TestNode_sudoPermission.sh", node.NodeIpAddr)
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 执行判断权限脚本
	cmd = fmt.Sprintf("./scripts/TestNode_sudoPermission.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 通过"gid=0"，判断是否有权限
	result1 := strings.Contains(string(result), "gid=0")
	if result1 == true {
		return 0
	}

	return -1
}

// 扫描子节点硬件信息
func ScanNodeHardware(node info.Node, nodeHardware *info.Hardwareinfo) int {
	// 修改扫描脚本中的user
	cmd := fmt.Sprintf("sed -i '/.*set user*/cset user %s' ./scripts/chainmaker-scanHardware.sh", node.NodeUser)
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 修改扫描脚本中的password
	cmd = fmt.Sprintf("sed -i '/.*set password*/cset password %s' ./scripts/chainmaker-scanHardware.sh", node.NodePassword)
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 修改扫描脚本中的ip
	cmd = fmt.Sprintf("sed -i '/.*set ip*/cset ip %s' ./scripts/chainmaker-scanHardware.sh", node.NodeIpAddr)
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 赋值硬件信息中的IP
	nodeHardware.IpAddr = node.NodeIpAddr

	// 扫描硬件信息
	cmd = fmt.Sprintf("./scripts/chainmaker-scanHardware.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 通过"model name"，定位CPU名称字符串的位置
	start := strings.Index(string(result), "model name	: ")
	end := strings.Index(string(result[start:]), "\n")
	nodeHardware.CpuName = string(result[start+13 : start+end])

	// fmt.Printf("result:%s %d:%d\n", result[start+13 : start+end], start, end)
	// fmt.Printf("	CpuName:%s\n", nodeHardware.CpuName)

	// 通过命令行，定位CPU数量字符串的位置
	start = strings.Index(string(result), "grep processor")
	end = strings.Index(string(result[start+30:]), "\n")

	// 解析字符串，获取CPU数量
	ProcessorNum, err := strconv.ParseInt(string(result[start+30:start+30+end-1]), 10, 64)
	if err == nil {
		nodeHardware.ProcessorNum = ProcessorNum
	} else {
		fmt.Printf("err: %s\n", err)
	}
	// fmt.Printf("result:%s %d:%d\n", result[start+30:start+30+end-1], start, end)
	// fmt.Printf("ProcessorNum:%d\n", nodeHardware.ProcessorNum)

	// 通过命令行，定位CPU频率字符串的位置
	start1 := strings.Index(string(result), "ARM")
	if start1 == -1 {
		start = strings.Index(string(result), "cpu MHz		: ")
		end = strings.Index(string(result[start:]), "\n")
		// 解析字符串，获取CPU频率
		CpuFreq, err := strconv.ParseFloat(string(result[start+11:start+end-1]), 64)
		if err == nil {
			nodeHardware.CpuFreq = CpuFreq
		} else {
			fmt.Printf("err: %s\n", err)
		}
	} else {
		start = strings.Index(string(result), "CPU max MHz: ")
		end = strings.Index(string(result[start:]), "\n")
		// 解析字符串，获取CPU频率
		CpuFreq, err := strconv.ParseFloat(string(result[start+33:start+end-1]), 64)
		if err == nil {
			nodeHardware.CpuFreq = CpuFreq
		} else {
			fmt.Printf("err: %s\n", err)
		}
	}
	// fmt.Printf("result:%s %d:%d\n", result[start+11:start+end-1], start, end)
	// fmt.Printf("CpuFreq:%f\n", nodeHardware.CpuFreq)

	// 通过命令行，定位Mem大小字符串的位置
	start2 := strings.Index(string(result), "Kylin")
	if start2 == -1 {
		start = strings.Index(string(result), "MemTotal")
		end = strings.Index(string(result[start:]), "kB")
		s1 := string(result[start+36 : start+end-1])
		// 去除字符串前后的空格
		s2 := strings.Replace(s1, " ", "", -1)
		// fmt.Printf("s1:%s %d:%d\n", s1, start, end)
		// fmt.Printf("s2:%s\n", s2)
		// 解析字符串，获取Mem大小
		MemSize, err := strconv.ParseInt(s2, 10, 64)
		if err == nil {
			nodeHardware.MemSize = MemSize
		} else {
			fmt.Printf("err: %s\n", err)
		}
	} else {
		start = strings.Index(string(result), "MemTotal")
		end = strings.Index(string(result[start:]), "kB")
		s1 := string(result[start+27 : start+end-1])
		// 去除字符串前后的空格
		s2 := strings.Replace(s1, " ", "", -1)
		// fmt.Printf("s1:%s %d:%d\n", s1, start, end)
		// fmt.Printf("s2:%s\n", s2)
		// 解析字符串，获取Mem大小
		MemSize, err := strconv.ParseInt(s2, 10, 64)
		if err == nil {
			nodeHardware.MemSize = MemSize
		} else {
			fmt.Printf("err: %s\n", err)
		}
	}

	// 通过命令行，定位Disk大小字符串的位置
	start = strings.Index(string(result), "df -m")
	//end = strings.Index(string(result[start+59:]), "\n")
	// fmt.Printf("DiskSize:%s\n", string(result[start+57:start+57+end-1]))
	// 解析字符串，获取Disk大小
	start3 := strings.Index(string(result), "Ubuntu")
	if start3 == -1 {
		end = strings.Index(string(result[start+59:]), "\n")
		DiskSize, err := strconv.ParseFloat(string(result[start+59:start+59+end-1]), 64)
		if err == nil {
			nodeHardware.DiskSize = DiskSize
		} else {
			fmt.Printf("err: %s\n", err)
		}
	} else {
		end = strings.Index(string(result[start+57:]), "\n")
		DiskSize, err := strconv.ParseFloat(string(result[start+57:start+57+end-1]), 64)
		if err == nil {
			nodeHardware.DiskSize = DiskSize
		} else {
			fmt.Printf("err: %s\n", err)
		}
	}

	// 通过命令行，定位句柄大小字符串的位置
	start = strings.Index(string(result), "ulimit -n 100010 && ulimit -n")
	end = strings.Index(string(result[start+31:]), "\n")
	// fmt.Printf("HandleNum:%s\n", string(result[start+11:start+11+end-1]))
	// 解析字符串，获取句柄大小
	HandleNum, err := strconv.ParseInt(string(result[start+31:start+31+end-1]), 10, 64)
	if err == nil {
		nodeHardware.HandleNum = HandleNum
	} else {
		fmt.Printf("err: %s\n", err)
	}

	// 通过命令行，定位进程数量字符串的位置
	start = strings.Index(string(result), "ps -ef | wc -l")
	end = strings.Index(string(result[start+16:]), "\n")
	// fmt.Printf("ProcessNum:%s\n", string(result[start+16:start+16+end-1]))
	// 解析字符串，获取进程数量
	ProcessNum, err := strconv.ParseInt(string(result[start+16:start+16+end-1]), 10, 64)
	if err == nil {
		nodeHardware.ProcessNum = ProcessNum
	} else {
		fmt.Printf("err: %s\n", err)
	}

	// 通过命令行，定位进程数量最大值字符串的位置
	start = strings.Index(string(result), "kernel.pid_max = ")
	end = strings.Index(string(result[start:]), "\n")
	// fmt.Printf("ProcessMax:%s\n", string(result[start+17:start+end-1]))
	// 解析字符串，获取进程数量最大值
	ProcessMax, err := strconv.ParseInt(string(result[start+17:start+end-1]), 10, 64)
	if err == nil {
		nodeHardware.ProcessMax = ProcessMax
	} else {
		fmt.Printf("err: %s\n", err)
	}

	// 解析字符串，获取CPU数量
	/*ProcessorNum, err := strconv.ParseInt(string(result[start+30:start+30+end-1]), 10, 64)
	  if err == nil {
	  	nodeHardware.ProcessorNum = ProcessorNum
	  } else {
	  			s3 := string(result[start+30:start+30+end-1])
	  			s4 := s3[9:]
	  		ProcessorNum, err := strconv.ParseInt(s4, 10, 64)
	  			 if err == nil {
	  			 nodeHardware.ProcessorNum = ProcessorNum
	  }  else{
	  			   fmt.Printf("err: %s\n", err)
	  	}

	  }
	   //fmt.Printf("result:%s %d:%d\n", result[start+30:start+30+end-1], start, end)
	   //fmt.Printf("ProcessorNum:%d\n", nodeHardware.ProcessorNum)*/

	// 通过命令行，定位CPU频率字符串的位置
	start = strings.Index(string(result), "cpu MHz		: ")
	end = strings.Index(string(result[start:]), "\n")
	// 解析字符串，获取CPU频率
	CpuFreq, err := strconv.ParseFloat(string(result[start+11:start+end-1]), 64)
	if err == nil {
		nodeHardware.CpuFreq = CpuFreq
	} else {
		s12 := string(result[start+11 : start+end-1])
		s13 := s12[9:]
		CpuFreq, err := strconv.ParseFloat(s13, 64)
		if err == nil {
			nodeHardware.CpuFreq = CpuFreq
		} else {
			fmt.Printf("err: %s\n", err)
		}
	}
	// fmt.Printf("result:%s %d:%d\n", result[start+11:start+end-1], start, end)
	// fmt.Printf("CpuFreq:%f\n", nodeHardware.CpuFreq)

	/*
	  // 通过命令行，定位Mem大小字符串的位置
	  start = strings.Index(string(result), "MemTotal")
	  end = strings.Index(string(result[start:]), "kB")
	  s1 := string(result[start+36 : start+end-1])
	  // 去除字符串前后的空格
	  s2 := strings.Replace(s1, " ", "", -1)
	  // fmt.Printf("s1:%s %d:%d\n", s1, start, end)
	  // fmt.Printf("s2:%s\n", s2)
	  // 解析字符串，获取Mem大小
	  MemSize, err := strconv.ParseInt(s2, 10, 64)
	  if err == nil {
	  	nodeHardware.MemSize = MemSize
	  } else {
	  			s5 := s2[9:]
	  			MemSize, err := strconv.ParseInt(s5, 10, 64)
	  				 if err == nil {
	  			 nodeHardware.MemSize = MemSize
	  } else {
	  	fmt.Printf("err: %s\n", err)
	  }
	  	}
	  // 通过命令行，定位Disk大小字符串的位置
	  start = strings.Index(string(result), "df -m | sed 1d | awk '{sum += $4} END {print sum/1024}'")
	  end = strings.Index(string(result[start+57:]), "\n")
	  // fmt.Printf("DiskSize:%s\n", string(result[start+57:start+57+end-1]))
	  // 解析字符串，获取Disk大小
	  DiskSize, err := strconv.ParseFloat(string(result[start+57:start+57+end-1]), 64)
	  if err == nil {
	  	nodeHardware.DiskSize = DiskSize
	  } else {
	  			s6 := string(result[start+57:start+57+end-1])
	  			s7 := s6[9:]
	  		   DiskSize, err := strconv.ParseFloat(s7, 64)
	  	if err == nil {
	  	nodeHardware.DiskSize = DiskSize
	  } else {
	  	fmt.Printf("err: %s\n", err)
	  }
	  	}
	  // 通过命令行，定位句柄大小字符串的位置
	  start = strings.Index(string(result), "ulimit -n")
	  end = strings.Index(string(result[start+11:]), "\n")
	  // fmt.Printf("HandleNum:%s\n", string(result[start+11:start+11+end-1]))
	  // 解析字符串，获取句柄大小
	  HandleNum, err := strconv.ParseInt(string(result[start+11:start+11+end-1]), 10, 64)
	  if err == nil {
	  	nodeHardware.HandleNum = HandleNum
	  } else {
	  		s8 := string(result[start+11:start+11+end-1])
	  			s9 := s8[9:]
	  		HandleNum, err := strconv.ParseInt(s9, 10, 64)
	  			if err == nil {
	  	  nodeHardware.HandleNum = HandleNum
	  } else {
	  			fmt.Printf("err: %s\n", err)
	  }
	  	}
	  // 通过命令行，定位进程数量字符串的位置
	  start = strings.Index(string(result), "ps -ef | wc -l")
	  end = strings.Index(string(result[start+16:]), "\n")
	  // fmt.Printf("ProcessNum:%s\n", string(result[start+16:start+16+end-1]))
	  // 解析字符串，获取进程数量
	  ProcessNum, err := strconv.ParseInt(string(result[start+16:start+16+end-1]), 10, 64)
	  if err == nil {
	  	nodeHardware.ProcessNum = ProcessNum
	  } else {
	  			s10 := string(result[start+16:start+16+end-1])
	  			s11 := s10[9:]
	  			ProcessNum, err := strconv.ParseInt(s11, 10, 64)
	  		  if err == nil {
	  		nodeHardware.ProcessNum = ProcessNum
	  } else {
	  	fmt.Printf("err: %s\n", err)
	  }
	  	}
	  // 通过命令行，定位进程数量最大值字符串的位置
	  start = strings.Index(string(result), "kernel.pid_max =")
	  end = strings.Index(string(result[start:]), "\n")
	  // fmt.Printf("ProcessMax:%s\n", string(result[start+17:start+end-1]))
	  // 解析字符串，获取进程数量最大值
	  ProcessMax, err := strconv.ParseInt(string(result[start+17:start+end-1]), 10, 64)
	  if err == nil {
	  	nodeHardware.ProcessMax = ProcessMax
	  } else {
	  			s14 := string(result[start+17:start+end-1])
	  			s15 := s14[9:]
	  			ProcessMax, err := strconv.ParseInt(s15, 10, 64)
	  		   if err == nil {
	  	nodeHardware.ProcessMax = ProcessMax
	  } else {
	  	fmt.Printf("err: %s\n", err)
	  }
	  	}
	*/
	// 通过命令行，定位Linux版本字符串的位置
	start = strings.Index(string(result), "PRETTY_NAME=")
	end = strings.Index(string(result[start:]), "\n")
	s3 := string(result[start+12 : start+end])
	// 去除字符串前后的空格与Tab
	// s2 = strings.Replace(s1, "	", "", -1)
	nodeHardware.OsInfo = s3

	// fmt.Printf("nodeHardware.OsInfo:%s\n", nodeHardware.OsInfo)

	return 0
}

// 扫描子节点软件信息
func ScanNodeSoftware(Userconfig info.Userconfig_json, node info.Node, nodeSoftware *info.Ossoftinfo) int {

	if Userconfig.DeploymentType == 1 {
		// 修改扫描脚本中的user
		cmd := fmt.Sprintf("sed -i '/.*set user*/cset user %s' ./scripts/chainmaker-scanSoftware.sh", node.NodeUser)
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

		// 修改扫描脚本中的password
		cmd = fmt.Sprintf("sed -i '/.*set password*/cset password %s' ./scripts/chainmaker-scanSoftware.sh", node.NodePassword)
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

		// 修改扫描脚本中的ip
		cmd = fmt.Sprintf("sed -i '/.*set ip*/cset ip %s' ./scripts/chainmaker-scanSoftware.sh", node.NodeIpAddr)
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}
		// 赋值软件信息中的IP
		nodeSoftware.IpAddr = node.NodeIpAddr

		//docker部署
		cmd = fmt.Sprintf("./scripts/chainmaker-scanSoftware.sh")
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

		start := strings.Index(string(result), "centos")
		if start != -1 {

			//解析glibc版本信息
			start := strings.Index(string(result), "libc")
			if start != -1 {
				start := strings.Index(string(result), "libc")
				end := strings.Index(string(result[start:]), "\n")
				nodeSoftware.GlibcVersion = string(result[start+6 : start+end-1])
			} else {
				fmt.Printf("ERROR to exec %s\n", cmd)
				return -1
			}
		} else {

			//解析glibc版本信息
			start := strings.Index(string(result), "GLIBC")
			if start != -1 {
				start := strings.Index(string(result), "GLIBC")
				end := strings.Index(string(result[start:]), "-")
				nodeSoftware.GlibcVersion = string(result[start+6 : start+end])
			} else {
				fmt.Printf("ERROR to exec %s\n", cmd)
				return -1
			}
		}

		//解析7z版本信息
		start = strings.Index(string(result), "p7zip")
		if start != -1 {
			start := strings.Index(string(result), "p7zip Version")
			end := strings.Index(string(result[start:]), " (")
			nodeSoftware.SevenZVersion = string(result[start+14 : start+end])
		} else {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

		//解析docker版本信息
		start = strings.Index(string(result), "Docker version ")
		if start != -1 {
			start := strings.Index(string(result), "Docker version ")
			end := strings.Index(string(result[start:]), ",")
			nodeSoftware.DockerVersion = string(result[start+15 : start+end])
		} else {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

	} else {

		// 修改扫描脚本中的user
		cmd := fmt.Sprintf("sed -i '/.*set user*/cset user %s' ./scripts/chainmaker-scanSoftware-binary.sh", node.NodeUser)
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

		// 修改扫描脚本中的password
		cmd = fmt.Sprintf("sed -i '/.*set password*/cset password %s' ./scripts/chainmaker-scanSoftware-binary.sh", node.NodePassword)
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

		// 修改扫描脚本中的ip
		cmd = fmt.Sprintf("sed -i '/.*set ip*/cset ip %s' ./scripts/chainmaker-scanSoftware-binary.sh", node.NodeIpAddr)
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}
		// 赋值软件信息中的IP
		nodeSoftware.IpAddr = node.NodeIpAddr
		// 扫描软件信息
		cmd = fmt.Sprintf("./scripts/chainmaker-scanSoftware-binary.sh")
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}

		start := strings.Index(string(result), "centos")
		if start != -1 {

			//解析glibc版本信息
			start := strings.Index(string(result), "libc")
			if start != -1 {
				start := strings.Index(string(result), "libc")
				end := strings.Index(string(result[start:]), "\n")
				nodeSoftware.GlibcVersion = string(result[start+6 : start+end-1])
			} else {
				fmt.Printf("ERROR to exec %s\n", cmd)
				return -1
			}
		} else {

			//解析glibc版本信息
			start := strings.Index(string(result), "GLIBC")
			if start != -1 {
				start := strings.Index(string(result), "GLIBC")
				end := strings.Index(string(result[start:]), "-")
				nodeSoftware.GlibcVersion = string(result[start+6 : start+end])
			} else {
				fmt.Printf("ERROR to exec %s\n", cmd)
				return -1
			}
		}

		//解析7z版本信息
		start = strings.Index(string(result), "p7zip")
		if start != -1 {
			start := strings.Index(string(result), "p7zip Version")
			end := strings.Index(string(result[start:]), " (")
			nodeSoftware.SevenZVersion = string(result[start+14 : start+end])
		} else {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}
		// 通过命令行，定位Glibc版本字符串的位置
		//start := strings.Index(string(result), "ldd --version")
		// 解析字符串，获取Glibc版本
		//nodeSoftware.GlibcVersion = GetSubString(string(result[start:]), ") ", "\n")
	}
	return 0
}

// 为UI界面版本提供硬件信息
func HardwareReturn(Userconfig info.Userconfig_json) string {
	// 扫描主节点硬件信息
	HardwareMin = info.GetHardwareMinFromIni(Userconfig.ChainmakerVersion)
	Hardware.HardwareMin = HardwareMin
	ScanHostHardware()
	HardwareAll.HostHardware = Hardware

	// 逐个扫描从节点硬件信息
	HardwareAll.NodeNum = Userconfig.NodeNum
	HardwareAll.NodeHardware = make([]info.Hardwareinfo, HardwareAll.NodeNum)

	//测试子节点能否ssh连通connect测试
	for i := 0; i < Userconfig.NodeNum; i++ {
		fmt.Printf("    子节点[%d](%s)\n", i, Userconfig.Nodes[i].NodeIpAddr)
		cmd := fmt.Sprintf("ping -c 2 %s", Userconfig.Nodes[i].NodeIpAddr)
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			fmt.Printf("    \033[1;31;40m子节点[%d](%s)无法ping连接 err\033[0m\n", i, Userconfig.Nodes[i].NodeIpAddr)
		} else {
			fmt.Printf("    子节点[%d](%s)可以ping连接\n", i, Userconfig.Nodes[i].NodeIpAddr)
		}

		flag := ConnectNode(Userconfig.Nodes[i])
		if flag == -1 {
			fmt.Printf("    \033[1;31;40m子节点[%d](%s)无法ssh连接 err\033[0m\n", i, Userconfig.Nodes[i].NodeIpAddr)
		} else {
			fmt.Printf("    子节点[%d](%s)可以ssh连接\n", i, Userconfig.Nodes[i].NodeIpAddr)
			HardwareAll.NodeHardware[i].ConnectFlag = 1
		}

	}

	for i := 0; i < Userconfig.NodeNum; i++ {
		ScanNodeHardware(Userconfig.Nodes[i], &HardwareAll.NodeHardware[i])
		HardwareAll.NodeHardware[i].HardwareMin = HardwareMin
		if HardwareAll.NodeHardware[i].CpuFreq > HardwareMin.CpuFreqMin {
			HardwareAll.NodeHardware[i].CpuResult = fmt.Sprintf("\033[1;32;40m CPU : %fMhz (>= %fMhz) \033[0m\n", HardwareAll.NodeHardware[i].CpuFreq, HardwareMin.CpuFreqMin)
			HardwareAll.NodeHardware[i].CpuFlag = 1
		} else {
			HardwareAll.NodeHardware[i].CpuResult = fmt.Sprintf("\033[1;31;40m CPU : %fMhz (< %fMhz) err\033[0m\n", HardwareAll.NodeHardware[i].CpuFreq, HardwareMin.CpuFreqMin)
		}

		if HardwareAll.NodeHardware[i].MemSize > HardwareMin.MemSizeMin {
			HardwareAll.NodeHardware[i].MemResult = fmt.Sprintf("\033[1;32;40m Mem : %dKB (>= %dKB) \033[0m\n", HardwareAll.NodeHardware[i].MemSize, HardwareMin.MemSizeMin)
			HardwareAll.NodeHardware[i].MemFlag = 1
		} else {
			HardwareAll.NodeHardware[i].MemResult = fmt.Sprintf("\033[1;31;40m Mem : %dKB (< %dKB) err\033[0m\n", HardwareAll.NodeHardware[i].MemSize, HardwareMin.MemSizeMin)
		}

		if HardwareAll.NodeHardware[i].DiskSize > HardwareMin.DiskSizeMin {
			HardwareAll.NodeHardware[i].DiskResult = fmt.Sprintf("\033[1;32;40m Disk : %fGB (>= %.fGB) \033[0m\n", HardwareAll.NodeHardware[i].DiskSize, HardwareMin.DiskSizeMin)
			HardwareAll.NodeHardware[i].DiskFlag = 1
		} else {
			HardwareAll.NodeHardware[i].DiskResult = fmt.Sprintf("\033[1;31;40m Disk : %fGB (< %.fGB) err\033[0m\n", HardwareAll.NodeHardware[i].DiskSize, HardwareMin.DiskSizeMin)
		}

		if HardwareAll.NodeHardware[i].HandleNum > HardwareMin.HandleMin {
			HardwareAll.NodeHardware[i].HandleResult = fmt.Sprintf("\033[1;32;40m 句柄数 : %d (>= %d) \033[0m\n", HardwareAll.NodeHardware[i].HandleNum, HardwareMin.HandleMin)
			HardwareAll.NodeHardware[i].HandleFlag = 1
		} else {
			HardwareAll.NodeHardware[i].HandleResult = fmt.Sprintf("\033[1;31;40m 句柄数 : %d (< %d) err\033[0m\n", HardwareAll.NodeHardware[i].HandleNum, HardwareMin.HandleMin)
		}

		if HardwareAll.NodeHardware[i].ProcessMax-HardwareAll.NodeHardware[i].ProcessNum > HardwareMin.ProcessMin {
			HardwareAll.NodeHardware[i].ProcessResult = fmt.Sprintf("\033[1;32;40m Mem : %d (>= %d) \033[0m\n", HardwareAll.NodeHardware[i].ProcessMax-HardwareAll.NodeHardware[i].ProcessNum, HardwareMin.ProcessMin)
			HardwareAll.NodeHardware[i].ProcessFlag = 1
		} else {
			HardwareAll.NodeHardware[i].ProcessResult = fmt.Sprintf("\033[1;31;40m Mem : %d (< %d) err\033[0m\n", HardwareAll.NodeHardware[i].ProcessMax-HardwareAll.NodeHardware[i].ProcessNum, HardwareMin.ProcessMin)
		}
	}

	// 将HardwareAll转换为json格式
	data, err := json.Marshal(HardwareAll)
	if err != nil {
		panic(err)
	}
	return string(data)
}

// 为UI界面版本提供软件信息
func OssoftReturn(Userconfig info.Userconfig_json) string {
	// 扫描主节点软件信息
	SoftwareMin = info.GetSoftMinFromIni(Userconfig.ChainmakerVersion)
	Software.OssoftMin = SoftwareMin

	if Userconfig.DeploymentType == 2 {
		ScanHostSoftware_binary()
	} else {
		ScanHostSoftware()
	}
	SoftwareAll.HostSoftware = Software

	// 逐个扫描从节点软件信息
	SoftwareAll.NodeNum = Userconfig.NodeNum
	SoftwareAll.NodeSoftware = make([]info.Ossoftinfo, SoftwareAll.NodeNum)

	//测试子节点能否ssh连通connect测试
	for i := 0; i < Userconfig.NodeNum; i++ {
		fmt.Printf("    子节点[%d](%s)\n", i, Userconfig.Nodes[i].NodeIpAddr)
		cmd := fmt.Sprintf("ping -c 2 %s", Userconfig.Nodes[i].NodeIpAddr)
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			fmt.Printf("    \033[1;31;40m子节点[%d](%s)无法ping连接 err\033[0m\n", i, Userconfig.Nodes[i].NodeIpAddr)
		} else {
			fmt.Printf("    子节点[%d](%s)可以ping连接\n", i, Userconfig.Nodes[i].NodeIpAddr)
		}

		flag := ConnectNode(Userconfig.Nodes[i])
		if flag == -1 {
			fmt.Printf("    \033[1;31;40m子节点[%d](%s)无法ssh连接 err\033[0m\n", i, Userconfig.Nodes[i].NodeIpAddr)
		} else {
			fmt.Printf("    子节点[%d](%s)可以ssh连接\n", i, Userconfig.Nodes[i].NodeIpAddr)
			SoftwareAll.NodeSoftware[i].ConnectFlag = 1
		}

	}

	for i := 0; i < Userconfig.NodeNum; i++ {
		ScanNodeSoftware(Userconfig, Userconfig.Nodes[i], &SoftwareAll.NodeSoftware[i])
		SoftwareAll.NodeSoftware[i].OssoftMin = SoftwareMin
		//docker部署
		if Userconfig.DeploymentType == 1 {
			flag := CheckVersion(SoftwareAll.NodeSoftware[i].GlibcVersion, SoftwareMin.GlibcVersionMin, "")
			if flag == 1 {
				SoftwareAll.NodeSoftware[i].GlibcFlag = 1
			}

			flag = CheckVersion(SoftwareAll.NodeSoftware[i].DockerVersion, SoftwareMin.DockerVersionMin, "")
			if flag == 1 {
				SoftwareAll.NodeSoftware[i].DockerFlag = 1
			}

			if SoftwareAll.NodeSoftware[i].SevenZVersion != "" {
				SoftwareAll.NodeSoftware[i].SevenZFlag = 1
			}

		} else {
			flag := CheckVersion(SoftwareAll.NodeSoftware[i].GlibcVersion, SoftwareMin.GlibcVersionMin, "")
			if flag == 1 {
				SoftwareAll.NodeSoftware[i].GlibcFlag = 1
			}

			if SoftwareAll.NodeSoftware[i].SevenZVersion != "" {
				SoftwareAll.NodeSoftware[i].SevenZFlag = 1
			}

		}
	}

	data, err := json.Marshal(SoftwareAll)
	if err != nil {
		panic(err)
	}
	return string(data)
}

// 提供软件信息
func GetSoftware(Userconfig info.Userconfig_json) info.Ossoftinfo {
	SoftwareMin = info.GetSoftMinFromIni(Userconfig.ChainmakerVersion)
	ScanHostSoftware()
	return Software
}

// 提供软件信息(二进制部署)
func GetSoftware_binary(Userconfig info.Userconfig_json) info.Ossoftinfo {
	SoftwareMin = info.GetSoftMinFromIni(Userconfig.ChainmakerVersion)
	ScanHostSoftware_binary()
	return Software
}

// 提供硬件信息
func GetHardware(Userconfig info.Userconfig_json) info.Hardwareinfo {
	HardwareMin = info.GetHardwareMinFromIni(Userconfig.ChainmakerVersion)
	ScanHostHardware()
	return Hardware
}
