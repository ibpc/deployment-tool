# 长安链一键部署工具

本产品定位为供开源社区区块链开发者，区块链产品经理使用，以便在初步接触长安链时，通过本平台可单机快速部署长安链用于产品体验和测试，并对长安链的技术架构，功能模块有一个初步了解。

本产品支持私有化部署，详细部署流程参考使用指南。

## 命令行(非界面)版本一键部署工具使用指南

### 版本支持

目前支持长安链`2.3.0`、`2.3.1`、`2.3.2`版本。

### 环境依赖


**expect**

Centos 系统请通过命令安装：

```bash
  $ yum -y install tcl
  $ yum -y install expect
```

Ubuntu 系统请通过命令安装：

```bash
  $ apt-get -y install tcl
  $ apt-get -y install expect
```

Kylin 系统请通过命令安装：

```bash
  $ yum -y install tcl
  $ yum -y install expect
```

若已安装，请通过命令查看版本：

```bash
  $ expect -v
```

**7z**

Centos 系统请通过命令安装：

```bash
  $ yum -y install epel-release
  $ yum -y install p7zip p7zip-plugins
```

Ubuntu 系统请通过命令安装：

```bash
  $ apt-get install p7zip-full p7zip-rar
```

Kylin 系统请通过命令安装：

```bash
  $ yum -y install epel-release
  $ yum -y install p7zip p7zip-plugins
```

若已安装，请通过命令查看版本：

```bash
  $ 7za --help
```

**docker**

`docker`版本为`20.10.7`或以上
`docker-compose`版本为`1.29.2`或以上

下载地址：
- [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)
- [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

若已安装，请通过命令查看版本：

```bash
  $ docker -v
  $ docker-compose -v
```
### 账号依赖

当前管理平台的代码仓库为私有仓库，需要先到[https://git.chainmaker.org.cn/users/sign_up](https://git.chainmaker.org.cn/users/sign_up) 进行账号注册，才能同步代码。

### 整体流程说明

#### 从零部署链

如果您还未部署过链，可通过本工具部署长安链，主要流程如下。

1、首先需要根据长安链版本需求，安装相应依赖。

2、接着修改`user_config.ini`配置文件，配置链的相关信息，包含共识策略、参与组织与节点、区块基本配置等。

3、执行工具启动命令，一键启动链。

4、启动成功后，您可以使用长安链相关功能。

5、如果您想关闭当前启动的链，根据工具提示关闭即可。

### 部署

一键部署工具具备源代码命令行、`docker`、二进制、离线四种方式部署。

#### 代码下载

下载地址： [https://git.chainmaker.org.cn/ibpc/deployment-tool.git](https://git.chainmaker.org.cn/ibpc/deployment-tool.git)
下载代码：

```bash
$ git clone https://git.chainmaker.org.cn/ibpc/deployment-tool.git
```

#### 启动一键部署工具

下载后端代码后，进入`deployment-tool`目录，执行以下命令
```bash
$ go mod tidy
$ go build main-cmd.go
$ ./main-cmd
```

启动成功后，根据工具提示进行部署即可，如果发现工具提示硬件依赖不足或软件依赖不足，请更换设备或更新软件依赖。

### 代码说明
```sh
├── README.md                           // 说明
├── main.go                             // 界面版本的主函数，与web UI交互，一键式部署长安链
├── main-cmd.go                         // 命令行版本的主函数，通过命令行交互，一键式部署长安链
├── info                        
     └──  info.go                         // 定义读取用户配置信息、系统软硬件信息
├── scan                        
     └──  scan.go                         // 扫描系统软/硬件信息
├── install                      
     ├── soft.go                         // 设置apt、yum源，升级安装软件依赖
     └── chainmaker.go                   // 下载、编译、配置、部署长安链，并验证部署是否成功
├── docker-prepare                      // docker部署所需配置文件及部署脚本
├── user_config.ini                     // 配置文件：需要由用户配置信息
├── config.ini                          // 配置文件：静态配置信息，用户不需要感知
├── backup.sh                           // 编译脚本：备份长安链启动记录
├──binary-build.sh                      // 编译脚本：编译生成二进制部署工具
├──binary-build1-0.sh                   // 配置脚本：PermissionWithCert模式下，执行prepare.sh
├──binary-build1-1.sh                   // 配置脚本：Public模式下，执行prepare_pk.sh
├──binary-build1-2.sh                   // 配置脚本：PermissionWithKey模式模式下，执行prepare_pwk.sh
├──binary-build2.sh                     // 编译脚本：执行build_release.sh脚本，编译chainmaker-go模块，并打包生成安装
├──binary-setup_ftp.sh                  // 部署脚本：ftp方式下的部署脚本
├──binary-setup_scp.sh                  // 部署脚本：scp方式下的部署脚本
├──binary-stop_scp.sh                   // 部署脚本：scp部署方式下停止长安链脚本
├──binary-test.sh                       // 验证脚本：部署示例合约，验证长安链
├──binary-test_pk.sh                    // 验证脚本：pk模式部署示例合约，验证长安链
├──build_release_demo.sh                // 配置脚本：二进制部署build_release.sh脚本的demo
├──offline-build.sh                     // 编译脚本：编译生成离线部署工具
├── chainmaker-build1.sh                // 编译脚本：编译证书生成工具
├── chainmaker-build2.sh                // 编译脚本：软连接chainmaker-cryptogen到tools目录下
├── chainmaker-build3-0.sh              // 配置脚本：PermissionWithCert模式下，执行prepare.sh
├── chainmaker-build3-1.sh              // 配置脚本：Public模式下，执行prepare_pk.sh
├── chainmaker-build3-2.sh              // 配置脚本：PermissionWithKey模式模式下，执行prepare_pwk.sh
├── chainmaker-build4.sh                // 编译脚本：执行build_release.sh脚本，编译chainmaker-go模块，并打包生成安装
├── chainmaker-connectNode.sh           // 扫描脚本：检查子节点能否ssh连通
├── chainmaker-create_swarm.sh          // 部署脚本:docker多机部署，子节点通过令牌加入swarm集群
├── chainmaker-setup_docker_pk.sh       // 部署脚本：docker部署pk模式下的部署脚本
├── chainmaker-setup_solo_docker_pk.sh  // 部署脚本：docker单点pk模式部署脚本
├── chainmaker-setup_solo_docker.sh     // 部署脚本：docker单点证书模式部署脚本
├── chainmaker-stop_scp.sh              // 部署脚本：多机部署下停止链脚本
├── chainmaker-test.sh                  // 验证脚本：部署示例合约，验证长安链
├── Docker_get_data_log.sh              // 验证脚本：docker多机部署传回data、log文件
├── multi_check_portListen.sh           // 验证脚本：多机部署检查端口是否监听
├── multi_check_process.sh              // 验证脚本：多机部署检查进程是否存在
├── multi_CmdGet_log.sh                 // 验证脚本：命令行源码部署证书模式多机部署传回日志信息
├── multi_CmdPkGet_log.sh               // 验证脚本：命令行源码部署pk模式多机部署传回日志信息
├── multi_docker_chain_stop.sh          // 部署脚本：docker多机部署停止链脚本
├── Node_Centos_InstallDocker.sh        // 安装脚本：子节点系统为centos的docker安装脚本
├── Node_Ubuntu_InstallDocker.sh        // 安装脚本：子节点系统为ubuntu的docker安装脚本
├── TestNode_sudoPermission.sh          // 验证脚本：子节点sudo权限检查脚本
├── chainmaker-setup_scp.sh             // 部署脚本：scp方式下的部署脚本
├── chainmaker-setup_ftp.sh             // 部署脚本：ftp方式下的部署脚本
├── chainmaker-setup_docker.sh          // 部署脚本：docker方式下的部署脚本
├── chainmaker-test.sh                  // 验证脚本：部署示例合约，验证长安链
├── chainmaker-test_pk.sh               // 验证脚本：pk模式部署示例合约，验证长安链
├── set_apt_source_1804.sh              // 软件安装脚本：apt源配置脚本
├── install_ftp.sh                      // 软件安装脚本：FTP安装脚本
├── chainmaker-scanHardware.sh          // 扫描脚本：扫描子节点硬件信息
├── chainmaker-scanSoftware.sh          // 扫描脚本：扫描子节点软件信息
```

