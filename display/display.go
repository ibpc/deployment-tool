package display

import (
	"fmt"
	"os/exec"
	"project/command"
	//"project/info"
	//"project/install"
)

func Display_panic() string {
	/*cmd1 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node1/bin/panic.log")
	cmd2 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node2/bin/panic.log")
	cmd3 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node3/bin/panic.log")
	cmd4 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node4/bin/panic.log")
	result1, _ := cmd1.CombinedOutput()
	result2, _ := cmd2.CombinedOutput()
	result3, _ := cmd3.CombinedOutput()
	result4, _ := cmd4.CombinedOutput()
	fmt.Printf("%s\n", string(result1)+string(result2)+string(result3)+string(result4))
	return string(result1) + string(result2) + string(result3) + string(result4)*/
	cmd := string("cat ../build/release/chainmaker-v2.3.1-node1/bin/panic.log")
	result1 := command.CommandExec(cmd)
	cmd2 := string("cat ../build/release/chainmaker-v2.3.1-node2/bin/panic.log")
	result2 := command.CommandExec(cmd2)
	fmt.Printf("%s\n", string(result1)+string(result2))
	return string(result1) + string(result2)

}
func Display_system() string {
	cmd1 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node1/log/system.log")
	cmd2 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node2/log/system.log")
	cmd3 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node3/log/system.log")
	cmd4 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node4/log/system.log")
	result1, _ := cmd1.CombinedOutput()
	result2, _ := cmd2.CombinedOutput()
	result3, _ := cmd3.CombinedOutput()
	result4, _ := cmd4.CombinedOutput()
	fmt.Printf("%s\n", string(result1)+string(result2)+string(result3)+string(result4))
	return string(result1) + string(result2) + string(result3) + string(result4)
}
func Display_systemErr() string {
	cmd1 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node1/log/system.log |grep \"ERROR\\|put block\\|all necessary")
	cmd2 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node2/log/system.log|grep \"ERROR\\|put block\\|all necessary")
	cmd3 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node3/log/system.log|grep \"ERROR\\|put block\\|all necessary")
	cmd4 := exec.Command("cat", "../build/release/chainmaker-v2.3.1-node4/log/system.log|grep \"ERROR\\|put block\\|all necessary")
	result1, _ := cmd1.CombinedOutput()
	result2, _ := cmd2.CombinedOutput()
	result3, _ := cmd3.CombinedOutput()
	result4, _ := cmd4.CombinedOutput()
	fmt.Printf("%s\n", string(result1)+string(result2)+string(result3)+string(result4))
	return string(result1) + string(result2) + string(result3) + string(result4)
}
