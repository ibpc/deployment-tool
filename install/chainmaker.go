package install

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"project/command"
	"project/info"
	"project/scan"
	"strconv"
	"strings"
	"time"
)

// var Userconfig info.Userconfig

var HardwareAll info.Hardware

var Userconfig info.Userconfig_json
var UserconfigJson info.Userconfig_json
var SoftwareAll info.Software
var RecommendedConfig info.RecommendedConfig
var Log string
var Url info.Html_url

// 刷新所有shell脚本，保证脚本使用LF换行,拥有可执行权限
func Update_shell() {
	cmd := fmt.Sprintf("chmod -R 777 scripts")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/backup.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-build.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-build1.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-build1-0.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-build1-1.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-build1-2.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-build2.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-setup_ftp.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-setup_scp.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-stop_scp.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-test.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/binary-test_pk.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/build_release_demo.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-build1.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-build2.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-build3.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-build3-0.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-build3-1.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-build3-2.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-build4.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-connectNode.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-scanHardware.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-scanSoftware.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-setup_docker.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-setup_ftp.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-setup_scp.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-test.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-test_pk.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/install_ftp.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/offline-build.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/set_apt_source_1804.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-create_swarm.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-setup_docker_pk.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-setup_solo_docker_pk.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-setup_solo_docker.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/chainmaker-stop_scp.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/Docker_get_data_log.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/multi_docker_chain_stop.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/multi_check_portListen.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/multi_check_process.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/multi_CmdGet_log.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/TestNode_sudoPermission.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/Node_Centos_InstallDocker.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/Node_Ubuntu_InstallDocker.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

	cmd = fmt.Sprintf("sed -i 's/\r//' scripts/Node_Kylin_InstallDocker.sh")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
	}

}

// 一键下载、编译、配置、部署长安链
func Test_initChangAnLian(InitMessage *info.InitMessage) int {
	InitMessage.ErrorFlag = 0
	InitMessage.OKFlag = 0

	Log = ""
	Update_shell()

	if Userconfig.DeploymentType == 2 {

		fmt.Printf("---------------------------------------部署step1 下载长安链------------------------------------------------\n")
		Log += fmt.Sprintf("---------------------------------------部署step1 下载长安链------------------------------------------------\n")
		InitMessage.ErrorFlag += Binary_download()
		(InitMessage.Log) = Log

		fmt.Printf("---------------------------------------部署step2.1生成二进制部署脚本----------------------------------------\n")
		Log += fmt.Sprintf("---------------------------------------部署step2.1生成二进制部署脚本----------------------------------------\n")
		InitMessage.ErrorFlag += Binary_generate_build_release()
		(InitMessage.Log) = Log
		InitMessage.ErrorFlag += Binary(SoftwareAll.HostSoftware)
		(InitMessage.Log) = Log
		Url = info.GetUrlFromJson()
		InitMessage.ErrorFlag += Release_load(Url)
		(InitMessage.Log) = Log
		InitMessage.ErrorFlag += Binary_build()
		(InitMessage.Log) = Log
		fmt.Printf("---------------------------------------部署step2.2二进制部署长安链----------------------------------------\n")
		Log += fmt.Sprintf("---------------------------------------部署step2.2二进制部署长安链----------------------------------------\n")
		if Userconfig.NodeNum > 0 {
			if Userconfig.CopyType == 0 {
				fmt.Printf("---------------------------------------部署step2.3 二进制部署长安链(scp方式)----------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署step3.3 二进制部署长安链(scp方式)----------------------------------------\n")
				InitMessage.ErrorFlag += Binary_generate_shell_scp()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Binary_setup_scp()
				(InitMessage.Log) = Log
			} else {
				fmt.Printf("---------------------------------------部署step2.3 二进制部署长安链(ftp方式)-----------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署step2.3 二进制部署长安链(ftp方式)-----------------------------------------\n")
				InitMessage.ErrorFlag += Binary_generate_shell_ftp()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Binary_setup_ftp()
				(InitMessage.Log) = Log
			}

			fmt.Printf("---------------------------------------部署示例合约------------------------------------------------\n")
			Log += fmt.Sprintf("---------------------------------------部署示例合约------------------------------------------------\n")
			InitMessage.ErrorFlag += Binary_test()
			(InitMessage.Log) = Log

			fmt.Printf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
			Log += fmt.Sprintf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
			InitMessage.ErrorFlag += Binary_result()
			(InitMessage.Log) = Log
			InitMessage.ErrorFlag += Chainmaker_explorer_dockerStart()
			(InitMessage.Log) = Log
			InitMessage.ErrorFlag += Chainmaker_explorer_AotoSubscribe()
			(InitMessage.Log) = Log

		} else {
			fmt.Printf("---------------------------------------部署step3 启动长安链------------------------------------------------\n")
			Log += fmt.Sprintf("-------------------------------部署step3 启动长安链------------------------------------------------\n")
			InitMessage.ErrorFlag += Binary_start()
			(InitMessage.Log) = Log

			time.Sleep(10 * time.Second) // 等待10s，保证启动完成

			fmt.Printf("---------------------------------------部署step4 查看节点启动情况------------------------------------------------\n")
			Log += fmt.Sprintf("-------------------------------部署step4 查看节点启动情况------------------------------------------------\n")
			InitMessage.ErrorFlag += Binary_display(Userconfig)
			(InitMessage.Log) = Log

			fmt.Printf("---------------------------------------部署示例合约------------------------------------------------\n")
			Log += fmt.Sprintf("---------------------------------------部署示例合约------------------------------------------------\n")
			InitMessage.ErrorFlag += Binary_test()
			(InitMessage.Log) = Log

			fmt.Printf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
			Log += fmt.Sprintf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
			InitMessage.ErrorFlag += Binary_result()
			(InitMessage.Log) = Log
			InitMessage.ErrorFlag += Chainmaker_explorer_dockerStart()
			(InitMessage.Log) = Log
			InitMessage.ErrorFlag += Chainmaker_explorer_AotoSubscribe()
			(InitMessage.Log) = Log
		}

	} else {

		fmt.Printf("---------------------------------------部署step1.1 下载长安链------------------------------------------------\n")
		Log += fmt.Sprintf("-------------------------------部署step1.1 下载长安链------------------------------------------------\n")
		InitMessage.ErrorFlag += Chain_download()
		(InitMessage.Log) = Log

		fmt.Printf("---------------------------------------部署step1.2 编译长安链------------------------------------------------\n")
		Log += fmt.Sprintf("-------------------------------部署step1.2 编译长安链------------------------------------------------\n")
		InitMessage.ErrorFlag += Chain_build()
		(InitMessage.Log) = Log

		if Userconfig.DeploymentType == 0 {
			// 命令行部署长安链
			if Userconfig.NodeNum > 0 {
				// 多点部署
				if Userconfig.CopyType == 0 {
					fmt.Printf("---------------------------------------部署step1.3 命令行部署长安链(scp方式)----------------------------------------\n")
					Log += fmt.Sprintf("-------------------------------部署step1.3 命令行部署长安链(scp方式)------------------------------------------------\n")
					InitMessage.ErrorFlag += Chain_generate_shell_scp()
					(InitMessage.Log) = Log
					fmt.Printf("---------------------------------------部署step2 启动长安链------------------------------------------------\n")
					Log += fmt.Sprintf("---------------------------------------部署step2 启动长安链------------------------------------------------\n")
					InitMessage.ErrorFlag += Chain_setup_scp()
					(InitMessage.Log) = Log
					InitMessage.ErrorFlag += Generate_shell_cmdMulti_data_log()
					(InitMessage.Log) = Log
				} else {
					fmt.Printf("---------------------------------------部署step2.3 命令行部署长安链(ftp方式)-----------------------------------------\n")
					Log += fmt.Sprintf("-------------------------------部署step2.3 命令行部署长安链(ftp方式)------------------------------------------------\n")
					InitMessage.ErrorFlag += Chain_generate_shell_ftp()
					(InitMessage.Log) = Log
					fmt.Printf("---------------------------------------部署step3 启动长安链------------------------------------------------\n")
					Log += fmt.Sprintf("---------------------------------------部署step3 启动长安链------------------------------------------------\n")
					InitMessage.ErrorFlag += Chain_setup_ftp()
					(InitMessage.Log) = Log
				}
				fmt.Printf("---------------------------------------部署step4 查看节点启动情况------------------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署step4 查看节点启动情况------------------------------------------------\n")
				InitMessage.ErrorFlag += Check_multi_process()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Check_port_listening()
				(InitMessage.Log) = Log
				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				InitMessage.ErrorFlag += Chain_test()
				(InitMessage.Log) = Log
			} else {

				fmt.Printf("---------------------------------------部署step5 启动长安链------------------------------------------------\n")
				Log += fmt.Sprintf("-------------------------------部署step5 启动长安链------------------------------------------------\n")
				InitMessage.ErrorFlag += Chain_start()
				(InitMessage.Log) = Log

				time.Sleep(10 * time.Second) // 等待10s，保证启动完成

				fmt.Printf("---------------------------------------部署step6 查看节点启动情况------------------------------------------------\n")
				Log += fmt.Sprintf("-------------------------------部署step6 查看节点启动情况------------------------------------------------\n")
				InitMessage.ErrorFlag += Chain_display(Userconfig)
				(InitMessage.Log) = Log

				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				InitMessage.ErrorFlag += Chain_test()
				(InitMessage.Log) = Log

			}
		} else {
			// docker部署长安链
			fmt.Printf("---------------------------------------部署step1.3 docker部署长安链------------------------------------------------\n")
			Log += fmt.Sprintf("-------------------------------部署step1.3 docker部署长安链------------------------------------------------\n")
			command.CommandExec("chmod -R 777 ./chainmaker-go")
			InitMessage.ErrorFlag += ChangeVersion()
			(InitMessage.Log) = Log
			InitMessage.ErrorFlag += Docker_file_prepare()
			(InitMessage.Log) = Log
			fmt.Printf("启动docker服务...\n")
			Log += fmt.Sprintf("启动docker服务...\n")
			command.CommandExec("systemctl stop firewalld.service")
			command.CommandExec("systemctl start docker")
			if Userconfig.NodeNum > 0 {
				// 多点部署
				fmt.Printf("正在为子节点配置docker环境...\033[1;34;40m耗时较长,请耐心等待\033[0m\n")
				Log += fmt.Sprintf("正在为子节点配置docker环境...\033[1;34;40m耗时较长,请耐心等待\033[0m\n")
				InitMessage.ErrorFlag += Check_host_docker_image()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Node_U_install_docker_shell()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Node_C_install_docker_shell()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Node_K_install_docker_shell()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Instal_docker_for_node()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Pack_docker_image()
				(InitMessage.Log) = Log
				command.CommandExec("chmod -R 777 ./chainmaker-go")
				InitMessage.ErrorFlag += Chain_setup_docker()
				(InitMessage.Log) = Log
				fmt.Printf("正在创建swarm集群...\n")
				Log += fmt.Sprintf("正在创建swarm集群...\n")
				InitMessage.ErrorFlag += Chain_generate_shell_swarm()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Join_docker_swarm()
				(InitMessage.Log) = Log
				fmt.Printf("加入集群完成\n")
				Log += fmt.Sprintf("加入集群完成\n")
				InitMessage.ErrorFlag += Chain_generate_shell_docker()
				(InitMessage.Log) = Log
				fmt.Printf("---------------------------------------部署step2 启动长安链------------------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署step2 启动长安链------------------------------------------------\n")
				command.CommandExec("chmod -R 777 ./chainmaker-go")
				InitMessage.ErrorFlag += Chain_start_docker()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Generate_shell_dockerMulti_data_log()
				(InitMessage.Log) = Log
			} else {
				//docker单点部署，部署在主节点上
				InitMessage.ErrorFlag += Chain_generate_shell_solo_docker()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Chain_setup_solo_docker()
				(InitMessage.Log) = Log
				fmt.Printf("---------------------------------------部署step2 启动长安链------------------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署step2 启动长安链------------------------------------------------\n")
				InitMessage.ErrorFlag += Chain_start_solo_docker()
				(InitMessage.Log) = Log
			}
			fmt.Printf("---------------------------------------部署step3 查看节点启动情况------------------------------------------------\n")
			Log += fmt.Sprintf("---------------------------------------部署step3 查看节点启动情况------------------------------------------------\n")
			if Userconfig.NodeNum > 0 {
				InitMessage.ErrorFlag += Check_multi_process()
				(InitMessage.Log) = Log
				InitMessage.ErrorFlag += Check_port_listening()
				(InitMessage.Log) = Log
				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				InitMessage.ErrorFlag += Chain_test()
				(InitMessage.Log) = Log
			} else {
				InitMessage.ErrorFlag += Chain_display(Userconfig)
				(InitMessage.Log) = Log
				fmt.Printf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				Log += fmt.Sprintf("---------------------------------------部署：部署示例合约------------------------------------------------\n")
				InitMessage.ErrorFlag += Chain_test()
				(InitMessage.Log) = Log
			}
		}
		fmt.Printf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
		Log += fmt.Sprintf("---------------------------------------全部完成，部署结果如下：------------------------------------------------\n")
		InitMessage.ErrorFlag += Chain_result()
		(InitMessage.Log) = Log
		InitMessage.ErrorFlag += Chainmaker_explorer_dockerStart()
		(InitMessage.Log) = Log
		InitMessage.ErrorFlag += Chainmaker_explorer_AotoSubscribe()
		(InitMessage.Log) = Log

	}

	fmt.Printf("Test_initChangAnLian函数执行完毕\n")
	Log += fmt.Sprintf("长安链部署执行完毕\n")

	fmt.Printf("OKFlag=%d ErrorFlag=%d\n", InitMessage.OKFlag, InitMessage.ErrorFlag)
	if InitMessage.ErrorFlag == 0 {
		InitMessage.OKFlag = 1
	} else {
		InitMessage.ErrorFlag = 1
	}
	fmt.Printf("OKFlag=%d ErrorFlag=%d\n", InitMessage.OKFlag, InitMessage.ErrorFlag)

	return 0
}

// 一键验证部署是否成功
func Chain_test() int {
	// 执行部署脚本，部署示例合约，验证长安链
	cmd1 := fmt.Sprintf("pwd")
	current_dir := command.CommandExec(cmd1)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", current_dir)

	if Userconfig.IdentityMode == 0 {
		//备份sdk_config.yml配置文件
		cmd1 := fmt.Sprintf("cp ./chainmaker-go/tools/cmc/testdata/sdk_config.yml ./chainmaker-go/tools/cmc/testdata/sdk_config.demo.yml")
		result1 := command.CommandExec(cmd1)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result1)

		//修改sdk_config.yml配置文件
		fmt.Printf("修改sdk_config.yml\n")
		Log += fmt.Sprintf("修改sdk_config.yml\n")
		fieName := fmt.Sprintf("./chainmaker-go/tools/cmc/testdata/sdk_config.yml")
		if Userconfig.NodeNum > 0 {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.Nodes[0].NodeIpAddr, Userconfig.Nodes[0].NodeRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %d", Userconfig.NodeNum)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		} else {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %s", Userconfig.Node_cnt)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		}
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
		fmt.Printf("部署示例合约...\n")
		Log += fmt.Sprintf("部署示例合约...\n")
		cmd := fmt.Sprintf("./scripts/chainmaker-test.sh")
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)

		//初始化sdk_config.yml配置文件
		cmd = fmt.Sprintf("mv ./chainmaker-go/tools/cmc/testdata/sdk_config.demo.yml ./chainmaker-go/tools/cmc/testdata/sdk_config.yml")
		result = command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)

	} else if Userconfig.IdentityMode == 1 {
		//备份sdk_config_pk.yml配置文件
		cmd1 := fmt.Sprintf("cp ./chainmaker-go/tools/cmc/testdata/sdk_config_pk.yml ./chainmaker-go/tools/cmc/testdata/sdk_config_pk.demo.yml")
		result1 := command.CommandExec(cmd1)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result1)

		//修改sdk_config_pk.yml配置文件
		fmt.Printf("修改sdk_config_pk.yml\n")
		Log += fmt.Sprintf("修改sdk_config_pk.yml\n")
		fieName := fmt.Sprintf("./chainmaker-go/tools/cmc/testdata/sdk_config_pk.yml")
		if Userconfig.NodeNum > 0 {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.Nodes[0].NodeIpAddr, Userconfig.Nodes[0].NodeRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %d", Userconfig.NodeNum)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		} else {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %s", Userconfig.Node_cnt)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		}
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
		fmt.Printf("部署示例合约...\n")
		Log += fmt.Sprintf("部署示例合约...\n")
		cmd := fmt.Sprintf("./scripts/chainmaker-test_pk.sh")
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)

		//初始化sdk_config_pk.yml配置文件
		cmd = fmt.Sprintf("mv ./chainmaker-go/tools/cmc/testdata/sdk_config_pk.demo.yml ./chainmaker-go/tools/cmc/testdata/sdk_config_pk.yml")
		result = command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
	}

	return 0
}

// install模块的UserConfig初始化
func SetUserConfig(data info.Userconfig_json) {
	if data.ChainmakerVersion != "" {
		Userconfig.ChainmakerVersion = data.ChainmakerVersion
	}
	if data.ChainmakerUsername != "" {
		Userconfig.ChainmakerUsername = data.ChainmakerUsername
	}
	if data.ChainmakerPassword != "" {
		Userconfig.ChainmakerPassword = data.ChainmakerPassword
	}
	if data.HostIpAddr != "" {
		Userconfig.HostIpAddr = data.HostIpAddr
	}
	//if data.HostDockerIp != "" {
	//	Userconfig.HostDockerIp = data.HostDockerIp
	//}
	if data.HostP2PPort != "" {
		Userconfig.HostP2PPort = data.HostP2PPort
	}
	if data.HostRPCPort != "" {
		Userconfig.HostRPCPort = data.HostRPCPort
	}
	if data.HostRootPassword != "" {
		Userconfig.HostRootPassword = data.HostRootPassword
	}
	if data.FtpUserName != "" {
		Userconfig.FtpUserName = data.FtpUserName
	}
	if data.FtpPassword != "" {
		Userconfig.FtpPassword = data.FtpPassword
	}
	if data.FtpPath != "" {
		Userconfig.FtpPath = data.FtpPath
	}
	if data.DeploymentType != -1 {
		Userconfig.DeploymentType = data.DeploymentType
	}
	if data.CopyType != -1 {
		Userconfig.CopyType = data.CopyType
	}
	if data.IdentityMode != -1 {
		Userconfig.IdentityMode = data.IdentityMode
	}
	if data.Chainmaker_explorer != -1 {
		Userconfig.Chainmaker_explorer = data.Chainmaker_explorer
	}
	if data.Way_start_explore != -1 {
		Userconfig.Way_start_explore = data.Way_start_explore
	}
	if data.Consensus_type != "" {
		Userconfig.Consensus_type = data.Consensus_type
	}
	if data.Log_level != "" {
		Userconfig.Log_level = data.Log_level
	}
	if data.Hash_type != "" {
		Userconfig.Hash_type = data.Hash_type
	}
	if data.Vm_go != "" {
		Userconfig.Vm_go = data.Vm_go
	}
	if data.Node_cnt != "" {
		Userconfig.Node_cnt = data.Node_cnt
	}
	if data.Chain_cnt != "" {
		Userconfig.Chain_cnt = data.Chain_cnt
	}
	//if data.Server_node_cnt != "" {
	//	Userconfig.Server_node_cnt = data.Server_node_cnt
	//}
	if data.Vm_go_transport_protocol != "" {
		Userconfig.Vm_go_transport_protocol = data.Vm_go_transport_protocol
	}
	if data.Vm_go_log_level != "" {
		Userconfig.Vm_go_log_level = data.Vm_go_log_level
	}
	if data.Block_height != "" {
		Userconfig.Block_height = data.Block_height
	}
	if data.Block_size != "" {
		Userconfig.Block_size = data.Block_size
	}
	if data.RecommendedConfigLevel != -1 {
		Userconfig.RecommendedConfigLevel = data.RecommendedConfigLevel
	}
	if data.NodeNum != -1 {
		Userconfig.NodeNum = data.NodeNum
	}

	for i := 0; i < Userconfig.NodeNum; i++ {
		if data.Nodes[i].NodeIpAddr != "" {
			Userconfig.Nodes[i].NodeIpAddr = data.Nodes[i].NodeIpAddr
		}
		//if data.Nodes[i].NodeDockerIp != "" {
		//	Userconfig.Nodes[i].NodeDockerIp = data.Nodes[i].NodeDockerIp
		//}
		if data.Nodes[i].NodeP2PPort != "" {
			Userconfig.Nodes[i].NodeP2PPort = data.Nodes[i].NodeP2PPort
		}
		if data.Nodes[i].NodeRPCPort != "" {
			Userconfig.Nodes[i].NodeRPCPort = data.Nodes[i].NodeRPCPort
		}
		if data.Nodes[i].NodeUser != "" {
			Userconfig.Nodes[i].NodeUser = data.Nodes[i].NodeUser
		}
		if data.Nodes[i].NodePassword != "" {
			Userconfig.Nodes[i].NodePassword = data.Nodes[i].NodePassword
		}
	}
}

// 读取user_config.ini，获取用户配置信息，初始化install模块的UserConfig
func SetUserconfigFromIni() {
	Userconfig = info.GetUserconfigFromIni()
}

// 下载长安链源码
func Chain_download() int {
	//临时把用户名中的@用%40来替换
	username := strings.Replace(Userconfig.ChainmakerUsername, "@", "%40", -1)
	fmt.Printf("即将使用以下命令下载长安链: git clone -b %s  https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git\n", Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)

	// 扫描代码是否已经提前下载
	result := command.CommandExec("ls")
	start := strings.Index(string(result), "chainmaker-cryptogen")
	if start == -1 {
		fmt.Printf("开始下载chainmaker-cryptogen...\n")
		Log += fmt.Sprintf("开始下载chainmaker-cryptogen...\n")
		fmt.Printf("git clone --depth=1  https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git\n", username, Userconfig.ChainmakerPassword)
		Log += fmt.Sprintf("git clone --depth=1  https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git\n", username, Userconfig.ChainmakerPassword)

		cmd := fmt.Sprintf("git clone --depth=1 https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git", username, Userconfig.ChainmakerPassword)
		//git clone  https://wangjin:qwer1234.@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git
		//result := command.CommandExec(cmd)

		result := exec.Command("/bin/sh", "-c", cmd)
		out, err := result.Output()

		if err != nil {
			fmt.Printf("nil:%s\n", err)
			Log += fmt.Sprintf("nil:%s\n", err)
		}
		fmt.Printf("%s\n", out)
		Log += fmt.Sprintf("%s\n", out)

	} else {
		fmt.Printf("已存在chainmaker-cryptogen文件夹\n")
		Log += fmt.Sprintf("已存在chainmaker-cryptogen文件夹\n")
	}

	start = strings.Index(string(result), "chainmaker-go")
	if start == -1 {
		fmt.Printf("开始下载chainmaker-go...\n")
		Log += fmt.Sprintf("开始下载chainmaker-go...\n")
		// TODO if else 选择2.3.0或2.3.1
		fmt.Printf("git clone -b %s https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git\n", Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)
		Log += fmt.Sprintf("git clone -b %s https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git\n", Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)
		cmd := fmt.Sprintf("git config --global http.postBuffer 2000000 && git clone -b %s https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git", Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)
		//git clone  https://wangjin:qwer1234.@git.chainmaker.org.cn/chainmaker/chainmaker-go.git
		//result := CommandExec(cmd)

		result := exec.Command("/bin/sh", "-c", cmd)
		out, err := result.Output()

		if err != nil {
			fmt.Printf("nil:%s\n", err)
			Log += fmt.Sprintf("nil:%s\n", err)
		}
		fmt.Printf("%s\n", out)
		Log += fmt.Sprintf("%s\n", out)
	} else {
		// chainmaker-go文件夹已存在，使用git分支切换至和ini配置文件中ChainmakerVersion一致的版本
		cmd := fmt.Sprintf("cd chainmaker-go;git checkout -f %s", Userconfig.ChainmakerVersion)
		result := exec.Command("/bin/sh", "-c", cmd)
		out, err := result.Output()
		if err != nil {
			fmt.Printf("nil:%s\n", err)
			Log += fmt.Sprintf("nil:%s\n", err)
		}
		Log += fmt.Sprintf("%s\n", out)
		fmt.Printf("已存在相应版本chainmaker-go文件夹\n")
		Log += fmt.Sprintf("已存在相应版本chainmaker-go文件夹\n")
	}

	// 扫描代码是否已经下载完成
	result = command.CommandExec("ls")
	start = strings.Index(string(result), "chainmaker-cryptogen")
	if start == -1 {
		fmt.Printf("ERROR! chainmaker-cryptogen下载失败\n")
		Log += fmt.Sprintf("ERROR! chainmaker-cryptogen下载失败\n")
	} else {
		fmt.Printf("chainmaker-cryptogen已存在或下载完成\n")
		Log += fmt.Sprintf("chainmaker-cryptogen已存在或下载完成\n")
	}

	start = strings.Index(string(result), "chainmaker-go")
	if start == -1 {
		fmt.Printf("ERROR! 下载chainmaker-go下载失败\n")
		Log += fmt.Sprintf("ERROR! 下载chainmaker-go下载失败\n")
	} else {
		fmt.Printf("chainmaker-go已存在或下载完成\n")
		Log += fmt.Sprintf("chainmaker-go已存在或下载完成\n")
	}

	return 0
}

// 二进制下载长安链源码
func Binary_download() int {
	//临时把用户名中的@用%40来替换
	username := strings.Replace(Userconfig.ChainmakerUsername, "@", "%40", -1)
	fmt.Printf("即将使用以下命令下载长安链: git clone -b %s https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git\n", Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)

	// 扫描代码是否已经提前下载
	result := command.CommandExec("ls")
	start := strings.Index(string(result), "chainmaker-cryptogen")
	if start == -1 {
		fmt.Printf("开始下载chainmaker-cryptogen...\n")
		Log += fmt.Sprintf("开始下载chainmaker-cryptogen...\n")
		fmt.Printf("git clone https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git\n", username, Userconfig.ChainmakerPassword)
		Log += fmt.Sprintf("git clone https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git\n", username, Userconfig.ChainmakerPassword)

		cmd := fmt.Sprintf("git config --global http.postBuffer 2000000 && git clone https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git", username, Userconfig.ChainmakerPassword)
		//git clone  https://wangjin:qwer1234.@git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen.git
		//result := command.CommandExec(cmd)

		result := exec.Command("/bin/sh", "-c", cmd)
		out, err := result.Output()

		if err != nil {
			fmt.Printf("nil:%s\n", err)
			Log += fmt.Sprintf("nil:%s\n", err)
		}
		fmt.Printf("%s\n", out)
		Log += fmt.Sprintf("%s\n", out)

	} else {
		fmt.Printf("已存在chainmaker-cryptogen文件夹\n")
		Log += fmt.Sprintf("已存在chainmaker-cryptogen文件夹\n")
	}

	start = strings.Index(string(result), "chainmaker-go")
	if start == -1 {
		fmt.Printf("开始下载chainmaker-go...\n")
		Log += fmt.Sprintf("开始下载chainmaker-go...\n")
		// TODO if else 选择2.3.0或2.3.1
		fmt.Printf("git clone -b %s https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git\n", Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)
		Log += fmt.Sprintf("git clone -b %s https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git\n", Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)
		cmd := fmt.Sprintf("git config --global http.postBuffer 2000000 && git clone -b %s https://%s:%s@git.chainmaker.org.cn/chainmaker/chainmaker-go.git",
			Userconfig.ChainmakerVersion, username, Userconfig.ChainmakerPassword)
		//git clone  https://wangjin:qwer1234.@git.chainmaker.org.cn/chainmaker/chainmaker-go.git
		//result := CommandExec(cmd)

		result := exec.Command("/bin/sh", "-c", cmd)
		out, err := result.Output()

		if err != nil {
			fmt.Printf("nil:%s\n", err)
			Log += fmt.Sprintf("nil:%s\n", err)
		}
		fmt.Printf("%s\n", out)
		Log += fmt.Sprintf("%s\n", out)
	} else {

		fmt.Printf("已存在chainmaker-go文件夹\n")
		Log += fmt.Sprintf("已存在chainmaker-go文件夹\n")
	}

	// 扫描代码是否已经下载完成
	result = command.CommandExec("ls")
	start = strings.Index(string(result), "chainmaker-cryptogen")
	if start == -1 {
		fmt.Printf("ERROR! chainmaker-cryptogen下载失败\n")
		Log += fmt.Sprintf("ERROR! chainmaker-cryptogen下载失败\n")
	} else {
		fmt.Printf("chainmaker-cryptogen已存在或下载完成\n")
		Log += fmt.Sprintf("chainmaker-cryptogen已存在或下载完成\n")
	}

	start = strings.Index(string(result), "chainmaker-go")
	if start == -1 {
		fmt.Printf("ERROR! 下载chainmaker-go下载失败\n")
		Log += fmt.Sprintf("ERROR! 下载chainmaker-go下载失败\n")
	} else {
		fmt.Printf("chainmaker-go已存在或下载完成\n")
		Log += fmt.Sprintf("chainmaker-go已存在或下载完成\n")
	}

	return 0
}

// 在文件中替换某一行内容，旧文件备份
func ReplaceFile(fileName string, oldStr string, NewStr string) int {
	// 打开旧文件
	in, err := os.Open(fileName)
	if err != nil {
		fmt.Println("open file fail:", err)
		os.Exit(-1)
	}
	//defer关闭文件
	defer in.Close()

	// 打开新文件
	out, err := os.OpenFile(fileName+".new", os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		fmt.Println("Open write file fail:", err)
		os.Exit(-1)
	}
	//defer关闭文件
	defer out.Close()

	//读取文件内容到io中
	br := bufio.NewReader(in)
	index := 1
	for {
		//读取每一行内容
		line, _, err := br.ReadLine()
		//读到末尾
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println("read err:", err)
			os.Exit(-1)
		}
		//根据关键词覆盖当前行
		if strings.Contains(string(line), oldStr) {
			newLine := strings.Replace(string(line), oldStr, NewStr, -1)
			_, err = out.WriteString(newLine + "\n")
		} else {
			newLine := string(line)
			_, err = out.WriteString(newLine + "\n")
		}

		if err != nil {
			fmt.Println("write to file fail:", err)
			os.Exit(-1)
		}
		//fmt.Println("done ", index)
		index++
	}
	//fmt.Println("FINISH!")

	// 新文件替换旧文件
	cmd := fmt.Sprintf("mv %s %s", fileName, fileName+".old")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	cmd = fmt.Sprintf("mv %s %s", fileName+".new", fileName)
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	return 0
}

// 旧文件不变，替换某多行内容，写入新文件
func GenerateFile(fileName string, newFileName string, strNum int, oldStr [10]string, NewStr [10]string) int {
	// 生成新文件
	cmd := fmt.Sprintf("cp %s %s", fileName, newFileName)
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	// 打开旧文件
	in, err := os.Open(fileName)
	if err != nil {
		fmt.Println("open file fail:", err)
		os.Exit(-1)
	}
	//defer关闭文件
	defer in.Close()

	// 打开新文件
	out, err := os.OpenFile(newFileName, os.O_RDWR|os.O_TRUNC, 0777)
	if err != nil {
		fmt.Println("Open write file fail:", err)
		os.Exit(-1)
	}
	//defer关闭文件
	defer out.Close()

	//读取文件内容到io中
	br := bufio.NewReader(in)
	index := 1
	for {
		//读取每一行内容
		line, _, err := br.ReadLine()
		//读到末尾
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println("read err:", err)
			os.Exit(-1)
		}
		//根据关键词覆盖当前行
		match := 0
		for j := 0; j < strNum; j++ {
			if strings.Contains(string(line), oldStr[j]) {
				newLine := strings.Replace(string(line), oldStr[j], NewStr[j], -1)
				_, err = out.WriteString(newLine + "\n")
				match = 1
				break
			}
		}

		if match == 0 {
			newLine := string(line)
			_, err = out.WriteString(newLine + "\n")
		}

		if err != nil {
			fmt.Println("write to file fail:", err)
			os.Exit(-1)
		}

		//fmt.Println("done ", index)
		index++
	}
	//fmt.Println("FINISH!")

	return 0
}

// 修改脚本文件中指定一行的内容
func ModifyFile(fileName string, key string, value string, a int) {
	//读写方式打开文件
	file, err := os.OpenFile(fileName, os.O_RDWR, 0777)
	if err != nil {
		fmt.Println("open file filed.", err)
		return
	}

	//defer关闭文件
	defer file.Close()

	//获取文件大小
	// stat, err := file.Stat()
	// if err != nil {
	// 	panic(err)
	// }
	// var size = stat.Size()
	// fmt.Println("file size:", size)
	//读取文件内容到io中
	reader := bufio.NewReader(file)
	pos := int64(0)
	for {
		//读取每一行内容
		line, err := reader.ReadString('\n')
		if err != nil {
			//读到末尾
			if err == io.EOF {
				// fmt.Println("[INFO]File Modify OK!", key, value)
				break
			} else {
				fmt.Println("[ERR]Read file error!", err)
				return
			}
		}
		//fmt.Println(line)

		//根据关键词覆盖当前行
		if strings.Contains(line, key) {
			if a == 0 {
				// value 不加引号
				bytes := []byte(key + value + "\n")
				file.WriteAt(bytes, pos)
				fmt.Printf("[INFO]%s %d: %s\n", fileName, pos, bytes)
			} else {
				// value 两侧加引号
				bytes := []byte(key + "\"" + value + "\"" + "\n")
				file.WriteAt(bytes, pos)
				fmt.Printf("[INFO]%s %d: %s\n", fileName, pos, bytes)
			}

		}

		//每一行读取完后记录位置
		pos += int64(len(line))
	}
}

// 二进制配置节点生成证书，打包生成安装
func Binary_build() int {
	RecommendedConfig = info.GetRecommendedConfigFromIni()

	// 第1步：通过prepare.sh配置节点，生成证书
	if Userconfig.IdentityMode == 0 {
		// fmt.Printf("[test]vm_go = %s %s\n", Userconfig.Vm_go_transport_protocol, Userconfig.Vm_go_log_level)
		ModifyFile("scripts/binary-build1-0.sh", "set P2P_PORT ", Userconfig.HostP2PPort, 0)
		ModifyFile("scripts/binary-build1-0.sh", "set RPC_PORT ", Userconfig.HostRPCPort, 0)
		ModifyFile("scripts/binary-build1-0.sh", "set consensus_type ", Userconfig.Consensus_type, 0)
		ModifyFile("scripts/binary-build1-0.sh", "set log_level ", Userconfig.Log_level, 1)
		ModifyFile("scripts/binary-build1-0.sh", "set hash_type ", Userconfig.Hash_type, 1)
		ModifyFile("scripts/binary-build1-0.sh", "set vm_go ", Userconfig.Vm_go, 1)
		ModifyFile("scripts/binary-build1-0.sh", "set vm_go_transport_protocol ", Userconfig.Vm_go_transport_protocol, 1)
		ModifyFile("scripts/binary-build1-0.sh", "set vm_go_log_level ", Userconfig.Vm_go_log_level, 1)
		ModifyFile("scripts/binary-build1-0.sh", "set node_cnt ", Userconfig.Node_cnt, 0)
		ModifyFile("scripts/binary-build1-0.sh", "set chain_cnt ", Userconfig.Chain_cnt, 0)
	} else if Userconfig.IdentityMode == 1 {
		ModifyFile("scripts/binary-build1-1.sh", "set P2P_PORT ", Userconfig.HostP2PPort, 0)
		ModifyFile("scripts/binary-build1-1.sh", "set RPC_PORT ", Userconfig.HostRPCPort, 0)
		ModifyFile("scripts/binary-build1-1.sh", "set consensus_type ", Userconfig.Consensus_type, 0)
		ModifyFile("scripts/binary-build1-1.sh", "set log_level ", Userconfig.Log_level, 1)
		ModifyFile("scripts/binary-build1-1.sh", "set hash_type ", Userconfig.Hash_type, 1)
		ModifyFile("scripts/binary-build1-1.sh", "set vm_go ", Userconfig.Vm_go, 1)
		ModifyFile("scripts/binary-build1-1.sh", "set vm_go_transport_protocol ", Userconfig.Vm_go_transport_protocol, 1)
		ModifyFile("scripts/binary-build1-1.sh", "set vm_go_log_level ", Userconfig.Vm_go_log_level, 1)
		ModifyFile("scripts/binary-build1-1.sh", "set node_cnt ", Userconfig.Node_cnt, 0)
		ModifyFile("scripts/binary-build1-1.sh", "set chain_cnt ", Userconfig.Chain_cnt, 0)
	} else {
		ModifyFile("scripts/binary-build1-2.sh", "set P2P_PORT ", Userconfig.HostP2PPort, 0)
		ModifyFile("scripts/binary-build1-2.sh", "set RPC_PORT ", Userconfig.HostRPCPort, 0)
		ModifyFile("scripts/binary-build1-2.sh", "set consensus_type ", Userconfig.Consensus_type, 0)
		ModifyFile("scripts/binary-build1-2.sh", "set log_level ", Userconfig.Log_level, 1)
		ModifyFile("scripts/binary-build1-2.sh", "set hash_type ", Userconfig.Hash_type, 1)
		ModifyFile("scripts/binary-build1-2.sh", "set vm_go ", Userconfig.Vm_go, 1)
		ModifyFile("scripts/binary-build1-2.sh", "set vm_go_transport_protocol ", Userconfig.Vm_go_transport_protocol, 1)
		ModifyFile("scripts/binary-build1-2.sh", "set vm_go_log_level ", Userconfig.Vm_go_log_level, 1)
		ModifyFile("scripts/binary-build1-2.sh", "set node_cnt ", Userconfig.Node_cnt, 0)
		ModifyFile("scripts/binary-build1-2.sh", "set chain_cnt ", Userconfig.Chain_cnt, 0)
	}

	var cmd string
	result := command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果

	if Userconfig.IdentityMode == 0 {
		fmt.Printf("开始配置prepare.sh\n")
		Log += fmt.Sprintf("开始配置prepare.sh \n")
	} else if Userconfig.IdentityMode == 1 {
		fmt.Printf("开始配置prepare_pk.sh\n")
		Log += fmt.Sprintf("开始配置prepare_pk.sh \n")
	}

	// 执行脚本
	if Userconfig.IdentityMode == 0 {
		cmd = fmt.Sprintf("cd scripts && ./backup.sh")
		cmd = fmt.Sprintf("./scripts/binary-build1-0.sh")
	} else if Userconfig.IdentityMode == 1 {
		cmd = fmt.Sprintf("./scripts/binary-build1-1.sh")
	} else {
		cmd = fmt.Sprintf("./scripts/binary-build1-2.sh")
	}
	//result = CommandExec(cmd)
	out := exec.Command("/bin/bash", "-c", cmd)
	result, err := out.Output()
	if err != nil {
	}
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	// 查看生成好的节点证书和配置
	cmd = fmt.Sprintf("ls -l binary/build/")
	result = command.CommandExec(cmd)

	if Userconfig.IdentityMode == 0 {
		fmt.Printf("完成prepare.sh配置\n")
		Log += fmt.Sprintf("完成prepare.sh配置\n")
	} else if Userconfig.IdentityMode == 1 {
		fmt.Printf("完成prepare_pk.sh配置\n")
		Log += fmt.Sprintf("完成prepare_pk.sh配置\n")
	}

	// 第2步：修改各节点对应chainmaker.yml,参考官网https://docs.chainmaker.org.cn/v2.3.0/html/instructions/多机部署.html
	fmt.Printf("修改chainmaker.yml\n")
	Log += fmt.Sprintf("修改chainmaker.yml\n")

	if Userconfig.NodeNum == 0 {
		k, err := strconv.Atoi(Userconfig.Node_cnt)
		_ = err
		for i := 0; i < k; i++ {
			// 修改IP & Port
			fieName := fmt.Sprintf("./binary/build/config/node%d/chainmaker.yml", i+1)
			for j := 0; j < k; j++ {
				oldIpPort := fmt.Sprintf("    - \"/ip4/127.0.0.1/tcp/")
				newIpPort := fmt.Sprintf("    - \"/ip4/%s/tcp/", Userconfig.HostIpAddr)
				ReplaceFile(fieName, oldIpPort, newIpPort)
			}
			// 修改unarchive_block_height
			cmd = fmt.Sprintf("sed -i '/.*unarchive_block_height:*/c\\  unarchive_block_height: %s' ./binary/build/config/node%d/chainmaker.yml",
				Userconfig.Block_height, i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改max_txpool_size
			cmd = fmt.Sprintf("sed -i '/.*max_txpool_size:*/c\\  max_txpool_size: %s' ./binary/build/config/node%d/chainmaker.yml",
				RecommendedConfig.MaxTxpoolSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改logdb_segment_size
			cmd = fmt.Sprintf("sed -i '/.*logdb_segment_size:*/c\\  logdb_segment_size: %s' ./binary/build/config/node%d/chainmaker.yml",
				RecommendedConfig.LogdbSegmentSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改hard_max_cache_size
			cmd = fmt.Sprintf("sed -i '/.*hard_max_cache_size:*/c\\  hard_max_cache_size: %s' ./binary/build/config/node%d/chainmaker.yml",
				RecommendedConfig.HardMaxCacheSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
	} else {
		for i := 0; i < Userconfig.NodeNum; i++ {
			// 修改IP & Port
			fieName := fmt.Sprintf("./binary/build/config/node%d/chainmaker.yml", i+1)
			for j := 0; j < Userconfig.NodeNum; j++ {
				oldIpPort := fmt.Sprintf("    - \"/ip4/127.0.0.1/tcp/%d", 11301+j)
				newIpPort := fmt.Sprintf("    - \"/ip4/%s/tcp/%s", Userconfig.Nodes[j].NodeIpAddr, Userconfig.Nodes[j].NodeP2PPort)
				//newIpPort := fmt.Sprintf("    - \"/dns/consensus1.tls.wx-org%d.chainmaker.org/tcp/1130%d", j+1, j+1)
				ReplaceFile(fieName, oldIpPort, newIpPort)
			}
			// 修改unarchive_block_height
			cmd = fmt.Sprintf("sed -i '/.*unarchive_block_height:*/c\\  unarchive_block_height: %s' ./binary/build/config/node%d/chainmaker.yml",
				Userconfig.Block_height, i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			//修改block_size
			//for j := 0; j < Userconfig.NodeNum; j++ {
			//	cmd = fmt.Sprintf("sed -i '/.*block_size:*/c\\  block_size: %s' ./binary/build/config/node%d/chainconfig/bc%d.yml",
			//		Userconfig.Block_size, i+1, j+1)
			//	result = command.CommandExec(cmd)
			//	if result == nil {
			//		fmt.Printf("ERROR to exec %s\n", cmd)
			//		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			//		return -1
			//	}
			//}

			// 修改block_tx_capacity
			//for j := 0; j < Userconfig.NodeNum; j++ {
			//	cmd = fmt.Sprintf("sed -i '/.*block_tx_capacity:*/c\\  block_tx_capacity: %s' ./binary/build/config/node%d/chainconfig/bc%d.yml",
			//		RecommendedConfig.BlockTxCapacity[Userconfig.RecommendedConfigLevel], i+1, j+1)
			//	result = command.CommandExec(cmd)
			//	if result == nil {
			//		fmt.Printf("ERROR to exec %s\n", cmd)
			//		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			//		return -1
			//	}
			//}

			// 修改max_txpool_size
			cmd = fmt.Sprintf("sed -i '/.*max_txpool_size:*/c\\  max_txpool_size: %s' ./binary/build/config/node%d/chainmaker.yml",
				RecommendedConfig.MaxTxpoolSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改block_interval
			//for j := 0; j < Userconfig.NodeNum; j++ {
			//	cmd = fmt.Sprintf("sed -i '/.*block_interval:*/c\\  block_interval: %s' ./binary/build/config/node%d/chainconfig/bc%d.yml",
			//		RecommendedConfig.BlockInterval[Userconfig.RecommendedConfigLevel], i+1, j+1)
			//	result = command.CommandExec(cmd)
			//	if result == nil {
			//		fmt.Printf("ERROR to exec %s\n", cmd)
			//		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			//		return -1
			//	}
			//}

			// 修改logdb_segment_size
			cmd = fmt.Sprintf("sed -i '/.*logdb_segment_size:*/c\\  logdb_segment_size: %s' ./binary/build/config/node%d/chainmaker.yml",
				RecommendedConfig.LogdbSegmentSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改hard_max_cache_size
			cmd = fmt.Sprintf("sed -i '/.*hard_max_cache_size:*/c\\  hard_max_cache_size: %s' ./binary/build/config/node%d/chainmaker.yml",
				RecommendedConfig.HardMaxCacheSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			fmt.Printf("%s modify OK!\n", fieName)
			Log += fmt.Sprintf("%s modify OK!\n", fieName)
		}
	}
	for i := 0; i < Userconfig.NodeNum; i++ {
		k, err := strconv.Atoi(Userconfig.Chain_cnt)
		_ = err
		//修改block_size
		for j := 0; j < k; j++ {
			cmd = fmt.Sprintf("sed -i '/.*block_size:*/c\\  block_size: %s' ./binary/build/config/node%d/chainconfig/bc%d.yml",
				Userconfig.Block_size, i+1, j+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
		// 修改block_tx_capacity
		for j := 0; j < k; j++ {
			cmd = fmt.Sprintf("sed -i '/.*block_tx_capacity:*/c\\  block_tx_capacity: %s' ./binary/build/config/node%d/chainconfig/bc%d.yml",
				RecommendedConfig.BlockTxCapacity[Userconfig.RecommendedConfigLevel], i+1, j+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
		// 修改block_interval
		for j := 0; j < k; j++ {
			cmd = fmt.Sprintf("sed -i '/.*block_interval:*/c\\  block_interval: %s' ./binary/build/config/node%d/chainconfig/bc%d.yml",
				RecommendedConfig.BlockInterval[Userconfig.RecommendedConfigLevel], i+1, j+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
	}
	fmt.Printf("链配置文件bc.yml配置完成\n")
	Log += fmt.Sprintf("链配置文件bc.yml配置完成\n")
	// 第5步：执行build_release.sh脚本，将编译chainmaker-go模块，并打包生成安装，存于路径chainmaker-go/build/release中
	// 5.1执行脚本
	fmt.Printf("执行build_release.sh脚本\n")
	Log += fmt.Sprintf("执行build_release.sh脚本\n")
	cmd = fmt.Sprintf("./scripts/binary-build2.sh")
	out = exec.Command("/bin/bash", "-c", cmd)
	result, err = out.Output()
	if err != nil {
		fmt.Printf("binary-build2执行失败,err不为0\n")
		Log += fmt.Sprintf("binary-build2执行失败,err不为0\n")
	}
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	// 5.2:查看生成好的节点证书和配置
	//fmt.Printf("查看生成好的节点证书和配置\n")
	if Userconfig.IdentityMode == 0 {
		fmt.Printf("查看生成好的节点证书和配置\n")
		Log += fmt.Sprintf("查看生成好的节点证书和配置\n")
	} else if Userconfig.IdentityMode == 1 {
		fmt.Printf("查看生成好的节点密钥和配置\n")
		Log += fmt.Sprintf("查看生成好的节点密钥和配置\n")
	}

	if Userconfig.NodeNum == 0 {
		var Node_cnt int
		Node_cnt, err = strconv.Atoi(Userconfig.Node_cnt)
		fmt.Printf("Node_cnt %s %d\n", Userconfig.Node_cnt, Node_cnt)
		Log += fmt.Sprintf("Node_cnt %s %d\n", Userconfig.Node_cnt, Node_cnt)
		for j := 0; j < Node_cnt; j++ {
			if Userconfig.IdentityMode == 0 {
				cmd = fmt.Sprintf("ls  binary/build/release |grep %d.chainmaker", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.HostFile[j] = string(result)
				index := strings.Index(Userconfig.HostFile[j], "chainmaker.org")
				Userconfig.HostFilePath[j] = Userconfig.HostFile[j][0 : index+14]
			} else if Userconfig.IdentityMode == 1 {
				cmd = fmt.Sprintf("ls  binary/build/release |grep node%d", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.HostFile[j] = string(result)
				index := strings.Index(Userconfig.HostFile[j], "node")
				Userconfig.HostFilePath[j] = Userconfig.HostFile[j][0 : index+5]
			}
		}
	} else {
		for j := 0; j < Userconfig.NodeNum; j++ {
			if Userconfig.IdentityMode == 0 {
				cmd = fmt.Sprintf("ls  binary/build/release |grep %d.chainmaker", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.Nodes[j].NodeFile = string(result)
				index := strings.Index(Userconfig.Nodes[j].NodeFile, "chainmaker.org")
				Userconfig.Nodes[j].NodeFilePath = Userconfig.Nodes[j].NodeFile[0 : index+14]
			} else if Userconfig.IdentityMode == 1 {
				cmd = fmt.Sprintf("ls  binary/build/release |grep node%d", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.Nodes[j].NodeFile = string(result)
				index := strings.Index(Userconfig.Nodes[j].NodeFile, "node")
				Userconfig.Nodes[j].NodeFilePath = Userconfig.Nodes[j].NodeFile[0 : index+5]
			}
		}
	}

	time.Sleep(2 * 1000 * time.Millisecond)
	cmd = fmt.Sprintf("ls  binary/build/release |grep chainmaker-%s-wx-org", Userconfig.ChainmakerVersion)
	result = command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	fmt.Printf("编译完成，生成以上安装包\n")
	Log += fmt.Sprintf("编译完成，生成以上安装包\n")
	cmd = fmt.Sprintf("ls -l binary/build/release/")
	result = command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result) //太长，先不打印
	Log += fmt.Sprintf("%s\n", result)

	return 0
}

// 编译长安链
func Chain_build() int {
	RecommendedConfig = info.GetRecommendedConfigFromIni()

	fmt.Printf("开始编译证书生成工具...\n")
	Log += fmt.Sprintf("开始编译证书生成工具...\n")

	// 第1步：编译证书生成工具
	cmd := fmt.Sprintf("./scripts/chainmaker-build1.sh")
	result := command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	fmt.Printf("软连接chainmaker-cryptogen到tools目录...\n")
	Log += fmt.Sprintf("软连接chainmaker-cryptogen到tools目录...\n")

	// 第2步：软连接chainmaker-cryptogen到tools目录下
	cmd = fmt.Sprintf("./scripts/chainmaker-build2.sh")
	//result = CommandExec(cmd)
	out := exec.Command("/bin/bash", "-c", cmd)
	result, err := out.Output()
	if err != nil {
	}
	// 通过返回，打印编译、配置结果
	// fmt.Printf("%s\n", result)

	// 第3步：通过prepare.sh配置节点，生成证书
	if Userconfig.IdentityMode == 0 {
		// fmt.Printf("[test]vm_go = %s %s\n", Userconfig.Vm_go_transport_protocol, Userconfig.Vm_go_log_level)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set P2P_PORT ", Userconfig.HostP2PPort, 0)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set RPC_PORT ", Userconfig.HostRPCPort, 0)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set consensus_type ", Userconfig.Consensus_type, 0)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set log_level ", Userconfig.Log_level, 1)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set hash_type ", Userconfig.Hash_type, 1)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set vm_go ", Userconfig.Vm_go, 1)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set vm_go_transport_protocol ", Userconfig.Vm_go_transport_protocol, 1)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set vm_go_log_level ", Userconfig.Vm_go_log_level, 1)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set node_cnt ", Userconfig.Node_cnt, 0)
		ModifyFile("scripts/chainmaker-build3-0.sh", "set chain_cnt ", Userconfig.Chain_cnt, 0)
	} else if Userconfig.IdentityMode == 1 {
		ModifyFile("scripts/chainmaker-build3-1.sh", "set P2P_PORT ", Userconfig.HostP2PPort, 0)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set RPC_PORT ", Userconfig.HostRPCPort, 0)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set consensus_type ", Userconfig.Consensus_type, 0)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set log_level ", Userconfig.Log_level, 1)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set hash_type ", Userconfig.Hash_type, 1)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set vm_go ", Userconfig.Vm_go, 1)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set vm_go_transport_protocol ", Userconfig.Vm_go_transport_protocol, 1)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set vm_go_log_level ", Userconfig.Vm_go_log_level, 1)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set node_cnt ", Userconfig.Node_cnt, 0)
		ModifyFile("scripts/chainmaker-build3-1.sh", "set chain_cnt ", Userconfig.Chain_cnt, 0)
	} else {
		ModifyFile("scripts/chainmaker-build3-2.sh", "set P2P_PORT ", Userconfig.HostP2PPort, 0)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set RPC_PORT ", Userconfig.HostRPCPort, 0)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set consensus_type ", Userconfig.Consensus_type, 0)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set log_level ", Userconfig.Log_level, 1)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set hash_type ", Userconfig.Hash_type, 1)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set vm_go ", Userconfig.Vm_go, 1)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set vm_go_transport_protocol ", Userconfig.Vm_go_transport_protocol, 1)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set vm_go_log_level ", Userconfig.Vm_go_log_level, 1)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set node_cnt ", Userconfig.Node_cnt, 0)
		ModifyFile("scripts/chainmaker-build3-2.sh", "set chain_cnt ", Userconfig.Chain_cnt, 0)
	}

	//fmt.Printf("开始配置prepare.sh\n")
	//Log += fmt.Sprintf("开始配置prepare.sh \n")
	if Userconfig.IdentityMode == 0 {
		fmt.Printf("开始配置prepare.sh\n")
		Log += fmt.Sprintf("开始配置prepare.sh \n")
	} else if Userconfig.IdentityMode == 1 {
		fmt.Printf("开始配置prepare_pk.sh\n")
		Log += fmt.Sprintf("开始配置prepare_pk.sh \n")
	}

	// 执行脚本
	if Userconfig.IdentityMode == 0 {
		cmd = fmt.Sprintf("./scripts/chainmaker-build3-0.sh")
	} else if Userconfig.IdentityMode == 1 {
		cmd = fmt.Sprintf("./scripts/chainmaker-build3-1.sh")
	} else {
		cmd = fmt.Sprintf("./scripts/chainmaker-build3-2.sh")
	}
	//result = CommandExec(cmd)
	out = exec.Command("/bin/bash", "-c", cmd)
	result, err = out.Output()
	if err != nil {
	}
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	// 查看生成好的节点证书和配置
	cmd = fmt.Sprintf("tree -L 3 chainmaker-go/build/")
	result = command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	//fmt.Printf("%s\n", result) // 太长，先不打印
	//fmt.Printf("完成prepare.sh配置\n")
	//Log += fmt.Sprintf("完成prepare.sh配置\n")
	if Userconfig.IdentityMode == 0 {
		fmt.Printf("完成prepare.sh配置\n")
		Log += fmt.Sprintf("完成prepare.sh配置\n")
	} else if Userconfig.IdentityMode == 1 {
		fmt.Printf("完成prepare_pk.sh配置\n")
		Log += fmt.Sprintf("完成prepare_pk.sh配置\n")
	}
	// 第4步：修改各节点对应chainmaker.yml,参考官网https://docs.chainmaker.org.cn/v2.3.0/html/instructions/多机部署.html
	fmt.Printf("修改chainmaker.yml\n")
	Log += fmt.Sprintf("修改chainmaker.yml\n")
	if Userconfig.NodeNum == 0 {
		k, err := strconv.Atoi(Userconfig.Node_cnt)
		_ = err
		for i := 0; i < k; i++ {
			// 修改IP & Port
			fieName := fmt.Sprintf("./chainmaker-go/build/config/node%d/chainmaker.yml", i+1)
			for j := 0; j < k; j++ {
				oldIpPort := fmt.Sprintf("    - \"/ip4/127.0.0.1/tcp/")
				newIpPort := fmt.Sprintf("    - \"/ip4/%s/tcp/", Userconfig.HostIpAddr)
				ReplaceFile(fieName, oldIpPort, newIpPort)
			}
			// 修改unarchive_block_height
			cmd = fmt.Sprintf("sed -i '/.*unarchive_block_height:*/c\\  unarchive_block_height: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				Userconfig.Block_height, i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改max_txpool_size
			cmd = fmt.Sprintf("sed -i '/.*max_txpool_size:*/c\\  max_txpool_size: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				RecommendedConfig.MaxTxpoolSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改logdb_segment_size
			cmd = fmt.Sprintf("sed -i '/.*logdb_segment_size:*/c\\  logdb_segment_size: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				RecommendedConfig.LogdbSegmentSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改hard_max_cache_size
			cmd = fmt.Sprintf("sed -i '/.*hard_max_cache_size:*/c\\  hard_max_cache_size: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				RecommendedConfig.HardMaxCacheSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
	} else {
		for i := 0; i < Userconfig.NodeNum; i++ {
			// 修改IP & Port
			fieName := fmt.Sprintf("./chainmaker-go/build/config/node%d/chainmaker.yml", i+1)
			for j := 0; j < Userconfig.NodeNum; j++ {
				oldIpPort := fmt.Sprintf("    - \"/ip4/127.0.0.1/tcp/%d", 11301+j)
				newIpPort := fmt.Sprintf("    - \"/ip4/%s/tcp/%s", Userconfig.Nodes[j].NodeIpAddr, Userconfig.Nodes[j].NodeP2PPort)
				//newIpPort := fmt.Sprintf("    - \"/dns/consensus1.tls.wx-org%d.chainmaker.org/tcp/1130%d", j+1, j+1)
				ReplaceFile(fieName, oldIpPort, newIpPort)
			}
			// 修改unarchive_block_height
			cmd = fmt.Sprintf("sed -i '/.*unarchive_block_height:*/c\\  unarchive_block_height: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				Userconfig.Block_height, i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			//修改block_size
			//for j := 0; j < Userconfig.NodeNum; j++ {
			//	cmd = fmt.Sprintf("sed -i '/.*block_size:*/c\\  block_size: %s' ./chainmaker-go/build/config/node%d/chainconfig/bc%d.yml",
			//		Userconfig.Block_size, i+1, j+1)
			//	result = command.CommandExec(cmd)
			//	if result == nil {
			//		fmt.Printf("ERROR to exec %s\n", cmd)
			//		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			//		return -1
			//	}
			//}

			// 修改block_tx_capacity
			//for j := 0; j < Userconfig.NodeNum; j++ {
			//	cmd = fmt.Sprintf("sed -i '/.*block_tx_capacity:*/c\\  block_tx_capacity: %s' ./chainmaker-go/build/config/node%d/chainconfig/bc%d.yml",
			//		RecommendedConfig.BlockTxCapacity[Userconfig.RecommendedConfigLevel], i+1, j+1)
			//	result = command.CommandExec(cmd)
			//	if result == nil {
			//		fmt.Printf("ERROR to exec %s\n", cmd)
			//		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			//		return -1
			//	}
			//}

			// 修改max_txpool_size
			cmd = fmt.Sprintf("sed -i '/.*max_txpool_size:*/c\\  max_txpool_size: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				RecommendedConfig.MaxTxpoolSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改block_interval
			//for j := 0; j < Userconfig.NodeNum; j++ {
			//	cmd = fmt.Sprintf("sed -i '/.*block_interval:*/c\\  block_interval: %s' ./chainmaker-go/build/config/node%d/chainconfig/bc%d.yml",
			//		RecommendedConfig.BlockInterval[Userconfig.RecommendedConfigLevel], i+1, j+1)
			//	result = command.CommandExec(cmd)
			//	if result == nil {
			//		fmt.Printf("ERROR to exec %s\n", cmd)
			//		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			//		return -1
			//	}
			//}

			// 修改logdb_segment_size
			cmd = fmt.Sprintf("sed -i '/.*logdb_segment_size:*/c\\  logdb_segment_size: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				RecommendedConfig.LogdbSegmentSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			// 修改hard_max_cache_size
			cmd = fmt.Sprintf("sed -i '/.*hard_max_cache_size:*/c\\  hard_max_cache_size: %s' ./chainmaker-go/build/config/node%d/chainmaker.yml",
				RecommendedConfig.HardMaxCacheSize[Userconfig.RecommendedConfigLevel], i+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			fmt.Printf("%s modify OK!\n", fieName)
			Log += fmt.Sprintf("%s modify OK!\n", fieName)
		}
	}
	for i := 0; i < Userconfig.NodeNum; i++ {
		k, err := strconv.Atoi(Userconfig.Chain_cnt)
		_ = err
		//修改block_size
		for j := 0; j < k; j++ {
			cmd = fmt.Sprintf("sed -i '/.*block_size:*/c\\  block_size: %s' ./chainmaker-go/build/config/node%d/chainconfig/bc%d.yml",
				Userconfig.Block_size, i+1, j+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
		// 修改block_tx_capacity
		for j := 0; j < k; j++ {
			cmd = fmt.Sprintf("sed -i '/.*block_tx_capacity:*/c\\  block_tx_capacity: %s' ./chainmaker-go/build/config/node%d/chainconfig/bc%d.yml",
				RecommendedConfig.BlockTxCapacity[Userconfig.RecommendedConfigLevel], i+1, j+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
		// 修改block_interval
		for j := 0; j < k; j++ {
			cmd = fmt.Sprintf("sed -i '/.*block_interval:*/c\\  block_interval: %s' ./chainmaker-go/build/config/node%d/chainconfig/bc%d.yml",
				RecommendedConfig.BlockInterval[Userconfig.RecommendedConfigLevel], i+1, j+1)
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
	}
	fmt.Printf("链配置文件bc.yml配置完成\n")
	Log += fmt.Sprintf("链配置文件bc.yml配置完成\n")
	// 第5步：执行build_release.sh脚本，将编译chainmaker-go模块，并打包生成安装，存于路径chainmaker-go/build/release中
	// 5.1执行脚本
	fmt.Printf("执行build_release.sh脚本, 编译chainmaker-go\n")
	Log += fmt.Sprintf("执行build_release.sh脚本, 编译chainmaker-go\n")
	cmd = fmt.Sprintf("./scripts/chainmaker-build4.sh")
	out = exec.Command("/bin/bash", "-c", cmd)
	result, err = out.Output()
	if err != nil {
		fmt.Printf("chainmaker-build4.sh执行失败,err不为0\n")
		Log += fmt.Sprintf("chainmaker-build4.sh执行失败,err不为0\n")
	}
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	// 5.2:查看生成好的节点证书和配置
	//fmt.Printf("查看生成好的节点证书和配置\n")
	if Userconfig.IdentityMode == 0 {
		fmt.Printf("查看生成好的节点证书和配置\n")
		Log += fmt.Sprintf("查看生成好的节点证书和配置\n")
	} else if Userconfig.IdentityMode == 1 {
		fmt.Printf("查看生成好的节点密钥和配置\n")
		Log += fmt.Sprintf("查看生成好的节点密钥和配置\n")
	}

	if Userconfig.NodeNum == 0 {
		var Node_cnt int
		Node_cnt, err = strconv.Atoi(Userconfig.Node_cnt)
		fmt.Printf("Node_cnt %s %d\n", Userconfig.Node_cnt, Node_cnt)
		Log += fmt.Sprintf("Node_cnt %s %d\n", Userconfig.Node_cnt, Node_cnt)
		for j := 0; j < Node_cnt; j++ {
			if Userconfig.IdentityMode == 0 {
				cmd = fmt.Sprintf("ls  chainmaker-go/build/release |grep %d.chainmaker", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.HostFile[j] = string(result)
				index := strings.Index(Userconfig.HostFile[j], "chainmaker.org")
				Userconfig.HostFilePath[j] = Userconfig.HostFile[j][0 : index+14]
			} else if Userconfig.IdentityMode == 1 {
				cmd = fmt.Sprintf("ls  chainmaker-go/build/release |grep node%d", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.HostFile[j] = string(result)
				index := strings.Index(Userconfig.HostFile[j], "node")
				Userconfig.HostFilePath[j] = Userconfig.HostFile[j][0 : index+5]
			}
		}
	} else {
		for j := 0; j < Userconfig.NodeNum; j++ {
			if Userconfig.IdentityMode == 0 {
				cmd = fmt.Sprintf("ls  chainmaker-go/build/release |grep %d.chainmaker", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.Nodes[j].NodeFile = string(result)
				index := strings.Index(Userconfig.Nodes[j].NodeFile, "chainmaker.org")
				Userconfig.Nodes[j].NodeFilePath = Userconfig.Nodes[j].NodeFile[0 : index+14]
			} else if Userconfig.IdentityMode == 1 {
				cmd = fmt.Sprintf("ls  chainmaker-go/build/release |grep node%d", j+1)
				result = command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
					return -1
				}
				Userconfig.Nodes[j].NodeFile = string(result)
				index := strings.Index(Userconfig.Nodes[j].NodeFile, "node")
				Userconfig.Nodes[j].NodeFilePath = Userconfig.Nodes[j].NodeFile[0 : index+5]
			}
		}
	}

	time.Sleep(2 * 1000 * time.Millisecond)
	if Userconfig.IdentityMode == 0 {
		cmd = fmt.Sprintf("ls  chainmaker-go/build/release |grep chainmaker-%s-wx-org", Userconfig.ChainmakerVersion)
		result = command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	} else if Userconfig.IdentityMode == 1 {
		cmd = fmt.Sprintf("ls  chainmaker-go/build/release |grep chainmaker-%s-node", Userconfig.ChainmakerVersion)
		result = command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	}
	fmt.Printf("编译完成，生成以上安装包\n")
	Log += fmt.Sprintf("编译完成，生成以上安装包\n")
	cmd = fmt.Sprintf("tree chainmaker-go/build/release/")
	result = command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result) //太长，先不打印
	Log += fmt.Sprintf("%s\n", result)

	return 0
}

//变更build_release.sh 脚本
func Binary_generate_build_release() int {
	//更新version
	fmt.Printf("开始更新...\n")
	Log += fmt.Sprintf("开始更新...\n")
	// 1 根据用户配置更新version
	fieName := fmt.Sprintf("scripts/build_release_demo.sh")

	var oldStr [10]string
	var newStr [10]string

	newFieName := fmt.Sprintf("scripts/build_release.sh")

	//二进制文件version
	oldStr[0] = fmt.Sprintf("VERSION=v2.3.1")
	newStr[0] = fmt.Sprintf("VERSION=%s", Userconfig.ChainmakerVersion)

	fmt.Printf("二进制文件version:%s\n", Userconfig.ChainmakerVersion)

	GenerateFile(fieName, newFieName, 1, oldStr, newStr)

	//  2 更新
	fmt.Printf("%s modify OK!\n", fieName)
	Log += fmt.Sprintf("%s modify OK!\n", fieName)

	return 0
}

// 生成部署脚本(scp方式)
func Chain_generate_shell_scp() int {
	// 部署长安链
	fmt.Printf("开始生成部署脚本(scp)...\n")
	Log += fmt.Sprintf("开始生成部署脚本(scp)...\n")
	// 1 根据用户配置，生成脚本
	fieName := fmt.Sprintf("scripts/chainmaker-setup_scp.sh")

	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/chainmaker-setup_scp%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		// 二进制文件名
		if Userconfig.IdentityMode == 0 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)
		} else if Userconfig.IdentityMode == 1 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-node%d", Userconfig.ChainmakerVersion, j+1)
		}
		// 二进制文件路径
		oldStr[4] = fmt.Sprintf("set host_path")
		newStr[4] = fmt.Sprintf("set host_path %s", Userconfig.Nodes[j].NodeFile)

		fmt.Printf("二进制文件路径%s\n", Userconfig.Nodes[j].NodeFile)

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}

	// 2 生成
	fmt.Printf("%s modify OK!\n", fieName)
	Log += fmt.Sprintf("%s modify OK!\n", fieName)

	return 0
}

// 生成二进制部署脚本(scp方式)
func Binary_generate_shell_scp() int {
	// 部署长安链
	fmt.Printf("开始生成部署脚本(scp)...\n")
	Log += fmt.Sprintf("开始生成部署脚本(scp)...\n")
	// 1 根据用户配置，生成脚本
	fieName := fmt.Sprintf("scripts/binary-setup_scp.sh")

	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/binary-setup_scp%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		// 二进制文件名
		if Userconfig.IdentityMode == 0 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)
		} else if Userconfig.IdentityMode == 1 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-node%d", Userconfig.ChainmakerVersion, j+1)
		}

		// 二进制文件路径
		oldStr[4] = fmt.Sprintf("set host_path")
		newStr[4] = fmt.Sprintf("set host_path %s", Userconfig.Nodes[j].NodeFile)

		fmt.Printf("二进制文件路径%s\n", Userconfig.Nodes[j].NodeFile)

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}

	// 2 生成
	fmt.Printf("%s modify OK!\n", fieName)
	Log += fmt.Sprintf("%s modify OK!\n", fieName)

	return 0
}

//命令行方式多点部署，通过ssh到各个子节点，执行stop.sh来停止长安链
func Chain_scp_stop() (int, string) {
	// 部署长安链
	var outresult1 string
	var outresult2 string
	var outresult3 string
	var outresult4 string
	fieName := fmt.Sprintf("scripts/chainmaker-stop_scp.sh")
	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/chainmaker-stop_scp%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		// 二进制文件名
		//oldStr[3] = fmt.Sprintf("set name")
		//newStr[3] = fmt.Sprintf("set name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)
		if Userconfig.IdentityMode == 0 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)
		} else if Userconfig.IdentityMode == 1 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-node%d", Userconfig.ChainmakerVersion, j+1)
		}

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}
	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行关闭脚本
		fmt.Printf("关闭子节点[%d]长安链\n", j)
		Log += fmt.Sprintf("关闭子节点[%d]长安链\n", j)
		cmd := fmt.Sprintf("./scripts/chainmaker-stop_scp%d.sh", j+1)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		if j == 0 {
			outresult1 = string(result)
		} else if j == 1 {
			outresult2 = string(result)
		} else if j == 2 {
			outresult3 = string(result)
		} else if j == 3 {
			outresult4 = string(result)
		}
	}
	return 0, string(outresult1) + string(outresult2) + string(outresult3) + string(outresult4)
}

// 生成二进制部署脚本(ftp方式)
func Binary_generate_shell_ftp() int {
	// 部署长安链
	fmt.Printf("开始生成部署脚本(ftp)...\n")
	Log += fmt.Sprintf("开始生成部署脚本(ftp)...\n")
	// 1 根据用户配置，生成脚本
	fieName := fmt.Sprintf("scripts/binary-setup_ftp.sh")

	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/binary-setup_ftp%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		// 二进制文件解压后的文件夹名
		oldStr[3] = fmt.Sprintf("set folder_name")
		newStr[3] = fmt.Sprintf("set folder_name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)

		// 二进制文件名
		oldStr[4] = fmt.Sprintf("set file_name")
		newStr[4] = fmt.Sprintf("set file_name %s", Userconfig.Nodes[j].NodeFile)

		// ftp登录使用的IP(主节点ip)
		oldStr[5] = fmt.Sprintf("set host_ip")
		newStr[5] = fmt.Sprintf("set host_ip %s", Userconfig.HostIpAddr)

		// ftp登录使用用户名
		oldStr[6] = fmt.Sprintf("set ftp_name")
		newStr[6] = fmt.Sprintf("set ftp_name %s", Userconfig.FtpUserName)

		// ftp登录使用的密码
		oldStr[7] = fmt.Sprintf("set ftp_password")
		newStr[7] = fmt.Sprintf("set ftp_password %s", Userconfig.FtpPassword)

		// 二进制文件路径
		oldStr[8] = fmt.Sprintf("set file_path")
		newStr[8] = fmt.Sprintf("set file_path %sbinary/build/release/", Userconfig.FtpPath)

		fmt.Printf("...%s\n", Userconfig.FtpPath)

		GenerateFile(fieName, newFieName, 9, oldStr, newStr)
	}

	// 2 生成
	fmt.Printf("%s modify OK!\n", fieName)
	Log += fmt.Sprintf("%s modify OK!\n", fieName)
	return 0
}

//docker多机部署为centos子节点安装docker（不安装docker则无法加入swarm集群）
func Node_C_install_docker_shell() int {

	fieName := fmt.Sprintf("scripts/Node_Centos_InstallDocker.sh")
	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/Node_Centos_InstallDocker%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		GenerateFile(fieName, newFieName, 3, oldStr, newStr)
	}

	return 0
}

//docker多机部署为ubuntu子节点安装docker（不安装docker则无法加入swarm集群）
func Node_U_install_docker_shell() int {

	fieName := fmt.Sprintf("scripts/Node_Ubuntu_InstallDocker.sh")
	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/Node_Ubuntu_InstallDocker%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		GenerateFile(fieName, newFieName, 3, oldStr, newStr)
	}

	return 0
}

//docker多机部署为Kylin子节点安装docker（不安装docker则无法加入swarm集群）
func Node_K_install_docker_shell() int {

	fieName := fmt.Sprintf("scripts/Node_Kylin_InstallDocker.sh")
	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/Node_Kylin_InstallDocker%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		GenerateFile(fieName, newFieName, 3, oldStr, newStr)
	}

	return 0
}

// 生成创建swarm集群脚本
func Chain_generate_shell_swarm() int {
	// 创建集群
	fmt.Printf("开始生成创建swarm集群脚本...\n")
	Log += fmt.Sprintf("开始生成创建swarm集群脚本...\n")
	// 生成加入swarm令牌
	cmd := fmt.Sprintf("docker swarm init --advertise-addr %s|grep docker|grep SWMTKN", Userconfig.HostIpAddr)
	result := command.CommandExec(cmd)
	result1 := result[4 : len(result)-1]
	str := "\""
	result2 := fmt.Sprintf("%s%s%s", str, result1, str)
	// 1 根据用户配置，生成脚本
	fieName := fmt.Sprintf("scripts/chainmaker-create_swarm.sh")

	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/chainmaker-create_swarm%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		//docker swarm join --token
		oldStr[3] = fmt.Sprintf("set token")
		newStr[3] = fmt.Sprintf("set token %s", result2)

		//临时修改主机名
		oldStr[4] = fmt.Sprintf("set nodenum")
		newStr[4] = fmt.Sprintf("set nodenum %d", j+1)

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}
	// 2 生成
	fmt.Printf("%s modify OK!\n", fieName)
	Log += fmt.Sprintf("%s modify OK!\n", fieName)

	return 0
}

// 子节点通过令牌加入swarm集群
func Join_docker_swarm() int {

	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		fmt.Printf("子节点开始加入集群[%d]\n", j)
		Log += fmt.Sprintf("子节点开始加入集群[%d]\n", j)
		cmd := fmt.Sprintf("./scripts/chainmaker-create_swarm%d.sh", j+1)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	}
	return 0
}

// 生成部署脚本(ftp方式)
func Chain_generate_shell_ftp() int {
	// 部署长安链
	fmt.Printf("开始生成部署脚本(ftp)...\n")
	// 1 根据用户配置，生成脚本
	fieName := fmt.Sprintf("scripts/chainmaker-setup_ftp.sh")

	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/chainmaker-setup_ftp%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		// 二进制文件解压后的文件夹名
		oldStr[3] = fmt.Sprintf("set folder_name")
		newStr[3] = fmt.Sprintf("set folder_name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)

		// 二进制文件名
		oldStr[4] = fmt.Sprintf("set file_name")
		newStr[4] = fmt.Sprintf("set file_name %s", Userconfig.Nodes[j].NodeFile)

		// ftp登录使用的IP(主节点ip)
		oldStr[5] = fmt.Sprintf("set host_ip")
		newStr[5] = fmt.Sprintf("set host_ip %s", Userconfig.HostIpAddr)

		// ftp登录使用用户名
		oldStr[6] = fmt.Sprintf("set ftp_name")
		newStr[6] = fmt.Sprintf("set ftp_name %s", Userconfig.FtpUserName)

		// ftp登录使用的密码
		oldStr[7] = fmt.Sprintf("set ftp_password")
		newStr[7] = fmt.Sprintf("set ftp_password %s", Userconfig.FtpPassword)

		// 二进制文件路径
		oldStr[8] = fmt.Sprintf("set file_path")
		newStr[8] = fmt.Sprintf("set file_path %schainmaker-go/build/release/", Userconfig.FtpPath)

		fmt.Printf("...%s\n", Userconfig.FtpPath)

		GenerateFile(fieName, newFieName, 9, oldStr, newStr)
	}

	// 2 生成
	fmt.Printf("%s modify OK!\n", fieName)

	return 0
}

// 生成部署脚本(docker方式)
func Chain_generate_shell_docker() int {
	// 部署长安链
	fmt.Printf("开始生成部署脚本...\n")
	Log += fmt.Sprintf("开始生成部署脚本...\n")
	// 获取NODE_ID

	// 1 根据用户配置，生成脚本
	if Userconfig.IdentityMode == 0 {
		fieName := fmt.Sprintf("scripts/chainmaker-setup_docker.sh")
		//ModifyFile(fieName, "set ip ", Userconfig.HostDockerIp, 0)
		ModifyFile(fieName, "P2P_PORT=", Userconfig.HostP2PPort, 0)
		ModifyFile(fieName, "RPC_PORT=", Userconfig.HostRPCPort, 0)
		ModifyFile(fieName, "NODE_COUNT=", Userconfig.Node_cnt, 0)
		//ModifyFile(fieName, "SERVER_NUM=", Userconfig.Server_node_cnt, 0)
		// 2 生成
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
	} else if Userconfig.IdentityMode == 1 {
		fieName := fmt.Sprintf("scripts/chainmaker-setup_docker_pk.sh")
		//ModifyFile(fieName, "set ip ", Userconfig.HostDockerIp, 0)
		ModifyFile(fieName, "P2P_PORT=", Userconfig.HostP2PPort, 0)
		ModifyFile(fieName, "RPC_PORT=", Userconfig.HostRPCPort, 0)
		ModifyFile(fieName, "NODE_COUNT=", Userconfig.Node_cnt, 0)
		//ModifyFile(fieName, "SERVER_NUM=", Userconfig.Server_node_cnt, 0)
		// 2 生成
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
	}
	return 0
}

func Chain_generate_shell_solo_docker() int {
	// 部署长安链
	fmt.Printf("开始生成部署脚本...\n")
	Log += fmt.Sprintf("开始生成部署脚本...\n")
	// 获取NODE_ID

	// 1 根据用户配置，生成脚本
	if Userconfig.IdentityMode == 0 {
		fieName := fmt.Sprintf("scripts/chainmaker-setup_solo_docker.sh")
		//ModifyFile(fieName, "set ip ", Userconfig.HostDockerIp, 0)
		ModifyFile(fieName, "P2P_PORT=", Userconfig.HostP2PPort, 0)
		ModifyFile(fieName, "RPC_PORT=", Userconfig.HostRPCPort, 0)
		ModifyFile(fieName, "NODE_COUNT=", Userconfig.Node_cnt, 0)
		//ModifyFile(fieName, "SERVER_NUM=", Userconfig.Server_node_cnt, 0)
		// 2 生成
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
	} else if Userconfig.IdentityMode == 1 {
		fieName := fmt.Sprintf("scripts/chainmaker-setup_solo_docker_pk.sh")
		//ModifyFile(fieName, "set ip ", Userconfig.HostDockerIp, 0)
		ModifyFile(fieName, "P2P_PORT=", Userconfig.HostP2PPort, 0)
		ModifyFile(fieName, "RPC_PORT=", Userconfig.HostRPCPort, 0)
		ModifyFile(fieName, "NODE_COUNT=", Userconfig.Node_cnt, 0)
		//ModifyFile(fieName, "SERVER_NUM=", Userconfig.Server_node_cnt, 0)
		// 2 生成
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
	}
	return 0
}

//获取架构信息（x86、arm），下载二进制文件
func Release_load(Url info.Html_url) int {
	result := command.CommandExec("uname -m")
	start := strings.Index(string(result), "x86")
	if start == -1 {
		cmd := fmt.Sprintf("wget -P binary/tools/chainmaker-cryptogen/bin %s --no-check-certificate && tar -zxf binary/tools/chainmaker-cryptogen/bin/chainmaker-cryptogen-v*-linux-*64.tar.gz -C binary/tools/chainmaker-cryptogen/bin \n", Url.CryptogenArm)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
		version := strings.Compare(Userconfig.ChainmakerVersion, "v2.3.0")
		if version == 0 {
			cmd := fmt.Sprintf("wget -P binary/bin %s --no-check-certificate && tar -zxf binary/bin/chainmaker-v*-linux-*64.tar.gz -C binary/bin \n", Url.Chainmakerv0Arm)
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
			cmd = fmt.Sprintf("wget -P binary/tools/cmc %s --no-check-certificate && tar -zxf binary/tools/cmc/cmc-v*-linux-*64.tar.gz -C binary/tools/cmc \n", Url.Cmcv0Arm)
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}

		version = strings.Compare(Userconfig.ChainmakerVersion, "v2.3.1")
		if version == 0 {
			cmd := fmt.Sprintf("wget -P binary/bin %s --no-check-certificate && tar -zxf binary/bin/chainmaker-v*-linux-*64.tar.gz -C binary/bin \n", Url.Chainmakerv1Arm)
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
			cmd = fmt.Sprintf("wget -P binary/tools/cmc %s --no-check-certificate && tar -zxf binary/tools/cmc/cmc-v*-linux-*64.tar.gz -C binary/tools/cmc \n", Url.Cmcv1Arm)
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}

		version = strings.Compare(Userconfig.ChainmakerVersion, "v2.3.2")
		if version == 0 {
			cmd := fmt.Sprintf("wget -P binary/bin %s --no-check-certificate && tar -zxf binary/bin/chainmaker-v*-linux-*64.tar.gz -C binary/bin \n", Url.Chainmakerv2Arm)
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
			cmd = fmt.Sprintf("wget -P binary/tools/cmc %s --no-check-certificate && tar -zxf binary/tools/cmc/cmc-v*-linux-*64.tar.gz -C binary/tools/cmc \n", Url.Cmcv2Arm)
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}

	} else {
		cmd := fmt.Sprintf("wget -P binary/tools/chainmaker-cryptogen/bin %s --no-check-certificate && tar -zxf binary/tools/chainmaker-cryptogen/bin/chainmaker-cryptogen-v*-linux-*64.tar.gz -C binary/tools/chainmaker-cryptogen/bin \n", Url.Cryptogen)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
		version := strings.Compare(Userconfig.ChainmakerVersion, "v2.3.0")
		if version == 0 {
			cmd := fmt.Sprintf("wget -P binary/bin %s --no-check-certificate && tar -zxf binary/bin/chainmaker-v*-linux-*64.tar.gz -C binary/bin \n", Url.Chainmakerv0)
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
			cmd = fmt.Sprintf("wget -P binary/tools/cmc %s --no-check-certificate && tar -zxf binary/tools/cmc/cmc-v*-linux-*64.tar.gz -C binary/tools/cmc \n", Url.Cmcv0)
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}

		version = strings.Compare(Userconfig.ChainmakerVersion, "v2.3.1")
		if version == 0 {
			cmd := fmt.Sprintf("wget -P binary/bin %s --no-check-certificate && tar -zxf binary/bin/chainmaker-v*-linux-*64.tar.gz -C binary/bin \n", Url.Chainmakerv1)
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
			cmd = fmt.Sprintf("wget -P binary/tools/cmc %s --no-check-certificate && tar -zxf binary/tools/cmc/cmc-v*-linux-*64.tar.gz -C binary/tools/cmc \n", Url.Cmcv1)
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}

		version = strings.Compare(Userconfig.ChainmakerVersion, "v2.3.2")
		if version == 0 {
			cmd := fmt.Sprintf("wget -P binary/bin %s --no-check-certificate && tar -zxf binary/bin/chainmaker-v*-linux-*64.tar.gz -C binary/bin \n", Url.Chainmakerv2)
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
			cmd = fmt.Sprintf("wget -P binary/tools/cmc %s --no-check-certificate && tar -zxf binary/tools/cmc/cmc-v*-linux-*64.tar.gz -C binary/tools/cmc \n", Url.Cmcv2)
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}

	}
	return 0
}

//生成二进制部署文件
func Binary(Software info.Ossoftinfo) int {
	//扫描二进制部署文件是否存在
	//下载release-linux-all.json文件
	cmd := fmt.Sprintf("rm -rf release-linux-all.json && wget https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/raw/chainmaker-release-linux-all/release-linux-all.json --no-check-certificate")
	end := command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", end)
	Log += fmt.Sprintf("%s\n", end)

	result := command.CommandExec("ls")
	start := strings.Index(string(result), "binary")
	if start == -1 {
		fmt.Printf("开始生成二进制部署文件\n")
		Log += fmt.Sprintf("开始生成二进制部署文件\n")
		cmd := fmt.Sprintf("cd scripts && ./binary-build.sh")
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	} else {
		scan.ScanChainVersion()
		version := strings.Compare("Software.ChainVersion", "Userconfig.ChainmakerVersion")
		if version == 0 {
			fmt.Printf("binary exist\n")
			Log += fmt.Sprintf("binary exist\n")
		} else {
			cmd := fmt.Sprintf("rm -rf binary")
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			fmt.Printf("开始生成二进制部署文件\n")
			Log += fmt.Sprintf("开始生成二进制部署文件\n")
			cmd = fmt.Sprintf("cd scripts && ./binary-build.sh")
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}
	}

	return 0
}

func Offline() int {
	//扫描离线部署文件是否存在
	result := command.CommandExec("ls")
	start := strings.Index(string(result), "chainmaker-cryptogen")
	if start == -1 {
		fmt.Printf("\033[1;31;40m请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen长安链官网下载chainmaker-cryptogen文件\033[0m\n")
		Log += fmt.Sprintf("请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-cryptogen长安链官网下载chainmaker-cryptogen文件\n")
	} else {
		fmt.Printf("\033[1;32;40m已存在chainmaker-cryptogen文件夹\033[0m\n")
		Log += fmt.Sprintf("已存在chainmaker-cryptogen文件夹\n")

	}

	result = command.CommandExec("ls")
	start = strings.Index(string(result), "chainmaker-go")
	if start == -1 {
		fmt.Printf("\033[1;31;40m请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-go长安链官网下载chainmaker-go源码\033[0m\n")
		Log += fmt.Sprintf("请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-go长安链官网下载chainmaker-go源码\n")
	} else {
		fmt.Printf("\033[1;32;40m已存在chainmaker-go文件夹\033[0m\n")
		Log += fmt.Sprintf("已存在chainmaker-go文件夹\n")

	}

	result = command.CommandExec("find . -name \"*chainmaker-v*-linux-*64.tar.gz\"")
	start = strings.Index(string(result), "chainmaker-v")
	if start == -1 {
		fmt.Printf("\033[1;31;40m请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/raw/chainmaker-release-linux-all/release-linux-all.json 长安链官网下载chainmaker-go二进制文件\033[0m\n")
		Log += fmt.Sprintf("请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/blob/chainmaker-release-linux-all/release-linux-all.json长安链官网下载chainmaker-go二进制\n")
	} else {
		fmt.Printf("\033[1;32;40m已存在chainmaker-go二进制文件\033[0m\n")
		Log += fmt.Sprintf("已存在chainmaker-go二进制文件\n")
	}

	result = command.CommandExec("find . -name \"*cmc-v*-linux-*64.tar.gz\"")
	start = strings.Index(string(result), "cmc-v")
	if start == -1 {
		fmt.Printf("\033[1;31;40m请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/raw/chainmaker-release-linux-all/release-linux-all.json 长安链官网下载cmc二进制文件\033[0m\n")
		Log += fmt.Sprintf("请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/blob/chainmaker-release-linux-all/release-linux-all.json长安链官网下载cmc二进制\n")
	} else {
		fmt.Printf("\033[1;32;40m已存在cmc二进制文件\033[0m\n")
		Log += fmt.Sprintf("已存在cmc二进制文件\n")
	}

	result = command.CommandExec("find . -name \"*chainmaker-cryptogen-v*-linux-*64.tar.gz\"")
	start = strings.Index(string(result), "chainmaker-cryptogen-v")
	if start == -1 {
		fmt.Printf("\033[1;31;40m请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/raw/chainmaker-release-linux-all/release-linux-all.json 长安链官网下载chainmaker-cryptogen二进制文件\033[0m\n")
		Log += fmt.Sprintf("请前往https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/raw/chainmaker-release-linux-all/release-linux-all.json长安链官网下载chainmaker-cryptogen二进制\n")
	} else {
		fmt.Printf("\033[1;32;40m已存在chainmaker-cryptogen二进制文件\033[0m\n")
		Log += fmt.Sprintf("已存在chainmaker-cryptogen二进制文件\n")
	}
	return 0
}

//生成二进制部署文件
func Offline_build(Software info.Ossoftinfo) int {
	//扫描二进制部署文件是否存在
	result := command.CommandExec("ls")
	start := strings.Index(string(result), "binary")
	if start == -1 {
		fmt.Printf("开始生成二进制部署文件\n")
		Log += fmt.Sprintf("开始生成二进制部署文件\n")
		cmd := fmt.Sprintf("cd scripts && ./offline-build.sh")
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	} else {
		scan.ScanChainVersion()
		version := strings.Compare("Software.ChainVersion", "Userconfig.ChainmakerVersion")
		if version == 0 {
			fmt.Printf("binary exist\n")
			Log += fmt.Sprintf("binary exist\n")
		} else {
			cmd := fmt.Sprintf("rm -rf binary")
			result := command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			fmt.Printf("开始生成二进制部署文件\n")
			Log += fmt.Sprintf("开始生成二进制部署文件\n")
			cmd = fmt.Sprintf("cd scripts && ./offline-build.sh")
			result = command.CommandExec(cmd)
			// 通过返回，打印编译、配置结果
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}
	}

	return 0
}

// 通过scp部署长安链
func Chain_setup_scp() int {
	// cmd部署长安链

	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		fmt.Printf("开始部署子节点[%d]\n", j)
		Log += fmt.Sprintf("开始部署子节点[%d]\n", j)
		cmd := fmt.Sprintf("./scripts/chainmaker-setup_scp%d.sh", j+1)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	}

	return 0
}

// 通过scp二进制部署长安链
func Binary_setup_scp() int {
	// cmd部署长安链

	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		fmt.Printf("开始部署子节点[%d]\n", j)
		Log += fmt.Sprintf("开始部署子节点[%d]\n", j)
		cmd := fmt.Sprintf("./scripts/binary-setup_scp%d.sh", j+1)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	}

	return 0
}

// 通过ftp部署长安链
func Chain_setup_ftp() int {
	// cmd部署长安链

	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		fmt.Printf("开始部署子节点[%d]\n", j)
		Log += fmt.Sprintf("开始部署子节点[%d]\n", j)
		cmd := fmt.Sprintf("./scripts/chainmaker-setup_ftp%d.sh", j+1)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)

	}

	return 0
}

// 通过ftp二进制部署长安链
func Binary_setup_ftp() int {
	// cmd部署长安链

	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		fmt.Printf("开始部署子节点[%d]\n", j)
		Log += fmt.Sprintf("开始部署子节点[%d]\n", j)
		cmd := fmt.Sprintf("./scripts/binary-setup_ftp%d.sh", j+1)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)

	}

	return 0
}

// 通过docker部署长安链
func Chain_setup_docker() int {
	// docker部署长安链

	// for j := 0; j < Userconfig.NodeNum; j++ {
	// 	// 执行部署脚本
	// 	cmd := fmt.Sprintf("./scripts/chainmaker-setup%d.sh", j)
	// 	result := command.CommandExec(cmd)
	// 	// 通过返回，打印编译、配置结果
	// 	fmt.Printf("%s\n", result)
	// }
	//判断是否已经存在docker-compose.yml文件，如果存在需要删除，以便后面继续生成docker-compose.yml文件
	result := command.CommandExec("cd chainmaker-go/scripts/docker/multi_node/;ls")
	start := strings.Index(string(result), "docker-compose.yml")
	if start == -1 {

	} else {
		command.CommandExec("cd chainmaker-go/scripts/docker/multi_node/;rm -rf docker-compose.yml")
	}
	if Userconfig.IdentityMode == 0 {
		cmd := fmt.Sprintf("./scripts/chainmaker-setup_docker.sh")
		result1 := command.CommandExec(cmd)
		fmt.Printf("%s\n", result1)
		Log += fmt.Sprintf("%s\n", result1)
	} else if Userconfig.IdentityMode == 1 {
		cmd := fmt.Sprintf("./scripts/chainmaker-setup_docker_pk.sh")
		result1 := command.CommandExec(cmd)
		fmt.Printf("%s\n", result1)
		Log += fmt.Sprintf("%s\n", result1)
	}
	return 0
}

// 通过docker单点部署长安链
func Chain_setup_solo_docker() int {
	if Userconfig.IdentityMode == 0 {
		cmd := fmt.Sprintf("./scripts/chainmaker-setup_solo_docker.sh")
		result1 := command.CommandExec(cmd)
		fmt.Printf("%s\n", result1)
		Log += fmt.Sprintf("%s\n", result1)
	} else if Userconfig.IdentityMode == 1 {
		cmd := fmt.Sprintf("./scripts/chainmaker-setup_solo_docker_pk.sh")
		result1 := command.CommandExec(cmd)
		fmt.Printf("%s\n", result1)
		Log += fmt.Sprintf("%s\n", result1)
	}
	return 0
}

//docker单点启动长安链
func Chain_start_solo_docker() int {
	// 启动长安链，docker-compose -f docker-compose1.yml up -d  or  ./start.sh docker-compose1.yml
	fmt.Printf("启动长安链...\n")
	Log += fmt.Sprintf("启动长安链...\n")
	k, err := strconv.Atoi(Userconfig.Node_cnt)
	_ = err
	for j := 0; j < k; j++ {
		// 执行部署脚本
		cmd := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/ && docker-compose -f docker-compose%d.yml up -d", j+1)
		command.CommandExec(cmd)
	}
	return 0
}

//docker单点停止长安链
func Chain_stop_solo_docker() (int, string) {
	// 启动长安链，docker-compose -f docker-compose1.yml down  or  ./stop.sh docker-compose1.yml
	fmt.Printf("停止长安链...\n")
	Log += fmt.Sprintf("停止长安链...\n")
	k, err := strconv.Atoi(Userconfig.Node_cnt)
	_ = err
	for j := 0; j < k; j++ {
		// 断开docker network中的multinode_default
		cmd := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/ && docker network disconnect -f multinode_default cm-node%d;cd ../../../../", j+1)
		command.CommandExec(cmd)
	}
	for j := 0; j < k; j++ {
		// 执行部署脚本
		cmd1 := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/ && docker-compose -f docker-compose%d.yml down -v;cd ../../../../", j+1)
		command.CommandExec(cmd1)
	}
	//关闭长安链后备份历史配置文件，保证下次部署节点组网成功
	Chain_cert_backup()
	//docker单机部署完后备份config文件以及data下的文件,保证下次docker单机不会报证书不匹配错误
	cmd := fmt.Sprintf("mkdir -p chainmaker-go/scripts/docker/multi_node/config_bak && mv chainmaker-go/scripts/docker/multi_node/config  chainmaker-go/scripts/docker/multi_node/config_bak/config_bak_$(date '+%%Y%%m%%d%%H%%M%%S')")
	command.CommandExec(cmd)
	cmd1 := fmt.Sprintf("mkdir -p chainmaker-go/scripts/docker/multi_node/data_bak && mv chainmaker-go/scripts/docker/multi_node/data  chainmaker-go/scripts/docker/multi_node/data_bak/data_bak_$(date '+%%Y%%m%%d%%H%%M%%S')")
	command.CommandExec(cmd1)
	outresult := fmt.Sprintf("长安链已关闭")
	fmt.Printf("%s\n", outresult)
	return 0, outresult
}

// 命令行方式启动长安链
func Chain_start() int {
	// 启动长安链，执行cluster_quick_start.sh脚本，会解压各个安装包，调用bin目录中的start.sh脚本，启动chainmaker节点
	fmt.Printf("启动长安链...\n")
	Log += fmt.Sprintf("启动长安链...\n")
	// 执行部署脚本
	cmd := fmt.Sprintf("cd chainmaker-go/scripts && ./cluster_quick_start.sh normal")
	result := command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	return 0
}

// 二进制方式启动长安链
func Binary_start() int {
	// 启动长安链，执行start.sh脚本，会解压各个安装包，调用bin目录中的start.sh脚本，启动chainmaker节点
	fmt.Printf("启动长安链...\n")
	Log += fmt.Sprintf("启动长安链...\n")
	// 执行部署脚本
	cmd := fmt.Sprintf("cd binary/scripts && ./cluster_quick_start.sh normal")
	result := command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	return 0
}

// docker方式启动长安链
func Chain_start_docker() int {
	// 启动长安链，docker-compose -f docker-compose1.yml up -d  or  ./start.sh docker-compose1.yml
	fmt.Printf("启动长安链...\n")
	Log += fmt.Sprintf("启动长安链...\n")
	// 执行部署脚本
	cmd := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/ && docker stack deploy --with-registry-auth -c docker-compose.yml chainmaker")
	result := command.CommandExec(cmd)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	return 0
}

// 查看节点启动使用是否正常
func Cat_process_exist() int {
	fmt.Printf("查看节点启动使用是否正常...\n")
	Log += fmt.Sprintf("查看节点启动使用是否正常...\n")
	// 执行命令行，查看进程是否存在
	cmd := string("ps -ef|grep chainmaker | grep -v grep")
	result := command.CommandExec(cmd)
	// 通过返回，打印结果
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
		return -1
	}
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)
	return 0
}

// 查看端口是否监听
func Chain_display(Userconfig info.Userconfig_json) int {
	Cat_process_exist()

	fmt.Printf("查看端口是否监听...\n")
	Log += fmt.Sprintf("查看端口是否监听...\n")
	// 查看端口是否监听
	strCount := strings.Count(Userconfig.HostRPCPort, "")
	// fmt.Printf("HostRPCPort %s, len=%d ,%s \n", Userconfig.HostRPCPort, strCount, Userconfig.HostRPCPort[:strCount-2])
	cmd := fmt.Sprintf("netstat -lptn | grep %s", Userconfig.HostRPCPort[:strCount-2])
	//cmd := fmt.Sprintf("netstat -lptn | grep %s", Userconfig.HostRPCPort)
	result := command.CommandExec(cmd)
	// 通过返回，打印结果
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
		return -1
	}
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	// 检查节点是否有ERROR日志
	if Userconfig.DeploymentType == 0 {
		cmd = string("cat chainmaker-go/build/release/*/log/system.log |grep \"ERROR\\|put block\\|all necessary\"")
		result = command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR日志为空\n")
			Log += fmt.Sprintf("ERROR日志为空\n")
			return 0
		}
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
	} else {
		if Userconfig.NodeNum > 0 {
			cmd = string("cat chainmaker-go/scripts/docker/multi_node/multi_log/log*/log*/system.log |grep \"ERROR\\|put block\\|all necessary\"")
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR日志为空\n")
				Log += fmt.Sprintf("ERROR日志为空\n")
				return 0
			}
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		} else {
			cmd = string("cat chainmaker-go/scripts/docker/multi_node/log/*/system.log |grep \"ERROR\\|put block\\|all necessary\"")
			result = command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR日志为空\n")
				Log += fmt.Sprintf("ERROR日志为空\n")
				return 0
			}
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}
	}

	return 0
}

// 查看端口是否监听(二进制部署)
func Binary_display(Userconfig info.Userconfig_json) int {
	Cat_process_exist()

	fmt.Printf("查看端口是否监听...\n")
	Log += fmt.Sprintf("查看端口是否监听...\n")
	// 查看端口是否监听
	//strCount := strings.Count(Userconfig.HostRPCPort, "")
	// fmt.Printf("HostRPCPort %s, len=%d ,%s \n", Userconfig.HostRPCPort, strCount, Userconfig.HostRPCPort[:strCount-2])
	//cmd := fmt.Sprintf("netstat -lptn | grep %s", Userconfig.HostRPCPort[:strCount-2])
	cmd := fmt.Sprintf("netstat -lptn | grep %s", Userconfig.HostRPCPort)
	result := command.CommandExec(cmd)
	// 通过返回，打印结果
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
		return -1
	}
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	// 检查节点是否有ERROR日志
	cmd = string("cat binary/build/release/*/log/system.log |grep \"ERROR\\|put block\\|all necessary\"")
	result = command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR日志为空\n")
		Log += fmt.Sprintf("ERROR日志为空\n")
		return 0
	}
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)

	return 0
}

// 一键验证二进制部署是否成功
func Binary_test() int {
	// 执行部署脚本，部署示例合约，验证长安链
	cmd1 := fmt.Sprintf("pwd")
	current_dir := command.CommandExec(cmd1)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", current_dir)

	if Userconfig.IdentityMode == 0 {
		//备份sdk_config.yml配置文件
		cmd1 := fmt.Sprintf("cp ./binary/tools/cmc/testdata/sdk_config.yml ./binary/tools/cmc/testdata/sdk_config.demo.yml")
		result1 := command.CommandExec(cmd1)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result1)

		//修改sdk_config.yml配置文件
		fmt.Printf("修改sdk_config.yml\n")
		Log += fmt.Sprintf("修改sdk_config.yml\n")
		fieName := fmt.Sprintf("./binary/tools/cmc/testdata/sdk_config.yml")
		if Userconfig.NodeNum > 0 {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.Nodes[0].NodeIpAddr, Userconfig.Nodes[0].NodeRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %d", Userconfig.NodeNum)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		} else {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %s", Userconfig.Node_cnt)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		}
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
		fmt.Printf("部署示例合约...\n")
		Log += fmt.Sprintf("部署示例合约...\n")
		cmd := fmt.Sprintf("./scripts/binary-test.sh")
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)

		//初始化sdk_config.yml配置文件
		cmd = fmt.Sprintf("mv ./binary/tools/cmc/testdata/sdk_config.demo.yml ./binary/tools/cmc/testdata/sdk_config.yml")
		result = command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)

	} else if Userconfig.IdentityMode == 1 {
		//备份sdk_config_pk.yml配置文件
		cmd1 := fmt.Sprintf("cp ./binary/tools/cmc/testdata/sdk_config_pk.yml ./binary/tools/cmc/testdata/sdk_config_pk.demo.yml")
		result1 := command.CommandExec(cmd1)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result1)

		//修改sdk_config_pk.yml配置文件
		fmt.Printf("修改sdk_config_pk.yml\n")
		Log += fmt.Sprintf("修改sdk_config_pk.yml\n")
		fieName := fmt.Sprintf("./binary/tools/cmc/testdata/sdk_config_pk.yml")
		if Userconfig.NodeNum > 0 {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.Nodes[0].NodeIpAddr, Userconfig.Nodes[0].NodeRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %d", Userconfig.NodeNum)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		} else {
			oldIpPort := fmt.Sprintf("node_addr: \"127.0.0.1:12301\"")
			newIpPort := fmt.Sprintf("node_addr: \"%s:%s\"", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
			ReplaceFile(fieName, oldIpPort, newIpPort)
			oldConnectCnt := fmt.Sprintf("conn_cnt: 10")
			newConnectCnt := fmt.Sprintf("conn_cnt: %s", Userconfig.Node_cnt)
			ReplaceFile(fieName, oldConnectCnt, newConnectCnt)
		}
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
		fmt.Printf("部署示例合约...\n")
		Log += fmt.Sprintf("部署示例合约...\n")
		cmd := fmt.Sprintf("./scripts/binary-test_pk.sh")
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)

		//初始化sdk_config_pk.yml配置文件
		cmd = fmt.Sprintf("mv ./binary/tools/cmc/testdata/sdk_config_pk.demo.yml ./binary/tools/cmc/testdata/sdk_config_pk.yml")
		result = command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
	}

	return 0
}

// PermissionWithCert模式下备份长安链
func Chain_cert_backup() int {
	if Userconfig.IdentityMode == 0 {
		// 执行部署命令
		// 备份证书文件
		cmd := fmt.Sprintf("mkdir -p chainmaker-go/build/backupWithCert/backup_certs && mv chainmaker-go/build/crypto-config  chainmaker-go/build/backupWithCert/backup_certs/crypto-config_$(date '+%%Y%%m%%d%%H%%M%%S')")
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		// 备份配置文件
		cmd1 := fmt.Sprintf("mkdir -p chainmaker-go/build/backupWithCert/backup_config && mv chainmaker-go/build/config  chainmaker-go/build/backupWithCert/backup_config/config_$(date '+%%Y%%m%%d%%H%%M%%S')")
		result1 := command.CommandExec(cmd1)
		fmt.Printf("%s\n", result1)
		// 备份release文件
		cmd2 := fmt.Sprintf("mkdir -p chainmaker-go/build/backupWithCert/backup_release && mv chainmaker-go/build/release  chainmaker-go/build/backupWithCert/backup_release/release_$(date '+%%Y%%m%%d%%H%%M%%S')")
		result2 := command.CommandExec(cmd2)
		fmt.Printf("%s\n", result2)
		// 备份crypto_config.yml文件
		cmd3 := fmt.Sprintf("mkdir -p chainmaker-go/build/backupWithCert/backup_crypto_config_yml && mv chainmaker-go/build/crypto_config.yml  chainmaker-go/build/backupWithCert/backup_crypto_config_yml/yml_$(date '+%%Y%%m%%d%%H%%M%%S')")
		result3 := command.CommandExec(cmd3)
		fmt.Printf("%s\n", result3)
		// 备份crypto_config.yml文件
		cmd4 := fmt.Sprintf("mkdir -p chainmaker-go/build/backupWithCert/backup_pkcs11_keys_yml && mv chainmaker-go/build/pkcs11_keys.yml  chainmaker-go/build/backupWithCert/backup_pkcs11_keys_yml/keys_$(date '+%%Y%%m%%d%%H%%M%%S')")
		result4 := command.CommandExec(cmd4)
		fmt.Printf("%s\n", result4)
	}
	return 0
}

// 生成关闭长安链脚本(scp方式)
func Binary_generate_shell_scp_stop() int {
	// 停止长安链
	fmt.Printf("开始生成停止脚本(scp)...\n")
	Log += fmt.Sprintf("开始生成停止脚本(scp)...\n")
	fieName := fmt.Sprintf("scripts/binary-stop_scp.sh")

	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		newFieName := fmt.Sprintf("scripts/binary-stop_scp%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		// 二进制文件名
		//oldStr[3] = fmt.Sprintf("set name")
		//newStr[3] = fmt.Sprintf("set name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)
		if Userconfig.IdentityMode == 0 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-wx-org%d.chainmaker.org", Userconfig.ChainmakerVersion, j+1)
		} else if Userconfig.IdentityMode == 1 {
			oldStr[3] = fmt.Sprintf("set name")
			newStr[3] = fmt.Sprintf("set name chainmaker-%s-node%d", Userconfig.ChainmakerVersion, j+1)
		}

		GenerateFile(fieName, newFieName, 4, oldStr, newStr)
	}

	fmt.Printf("%s modify OK!\n", fieName)
	Log += fmt.Sprintf("%s modify OK!\n", fieName)

	return 0
}

// 关闭长安链
func Chain_stop() (int, string) {
	// 执行部署脚本,关闭集群
	var outresult string
	if Userconfig.NodeNum == 0 {
		cmd := fmt.Sprintf("cd chainmaker-go/scripts && ./cluster_quick_stop.sh")
		result := command.CommandExec(cmd)
		outresult = string(result)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
		//关闭长安链后备份历史配置文件，保证下次部署节点组网成功
		Chain_cert_backup()
		fmt.Printf("长安链已关闭！\n")
		Log += fmt.Sprintf("长安链已关闭！\n")

	} else {
		_, outresult = Chain_scp_stop()
		Chain_cert_backup()
		fmt.Printf("长安链已关闭！\n")
		Log += fmt.Sprintf("长安链已关闭！\n")

	}
	//fmt.Printf("输出结果检查\n%s\n", outresult)
	return 0, outresult
}

// 关闭长安链（二进制部署）
func Binary_stop() (int, string) {
	// 执行部署脚本,关闭集群
	cmd := fmt.Sprintf("cd binary/scripts && ./cluster_quick_stop.sh")
	result := command.CommandExec(cmd)
	outresult := string(result)
	// 通过返回，打印编译、配置结果
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)
	return 0, outresult
}

// 关闭长安链（二进制部署scp方式）
func Binary_stop_scp() (int, string) {
	// 执行部署脚本,关闭各节点长安链
	var outresult1 string
	var outresult2 string
	var outresult3 string
	var outresult4 string
	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行关闭脚本
		fmt.Printf("关闭子节点[%d]长安链\n", j)
		Log += fmt.Sprintf("关闭子节点[%d]长安链\n", j)
		cmd := fmt.Sprintf("./scripts/binary-stop_scp%d.sh", j+1)
		result := command.CommandExec(cmd)
		// 通过返回，打印编译、配置结果
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
		if j == 0 {
			outresult1 = string(result)
		} else if j == 1 {
			outresult2 = string(result)
		} else if j == 2 {
			outresult3 = string(result)
		} else if j == 3 {
			outresult4 = string(result)
		}
	}

	return 0, string(outresult1) + string(outresult2) + string(outresult3) + string(outresult4)
}

func Chain_result() int {
	// 部署结果打印
	fmt.Printf("主节点: IP:%s, Port:%s \n", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
	Log += fmt.Sprintf("主节点: IP:%s, Port:%s \n", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
	var Node_cnt int
	Node_cnt, err := strconv.Atoi(Userconfig.Node_cnt)
	if err != nil {
		fmt.Println("Node_cnt异常,请检查")
	}
	if Userconfig.DeploymentType == 0 {
		for i := 0; i < Node_cnt; i++ {
			fmt.Printf("  部署文件:chainmaker-go/build/release/%s  区块链高度:%s\n", Userconfig.HostFile[i], Userconfig.Block_height)
			Log += fmt.Sprintf("  部署文件:chainmaker-go/build/release/%s  区块链高度:%s\n", Userconfig.HostFile[i], Userconfig.Block_height)
		}

		for i := 0; i < Userconfig.NodeNum; i++ {
			fmt.Printf("子节点[%d]: IP:%s, Port:%s 部署文件:%s  区块链高度:%s\n", i, Userconfig.Nodes[i].NodeIpAddr, Userconfig.Nodes[i].NodeP2PPort,
				Userconfig.Nodes[i].NodeFile, Userconfig.Block_height)
			Log += fmt.Sprintf("子节点[%d]: IP:%s, Port:%s 部署文件:%s  区块链高度:%s\n", i, Userconfig.Nodes[i].NodeIpAddr, Userconfig.Nodes[i].NodeP2PPort,
				Userconfig.Nodes[i].NodeFile, Userconfig.Block_height)
		}
	} else {
		for i := 0; i < Node_cnt; i++ {
			fmt.Printf("  部署文件:chainmaker-go/scripts/docker/multi_node/ 区块链高度:%s\n", Userconfig.Block_height)
			Log += fmt.Sprintf("  部署文件:chainmaker-go/scripts/docker/multi_node/ 区块链高度:%s\n", Userconfig.Block_height)
		}

		for i := 0; i < Userconfig.NodeNum; i++ {
			fmt.Printf("子节点[%d]: IP:%s, Port:%s 部署文件:chainmaker-go/scripts/docker/multi_node/  区块链高度:%s\n", i, Userconfig.Nodes[i].NodeIpAddr, Userconfig.Nodes[i].NodeP2PPort,
				Userconfig.Block_height)
			Log += fmt.Sprintf("子节点[%d]: IP:%s, Port:%s 部署文件:chainmaker-go/scripts/docker/multi_node/  区块链高度:%s\n", i, Userconfig.Nodes[i].NodeIpAddr, Userconfig.Nodes[i].NodeP2PPort,
				Userconfig.Block_height)
		}
	}
	return 0
}

//更换create_docker_compose_yml.sh和tpl_docker-compose_services.yml
func Docker_file_prepare() int {
	//将Docker-prepare下的文件拷贝到multi_node下
	cmd := fmt.Sprintf("\\cp -rf ./docker-prepare/create_docker_compose_yml.sh ./chainmaker-go/scripts/docker/multi_node/;\\cp -rf ./docker-prepare/tpl_docker-compose_services.yml ./chainmaker-go/scripts/docker/multi_node/")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
		return -1
	}
	cmd1 := fmt.Sprintf("\\cp -rf ./docker-prepare/create_solo_docker_compose_yml.sh ./chainmaker-go/scripts/docker/multi_node/;\\cp -rf ./docker-prepare/tpl_solo_docker-compose_services.yml ./chainmaker-go/scripts/docker/multi_node/")
	result1 := command.CommandExec(cmd1)
	if result1 == nil {
		fmt.Printf("ERROR to exec %s\n", cmd1)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd1)
		return -1
	}
	cmd2 := fmt.Sprintf("\\cp -rf ./docker-prepare/create_docker_compose_yml_pk.sh ./chainmaker-go/scripts/docker/multi_node/;\\cp -rf ./docker-prepare/tpl_docker-compose_services_pk.yml ./chainmaker-go/scripts/docker/multi_node/")
	result2 := command.CommandExec(cmd2)
	if result2 == nil {
		fmt.Printf("ERROR to exec %s\n", cmd2)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd2)
		return -1
	}
	cmd3 := fmt.Sprintf("\\cp -rf ./docker-prepare/create_solo_docker_compose_yml_pk.sh ./chainmaker-go/scripts/docker/multi_node/;\\cp -rf ./docker-prepare/tpl_solo_docker-compose_services_pk.yml ./chainmaker-go/scripts/docker/multi_node/")
	result3 := command.CommandExec(cmd3)
	if result3 == nil {
		fmt.Printf("ERROR to exec %s\n", cmd3)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd3)
		return -1
	}
	return 0
}

//docker多点部署时，将容器的data和log拷贝到主节点上
func Generate_shell_dockerMulti_data_log() int {
	// 创建文件夹存放data,log
	command.CommandExec("cd chainmaker-go/scripts/docker/multi_node;mkdir -p multi_data;mkdir -p multi_log")

	// 1 根据用户配置，生成脚本
	fieName := fmt.Sprintf("scripts/Docker_get_data_log.sh")

	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {
		cmd1 := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/multi_data;mkdir -p data%d ", j+1)
		result1 := command.CommandExec(cmd1)
		if result1 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd1)
			Log += fmt.Sprintf("ERROR to exec %s\n", cmd1)
			return -1
		}
		cmd2 := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/multi_log;mkdir -p log%d ", j+1)
		result2 := command.CommandExec(cmd2)
		if result2 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd2)
			Log += fmt.Sprintf("ERROR to exec %s\n", cmd2)
			return -1
		}

		newFieName := fmt.Sprintf("scripts/Docker_get_data_log%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		oldStr[3] = fmt.Sprintf("set nodenum")
		newStr[3] = fmt.Sprintf("set nodenum %d", j+1)

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}
	// 2 生成
	fmt.Printf("%s modify OK!\n", fieName)
	Log += fmt.Sprintf("%s modify OK!\n", fieName)
	command.CommandExec("chmod -R 777 ./chainmaker-go")
	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		cmd := fmt.Sprintf("./scripts/Docker_get_data_log%d.sh", j+1)
		command.CommandExec(cmd)
	}

	return 0
}

//命令行多机部署时，将各个子节点log拷贝到主节点上
func Generate_shell_cmdMulti_data_log() int {
	// 创建文件夹存放log
	command.CommandExec("cd chainmaker-go/build/release;mkdir -p multi_log")

	var oldStr [10]string
	var newStr [10]string
	if Userconfig.IdentityMode == 0 {
		// 1 根据用户配置，生成脚本
		fieName := fmt.Sprintf("scripts/multi_CmdGet_log.sh")
		for j := 0; j < Userconfig.NodeNum; j++ {

			cmd := fmt.Sprintf("cd chainmaker-go/build/release/multi_log;mkdir -p log%d ", j+1)
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			newFieName := fmt.Sprintf("scripts/multi_CmdGet_log%d.sh", j+1)

			// ssh登录使用的用户名
			oldStr[0] = fmt.Sprintf("set user")
			newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

			// ssh登录使用的密码
			oldStr[1] = fmt.Sprintf("set password")
			newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

			// ssh登录使用的IP
			oldStr[2] = fmt.Sprintf("set ip")
			newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

			oldStr[3] = fmt.Sprintf("set nodenum")
			newStr[3] = fmt.Sprintf("set nodenum %d", j+1)

			GenerateFile(fieName, newFieName, 5, oldStr, newStr)
		}
		// 2 生成
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
		command.CommandExec("chmod -R 777 ./chainmaker-go")
		for j := 0; j < Userconfig.NodeNum; j++ {
			// 执行部署脚本
			cmd := fmt.Sprintf("./scripts/multi_CmdGet_log%d.sh", j+1)
			command.CommandExec(cmd)
		}
	} else if Userconfig.IdentityMode == 1 {
		// 1 根据用户配置，生成脚本
		fieName := fmt.Sprintf("scripts/multi_CmdPkGet_log.sh")
		for j := 0; j < Userconfig.NodeNum; j++ {

			cmd := fmt.Sprintf("cd chainmaker-go/build/release/multi_log;mkdir -p Pklog%d ", j+1)
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
				return -1
			}

			newFieName := fmt.Sprintf("scripts/multi_CmdPkGet_log%d.sh", j+1)

			// ssh登录使用的用户名
			oldStr[0] = fmt.Sprintf("set user")
			newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

			// ssh登录使用的密码
			oldStr[1] = fmt.Sprintf("set password")
			newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

			// ssh登录使用的IP
			oldStr[2] = fmt.Sprintf("set ip")
			newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

			oldStr[3] = fmt.Sprintf("set nodenum")
			newStr[3] = fmt.Sprintf("set nodenum %d", j+1)

			GenerateFile(fieName, newFieName, 5, oldStr, newStr)
		}
		// 2 生成
		fmt.Printf("%s modify OK!\n", fieName)
		Log += fmt.Sprintf("%s modify OK!\n", fieName)
		command.CommandExec("chmod -R 777 ./chainmaker-go")
		for j := 0; j < Userconfig.NodeNum; j++ {
			// 执行部署脚本
			cmd := fmt.Sprintf("./scripts/multi_CmdPkGet_log%d.sh", j+1)
			command.CommandExec(cmd)
		}
	}

	return 0
}

func D_HardwarePrint(Hardware info.Hardwareinfo) (int, string) {
	st := Hardware.OsInfo
	return 0, st
}

//docker多机部署为各个子节点安装docker（不安装docker则无法加入swarm集群）
func Instal_docker_for_node() int {
	for i := 0; i < UserconfigJson.NodeNum; i++ {
		if UserconfigJson.DeploymentType == 1 {
			_, b := D_HardwarePrint(HardwareAll.NodeHardware[i])
			if strings.Contains(b, "Ubuntu") {
				cmd := fmt.Sprintf("./scripts/Node_Ubuntu_InstallDocker%d.sh", i+1)
				command.CommandExec(cmd)
			} else if strings.Contains(b, "CentOS") {
				cmd := fmt.Sprintf("./scripts/Node_Centos_InstallDocker%d.sh", i+1)
				command.CommandExec(cmd)
			} else if strings.Contains(b, "Kylin") {
				cmd := fmt.Sprintf("./scripts/Node_Kylin_InstallDocker%d.sh", i+1)
				command.CommandExec(cmd)
			}
		}
	}
	return 0
}

//docker多点部署时，查看节点是否启动正常
func Check_multi_process() int {
	fieName := fmt.Sprintf("scripts/multi_check_process.sh")
	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {

		newFieName := fmt.Sprintf("scripts/multi_check_process%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}
	command.CommandExec("chmod -R 777 ./chainmaker-go")
	fmt.Printf("查看节点启动使用是否正常...\n")
	Log += fmt.Sprintf("查看节点启动使用是否正常...\n")
	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		cmd := fmt.Sprintf("./scripts/multi_check_process%d.sh |grep ./chainmaker", j+1)
		result := command.CommandExec(cmd)
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
		// 通过返回，打印结果
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			return -1
		}
	}

	return 0
}

// docker多点部署查看端口是否监听
func Check_port_listening() int {
	fmt.Printf("查看端口是否监听...\n")
	Log += fmt.Sprintf("查看端口是否监听...\n")
	fieName := fmt.Sprintf("scripts/multi_check_portListen.sh")
	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {

		newFieName := fmt.Sprintf("scripts/multi_check_portListen%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}
	command.CommandExec("chmod -R 777 ./chainmaker-go")
	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		cmd := fmt.Sprintf("./scripts/multi_check_portListen%d.sh |grep LISTEN", j+1)
		result := command.CommandExec(cmd)
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
		// 通过返回，打印结果
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			return -1
		}
	}
	// 检查节点是否有ERROR日志
	if Userconfig.DeploymentType == 1 {
		cmd := string("cat chainmaker-go/scripts/docker/multi_node/multi_log/log*/log*/system.log |grep \"ERROR\\|put block\\|all necessary\"")
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR日志为空\n")
			Log += fmt.Sprintf("ERROR日志为空\n")
		}
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)

	} else if Userconfig.DeploymentType == 0 {
		if Userconfig.IdentityMode == 0 {
			cmd := string("cat chainmaker-go/build/release/multi_log/log*/log/system.log |grep \"ERROR\\|put block\\|all necessary\"")
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR日志为空\n")
				Log += fmt.Sprintf("ERROR日志为空\n")
			}
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)

		} else if Userconfig.IdentityMode == 1 {
			cmd := string("cat chainmaker-go/build/release/multi_log/Pklog*/log/system.log |grep \"ERROR\\|put block\\|all necessary\"")
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR日志为空\n")
				Log += fmt.Sprintf("ERROR日志为空\n")
			}
			fmt.Printf("%s\n", result)
			Log += fmt.Sprintf("%s\n", result)
		}
	}
	return 0
}

//docker多点部署stop长安链
func Multi_docker_chain_stop() (int, string, string, string, string, string) {
	var outresult1 string
	var outresult2 string
	var outresult3 string
	var outresult4 string
	cmd := string("docker stack rm chaimaker")
	result := command.CommandExec(cmd)
	// 通过返回，打印结果
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
		//return -1
	}
	fmt.Printf("%s\n", result)
	Log += fmt.Sprintf("%s\n", result)
	cmd1 := string("docker swarm leave --force")
	result1 := command.CommandExec(cmd1)
	// 通过返回，打印结果
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
		//return -1
	}
	fmt.Printf("%s\n", result1)
	Log += fmt.Sprintf("%s\n", result1)
	fieName := fmt.Sprintf("scripts/multi_docker_chain_stop.sh")
	var oldStr [10]string
	var newStr [10]string
	for j := 0; j < Userconfig.NodeNum; j++ {

		newFieName := fmt.Sprintf("scripts/multi_docker_chain_stop%d.sh", j+1)

		// ssh登录使用的用户名
		oldStr[0] = fmt.Sprintf("set user")
		newStr[0] = fmt.Sprintf("set user %s", Userconfig.Nodes[j].NodeUser)

		// ssh登录使用的密码
		oldStr[1] = fmt.Sprintf("set password")
		newStr[1] = fmt.Sprintf("set password %s", Userconfig.Nodes[j].NodePassword)

		// ssh登录使用的IP
		oldStr[2] = fmt.Sprintf("set ip")
		newStr[2] = fmt.Sprintf("set ip %s", Userconfig.Nodes[j].NodeIpAddr)

		oldStr[3] = fmt.Sprintf("set num")
		newStr[3] = fmt.Sprintf("set num %d", j+1)

		GenerateFile(fieName, newFieName, 5, oldStr, newStr)
	}
	command.CommandExec("chmod -R 777 ./chainmaker-go")
	for j := 0; j < Userconfig.NodeNum; j++ {
		// 执行部署脚本
		fmt.Printf("node%d离开集群...\n", j+1)
		Log += fmt.Sprintf("node%d离开集群...\n", j+1)
		cmd := fmt.Sprintf("./scripts/multi_docker_chain_stop%d.sh |grep left", j+1)
		result := command.CommandExec(cmd)
		fmt.Printf("%s\n", result)
		Log += fmt.Sprintf("%s\n", result)
		// 通过返回，打印结果
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
			//return -1
		}
		if j == 0 {
			outresult1 = string(result) + " node1"
		} else if j == 1 {
			outresult2 = string(result) + " node2"
		} else if j == 2 {
			outresult3 = string(result) + " node3"
		} else if j == 3 {
			outresult4 = string(result) + " node4"
		}
	}
	//删除历史文件，保证下次部署节点组网成功
	//多机部署关闭长安链后删除历史配置文件
	//cmd2 := fmt.Sprintf("cd chainmaker-go/;chmod -R 777 build/;cd build/;rm -rf config;rm -rf crypto-config;rm -rf crypto_config.yml;rm -rf pkcs11_keys.yml;rm -rf release")
	//result2 := command.CommandExec(cmd2)
	// 通过返回，打印编译、配置结果
	//fmt.Printf("%s\n", result2)
	//多机部署关闭长安链后备份历史配置文件，保证下次部署节点组网成功
	Chain_cert_backup()
	//docker部署完后备份config文件,保证下次不会报证书不匹配错误
	cmd2 := fmt.Sprintf("mkdir -p chainmaker-go/scripts/docker/multi_node/config_bak && mv chainmaker-go/scripts/docker/multi_node/config  chainmaker-go/scripts/docker/multi_node/config_bak/config_bak_$(date '+%%Y%%m%%d%%H%%M%%S')")
	command.CommandExec(cmd2)
	st := fmt.Sprintf("长安链已关闭")
	fmt.Printf("%s\n", st)
	Log += fmt.Sprintf("%s\n", st)
	return 0, outresult1, outresult2, outresult3, outresult4, st
}

//打包容器镜像
func Pack_docker_image() int {
	cmd := fmt.Sprintf("cd docker-prepare/ && mkdir -p docker_image && cd docker_image && docker save -o docker_image.tar chainmakerofficial/chainmaker && cd ../../")
	result := command.CommandExec(cmd)
	// 通过返回，打印结果
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		Log += fmt.Sprintf("ERROR to exec %s\n", cmd)
		return -1
	}
	return 0
}

//保证主节点上有docker image
func Check_host_docker_image() int {
	result := command.CommandExec("docker images")
	start := strings.Index(string(result), "chainmakerofficial/chainmaker")
	if start == -1 {
		//通过docker单点部署来拉取镜像
		command.CommandExec("chmod -R 777 ./chainmaker-go")
		//Chain_generate_shell_solo_docker()
		//Chain_setup_solo_docker()

		fieName := fmt.Sprintf("scripts/chainmaker-setup_solo_docker.sh")
		//ModifyFile(fieName, "set ip ", Userconfig.HostDockerIp, 0)
		ModifyFile(fieName, "P2P_PORT=", Userconfig.HostP2PPort, 0)
		ModifyFile(fieName, "RPC_PORT=", Userconfig.HostRPCPort, 0)
		ModifyFile(fieName, "NODE_COUNT=", Userconfig.Node_cnt, 0)
		//ModifyFile(fieName, "SERVER_NUM=", Userconfig.Server_node_cnt, 0)

		command.CommandExec("./scripts/chainmaker-setup_solo_docker.sh")

		command.CommandExec("cd chainmaker-go/scripts/docker/multi_node/ && docker-compose -f docker-compose1.yml up -d")

		k, err := strconv.Atoi(Userconfig.Node_cnt)
		_ = err
		for j := 0; j < k; j++ {
			// 断开docker network中的multinode_default
			cmd := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/ && docker network disconnect -f multinode_default cm-node%d;cd ../../../../", j+1)
			command.CommandExec(cmd)
		}
		for j := 0; j < k; j++ {
			// 执行部署脚本
			cmd1 := fmt.Sprintf("cd chainmaker-go/scripts/docker/multi_node/ && docker-compose -f docker-compose%d.yml down -v;cd ../../../../", j+1)
			command.CommandExec(cmd1)
		}

		Chain_cert_backup()

		cmd := fmt.Sprintf("mkdir -p chainmaker-go/scripts/docker/multi_node/config_bak && mv chainmaker-go/scripts/docker/multi_node/config  chainmaker-go/scripts/docker/multi_node/config_bak/config_bak_$(date '+%%Y%%m%%d%%H%%M%%S')")
		command.CommandExec(cmd)
		cmd1 := fmt.Sprintf("mkdir -p chainmaker-go/scripts/docker/multi_node/data_bak && mv chainmaker-go/scripts/docker/multi_node/data  chainmaker-go/scripts/docker/multi_node/data_bak/data_bak_$(date '+%%Y%%m%%d%%H%%M%%S')")
		command.CommandExec(cmd1)

	} else {
	}
	return 0
}

//修改docker容器内长安链的版本号和下载长安链的版本号一致
func ChangeVersion() int {
	cmd := fmt.Sprintf(`sed -i '/.*IMAGE=/cIMAGE="chainmakerofficial/chainmaker:%s"' ./docker-prepare/create_docker_compose_yml.sh`, Userconfig.ChainmakerVersion)
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}

	cmd1 := fmt.Sprintf(`sed -i '/.*IMAGE=/cIMAGE="chainmakerofficial/chainmaker:%s"' ./docker-prepare/create_docker_compose_yml_pk.sh`, Userconfig.ChainmakerVersion)
	result1 := command.CommandExec(cmd1)
	if result1 == nil {
		fmt.Printf("ERROR to exec %s\n", cmd1)
		return -1
	}

	cmd2 := fmt.Sprintf(`sed -i '/.*IMAGE=/cIMAGE="chainmakerofficial/chainmaker:%s"' ./docker-prepare/create_solo_docker_compose_yml.sh`, Userconfig.ChainmakerVersion)
	result2 := command.CommandExec(cmd2)
	if result2 == nil {
		fmt.Printf("ERROR to exec %s\n", cmd2)
		return -1
	}

	cmd3 := fmt.Sprintf(`sed -i '/.*IMAGE=/cIMAGE="chainmakerofficial/chainmaker:%s"' ./docker-prepare/create_solo_docker_compose_yml_pk.sh`, Userconfig.ChainmakerVersion)
	result3 := command.CommandExec(cmd3)
	if result3 == nil {
		fmt.Printf("ERROR to exec %s\n", cmd3)
		return -1
	}

	return 0
}

//下载区块链浏览器源码
func Chainmaker_explorer_download() int {
	// 扫描代码是否已经提前下载
	result := command.CommandExec("ls")
	start := strings.Index(string(result), "chainmaker-explorer")
	if start == -1 {
		fmt.Printf("开始下载chainmaker-explorer...\n")
		Log += fmt.Sprintf("开始下载chainmaker-explorer...\n")
		fmt.Printf("git clone -b v2.3.1 --depth=1 https://git.chainmaker.org.cn/chainmaker/chainmaker-explorer.git")
		Log += fmt.Sprintf("git clone -b v2.3.1 --depth=1 https://git.chainmaker.org.cn/chainmaker/chainmaker-explorer.git")
		cmd := fmt.Sprintf("git clone -b v2.3.1 --depth=1 https://git.chainmaker.org.cn/chainmaker/chainmaker-explorer.git")
		result := exec.Command("/bin/sh", "-c", cmd)
		out, err := result.Output()

		if err != nil {
			fmt.Printf("nil:%s\n", err)
			Log += fmt.Sprintf("nil:%s\n", err)
		}
		fmt.Printf("%s\n", out)
		Log += fmt.Sprintf("%s\n", out)

	} else {
		fmt.Printf("已存在chainmaker-explorer文件夹\n")
		Log += fmt.Sprintf("已存在chainmaker-explorer文件夹\n")
	}

	// 扫描代码是否已经下载完成
	result = command.CommandExec("ls")
	start = strings.Index(string(result), "chainmaker-explorer")
	if start == -1 {
		fmt.Printf("ERROR! chainmaker-explorer下载失败\n")
		Log += fmt.Sprintf("ERROR! chainmaker-explorer下载失败\n")
	} else {
		fmt.Printf("chainmaker-explorer已存在或下载完成\n")
		Log += fmt.Sprintf("chainmaker-explorer已存在或下载完成\n")
	}

	return 0
}

//启动docker（区块链浏览器）
func Chainmaker_explorer_dockerStart() int {
	cmd := string("cat /proc/cpuinfo |grep 'model name'|uniq")
	result := command.CommandExec(cmd)
	start := strings.Index(string(result), "ARM")
	if start == -1 {
		if Userconfig.Chainmaker_explorer == 1 {
			fmt.Printf("正在启动区块链浏览器...\n")
			Chainmaker_explorer_download()
			if Userconfig.Way_start_explore == 0 {
				cmd := fmt.Sprintf("cd chainmaker-explorer/docker && docker-compose -f docker-compose.yml up -d")
				result := command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					return -1
				}
			} else if Userconfig.Way_start_explore == 1 {
				cmd := fmt.Sprintf("cd chainmaker-explorer/docker && docker-compose -f docker-compose_evm.yml up -d")
				result := command.CommandExec(cmd)
				if result == nil {
					fmt.Printf("ERROR to exec %s\n", cmd)
					return -1
				}
			}
			//result := Userconfig.HostIpAddr
			//fmt.Printf("\n请打开浏览器输入网址:http://%s:9996\n", result)
		}
	}

	return 0

}

//停止docker（区块链浏览器）
func Chainmaker_explorer_dockerStop() int {
	if Userconfig.Chainmaker_explorer == 1 {
		if Userconfig.Way_start_explore == 0 {
			//常规停止
			cmd := fmt.Sprintf("cd chainmaker-explorer/docker && docker-compose -f docker-compose.yml down -v")
			result := command.CommandExec(cmd)
			fmt.Printf("%s\n", result)
			// 通过返回，打印结果
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				return -1
			}
		} else if Userconfig.Way_start_explore == 1 {
			//增强停止
			cmd := fmt.Sprintf("cd chainmaker-explorer/docker && docker-compose -f docker-compose_evm.yml down -v")
			result := command.CommandExec(cmd)
			fmt.Printf("%s\n", result)
			// 通过返回，打印结果
			if result == nil {
				fmt.Printf("ERROR to exec %s\n", cmd)
				return -1
			}
		}
		fmt.Printf("区块链浏览器已停止\n")
	}

	return 0
}

func Binary_result() int {
	// 二进制部署结果打印
	fmt.Printf("主节点: IP:%s, Port:%s \n", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
	Log += fmt.Sprintf("主节点: IP:%s, Port:%s \n", Userconfig.HostIpAddr, Userconfig.HostRPCPort)
	var Node_cnt int
	Node_cnt, err := strconv.Atoi(Userconfig.Node_cnt)
	if err != nil {
		fmt.Println("Node_cnt异常,请检查")
		Log += fmt.Sprintln("Node_cnt异常,请检查")
	}
	for i := 0; i < Node_cnt; i++ {
		fmt.Printf("  部署文件:binary/build/release/%s  区块链高度:%s\n", Userconfig.HostFile[i], Userconfig.Block_height)
		Log += fmt.Sprintf("  部署文件:binary/build/release/%s  区块链高度:%s\n", Userconfig.HostFile[i], Userconfig.Block_height)
	}

	for i := 0; i < Userconfig.NodeNum; i++ {
		fmt.Printf("子节点[%d]: IP:%s, Port:%s 部署文件:%s  区块链高度:%s\n", i, Userconfig.Nodes[i].NodeIpAddr, Userconfig.Nodes[i].NodeP2PPort, Userconfig.Nodes[i].NodeFile, Userconfig.Block_height)
		Log += fmt.Sprintf("子节点[%d]: IP:%s, Port:%s 部署文件:%s  区块链高度:%s\n", i, Userconfig.Nodes[i].NodeIpAddr, Userconfig.Nodes[i].NodeP2PPort, Userconfig.Nodes[i].NodeFile, Userconfig.Block_height)
	}

	return 0
}

//自动订阅区块链浏览器结构体
//证书模式参数结构体
type para struct {
	ChainId  string
	OrgId    string
	UserCert string
	UserKey  string
	NodeList []para1
	AuthType string
}

//pk模式参数结构体
type para2 struct {
	ChainId  string
	UserKey  string
	NodeList []para1
	AuthType string
	HashType int
}

type para1 struct {
	Addr        string
	OrgCa       string
	Tls         bool
	TLSHostName string
}

//区块链浏览器自动订阅
func Chainmaker_explorer_AotoSubscribe() int {
	var authtype string
	var addr string
	cmd := string("cat /proc/cpuinfo |grep 'model name'|uniq")
	result := command.CommandExec(cmd)
	start := strings.Index(string(result), "ARM")
	if start == -1 {
		if Userconfig.Chainmaker_explorer == 1 {
			fmt.Printf("正在订阅链...\n")
			Log += fmt.Sprintf("正在订阅链...\n")
			time.Sleep(time.Second * 20) //设置20秒延时，确保浏览器完全启动

			if Userconfig.DeploymentType == 0 {
				if Userconfig.IdentityMode == 0 {
					filename1 := "chainmaker-go/build/config/node1/certs/ca/wx-org1.chainmaker.org/ca.crt"
					fileContent1, _ := ioutil.ReadFile(filename1)
					filename2 := "chainmaker-go/build/config/node1/certs/user/admin1/admin1.sign.crt"
					fileContent2, _ := ioutil.ReadFile(filename2)
					filename3 := "chainmaker-go/build/config/node1/certs/user/admin1/admin1.sign.key"
					fileContent3, _ := ioutil.ReadFile(filename3)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					s1 := string(fileContent1)
					authtype = "permissionedwithcert"

					t := para{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: s1, Tls: true, TLSHostName: "chainmaker.org"})
					t.OrgId = "wx-org1.chainmaker.org"
					t.UserCert = string(fileContent2)
					t.UserKey = string(fileContent3)

					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)

				} else if Userconfig.IdentityMode == 1 {

					filename := "chainmaker-go/build/config/node1/user/client1/client1.key"
					fileContent, _ := ioutil.ReadFile(filename)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					authtype = "public"

					t := para2{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					if Userconfig.Hash_type == "SHA256" {
						t.HashType = 1 //pk模式非国密
					} else if Userconfig.Hash_type == "SM3" {
						t.HashType = 0 //pk模式国密
					}
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: "", Tls: false, TLSHostName: "chainmaker.org"})
					t.UserKey = string(fileContent)
					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
				}
			}

			if Userconfig.DeploymentType == 1 {
				if Userconfig.IdentityMode == 0 {
					filename1 := "chainmaker-go/build/config/node1/certs/ca/wx-org1.chainmaker.org/ca.crt"
					fileContent1, _ := ioutil.ReadFile(filename1)
					filename2 := "chainmaker-go/build/config/node1/certs/user/admin1/admin1.sign.crt"
					fileContent2, _ := ioutil.ReadFile(filename2)
					filename3 := "chainmaker-go/build/config/node1/certs/user/admin1/admin1.sign.key"
					fileContent3, _ := ioutil.ReadFile(filename3)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					s1 := string(fileContent1)
					authtype = "permissionedwithcert"

					t := para{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: s1, Tls: true, TLSHostName: "chainmaker.org"})
					t.OrgId = "wx-org1.chainmaker.org"
					t.UserCert = string(fileContent2)
					t.UserKey = string(fileContent3)

					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)

				} else if Userconfig.IdentityMode == 1 {

					filename := "chainmaker-go/build/config/node1/user/client1/client1.key"
					fileContent, _ := ioutil.ReadFile(filename)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					authtype = "public"

					t := para2{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					if Userconfig.Hash_type == "SHA256" {
						t.HashType = 1 //pk模式非国密
					} else if Userconfig.Hash_type == "SM3" {
						t.HashType = 0 //pk模式国密
					}
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: "", Tls: false, TLSHostName: "chainmaker.org"})
					t.UserKey = string(fileContent)
					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)

				}
			}

			if Userconfig.DeploymentType == 2 {
				if Userconfig.IdentityMode == 0 {
					filename1 := "binary/build/config/node1/certs/ca/wx-org1.chainmaker.org/ca.crt"
					fileContent1, _ := ioutil.ReadFile(filename1)
					filename2 := "binary/build/config/node1/certs/user/admin1/admin1.sign.crt"
					fileContent2, _ := ioutil.ReadFile(filename2)
					filename3 := "binary/build/config/node1/certs/user/admin1/admin1.sign.key"
					fileContent3, _ := ioutil.ReadFile(filename3)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					s1 := string(fileContent1)
					authtype = "permissionedwithcert"

					t := para{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: s1, Tls: true, TLSHostName: "chainmaker.org"})
					t.OrgId = "wx-org1.chainmaker.org"
					t.UserCert = string(fileContent2)
					t.UserKey = string(fileContent3)

					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)

				} else if Userconfig.IdentityMode == 1 {

					filename := "binary/build/config/node1/user/client1/client1.key"
					fileContent, _ := ioutil.ReadFile(filename)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					authtype = "public"

					t := para2{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					if Userconfig.Hash_type == "SHA256" {
						t.HashType = 1 //pk模式非国密
					} else if Userconfig.Hash_type == "SM3" {
						t.HashType = 0 //pk模式国密
					}
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: "", Tls: false, TLSHostName: "chainmaker.org"})
					t.UserKey = string(fileContent)
					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)

				}
			}

			if Userconfig.DeploymentType == 3 {
				if Userconfig.IdentityMode == 0 {
					filename1 := "binary/build/config/node1/certs/ca/wx-org1.chainmaker.org/ca.crt"
					fileContent1, _ := ioutil.ReadFile(filename1)
					filename2 := "binary/build/config/node1/certs/user/admin1/admin1.sign.crt"
					fileContent2, _ := ioutil.ReadFile(filename2)
					filename3 := "binary/build/config/node1/certs/user/admin1/admin1.sign.key"
					fileContent3, _ := ioutil.ReadFile(filename3)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					s1 := string(fileContent1)
					authtype = "permissionedwithcert"

					t := para{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: s1, Tls: true, TLSHostName: "chainmaker.org"})
					t.OrgId = "wx-org1.chainmaker.org"
					t.UserCert = string(fileContent2)
					t.UserKey = string(fileContent3)

					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)

				} else if Userconfig.IdentityMode == 1 {

					filename := "binary/build/config/node1/user/client1/client1.key"
					fileContent, _ := ioutil.ReadFile(filename)
					if Userconfig.NodeNum == 0 {
						addr = Userconfig.HostIpAddr + ":12301"
					} else {
						addr = Userconfig.Nodes[0].NodeIpAddr + ":12301"
					}
					authtype = "public"

					t := para2{}
					t.AuthType = string(authtype)
					t.ChainId = "chain1"
					if Userconfig.Hash_type == "SHA256" {
						t.HashType = 1 //pk模式非国密
					} else if Userconfig.Hash_type == "SM3" {
						t.HashType = 0 //pk模式国密
					}
					t.NodeList = append(t.NodeList, para1{Addr: addr, OrgCa: "", Tls: false, TLSHostName: "chainmaker.org"})
					t.UserKey = string(fileContent)
					url := "http://" + Userconfig.HostIpAddr + ":9996" + "/chainmaker/?cmb=SubscribeChain"
					payload, _ := json.Marshal(t)
					//fmt.Println("marsha result: ", string(payload))
					resp, _ := http.Post(url, "application/json;charset=UTF-8", bytes.NewReader(payload))
					body, _ := ioutil.ReadAll(resp.Body)
					fmt.Println(string(body))

					result := Userconfig.HostIpAddr
					fmt.Printf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)
					Log += fmt.Sprintf("\n请打开浏览器输入网址:\nhttp://%s:9996\n", result)

				}
			}

		}
	}
	return 0
}

//前端按下停止按钮停止长安链并展示停止停止信息
func Display_StopChainmaker() string {

	var outPrint string
	var outPrint1 string
	var outPrint2 string
	var outPrint3 string
	var outPrint4 string
	var outPrint5 string

	if Userconfig.DeploymentType == 0 {
		_, outPrint = Chain_stop()
		fmt.Printf("%s\n", outPrint)
		Log += fmt.Sprintf("%s\n", outPrint)
	}

	if Userconfig.DeploymentType == 1 {
		if Userconfig.NodeNum > 0 {
			_, outPrint1, outPrint2, outPrint3, outPrint4, outPrint5 = Multi_docker_chain_stop()
			outPrint = fmt.Sprintf("%s\n%s\n%s\n%s\n%s\n", outPrint1, outPrint2, outPrint3, outPrint4, outPrint5)
			fmt.Printf("%s\n", outPrint)
			Log += fmt.Sprintf("%s\n", outPrint)
		} else {
			_, outPrint = Chain_stop_solo_docker()
			fmt.Printf("%s\n", outPrint)
			Log += fmt.Sprintf("%s\n", outPrint)
		}
	}

	if Userconfig.DeploymentType > 1 {

		if Userconfig.NodeNum > 0 {
			Binary_generate_shell_scp_stop()
			_, outPrint = Binary_stop_scp()
			fmt.Printf("%s\n", outPrint)
			Log += fmt.Sprintf("%s\n", outPrint)
		} else {
			_, outPrint = Binary_stop()
			fmt.Printf("%s\n", outPrint)
			Log += fmt.Sprintf("%s\n", outPrint)
		}
	}
	//区块链浏览器停止功能
	Chainmaker_explorer_dockerStop()

	return string(outPrint)

}
