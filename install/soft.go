package install

import (
	"fmt"
	"project/command"
	"project/info"
	"project/scan"
	"strings"
)

//var FinshInitSoft int

var Hardware info.Hardwareinfo
var Ossoft info.Ossoftinfo
var OssoftMin info.OssoftinfoMin

// 升级安装所有软件依赖
func Test_initApp(Userconfig info.Userconfig_json, InitLog *string) int {
	// 需要拿到Ossoft的结构体
	Ossoft = scan.GetSoftware(Userconfig)
	OssoftMin = info.GetSoftMinFromIni(Userconfig.ChainmakerVersion)
	Hardware = scan.GetHardware(Userconfig)

	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		fmt.Printf("开始前请您手动设置apt源...\n")
		//initApt()
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		fmt.Printf("开始前请您手动设置yum源...\n")
		//initYum()
	}

	if strings.Contains(Hardware.OsInfo, "Kylin") {
		result := command.CommandExec("ls /etc/yum.repos.d/")
		start := strings.Index(string(result), "epel")
		if start == -1 {
			fmt.Printf("开始设置epel源...\n")
			cmd1 := fmt.Sprintf("wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm")
			result1 := command.CommandExec(cmd1)
			if result1 == nil {
				fmt.Printf("ERROR to exec %s\n", cmd1)
				return -1
			}
			cmd2 := fmt.Sprintf("rpm -ivh --nodeps epel-release-latest-7.noarch.rpm")
			result2 := command.CommandExec(cmd2)
			if result2 == nil {
				fmt.Printf("ERROR to exec %s\n", cmd2)
				return -1
			}
			cmd3 := fmt.Sprintf("rm -fr epel-release-latest-7.noarch.rpm")
			result3 := command.CommandExec(cmd3)
			if result3 == nil {
				fmt.Printf("ERROR to exec %s\n", cmd3)
				return -1
			}
		}
	}

	fmt.Printf("git version is %s\n", Ossoft.GitVersion)
	fmt.Printf("git version min is %s\n", OssoftMin.GitVersionMin)
	if Ossoft.GitFlag == 1 {
		fmt.Printf("	Git无需升级\n")
	} else {
		a := install_git(Userconfig)
		if a == -1 {
			return -1
		}
	}
	fmt.Printf("go version is %s\n", Ossoft.GoVersion)
	fmt.Printf("go version min is %s\n", OssoftMin.GoVersionMin)
	fmt.Printf("go version max is %s\n", OssoftMin.GoVersionMax)
	if Ossoft.GoFlag == 1 {
		fmt.Printf("	Golang无需升级\n")
	} else {
		a := install_go(Userconfig)
		if a == -1 {
			return -1
		}
	}

	fmt.Printf("gcc version is %s\n", Ossoft.GccVersion)
	fmt.Printf("gcc version min is %s\n", OssoftMin.GccVersionMin)
	if Ossoft.GccFlag == 1 {
		fmt.Printf("	Gcc无需升级\n")
	} else {
		a := install_gcc(Userconfig)
		if a == -1 {
			return -1
		}
	}

	fmt.Printf("Glibc version is %s\n", Ossoft.GlibcVersion)
	fmt.Printf("Glibc version min is %s\n", OssoftMin.GlibcVersionMin)
	if Ossoft.GlibcFlag == 1 {
		fmt.Printf("	Glibc无需升级\n")
	} else {
		fmt.Println("\033[1;31;40mWARNING, Glibc version too old\033[0m")
		//return -1
	}
	//扫描tree是否安装，若未安装则安装
	install_tree(Userconfig)
	//扫描expect是否安装，若未安装则安装
	Install_Expect(Userconfig)
	//扫描7z是否安装，若未安装则安装
	install_SevenZ(Userconfig)
	//安装make,缺乏make无法编译
	Install_Make(Userconfig)
	//根据配置文件判断安装docker和docker-compose
	Install_dockerAcompose(Userconfig)
	if Userconfig.CopyType == 1 {
		fmt.Printf("开始安装/配置ftp(CopyType=ftp方式)...\n")
		install_ftp()
	} else {
		fmt.Printf("无需安装/配置ftp(CopyType=scp方式)\n")
	}

	// 一些裸机设备可能需要安装的app
	// install_tmux()

	// 安装App完成后，再扫描一次
	//ScanHostSoftware()
	//FinshInitSoft = 1
	return 0
}

func Install_dockerAcompose(Userconfig info.Userconfig_json) int {
	var a bool
	var b bool
	if Userconfig.DeploymentType == 1 {
		a = true
	} else {
		a = false
	}
	if Userconfig.Chainmaker_explorer == 1 {
		b = true
	} else {
		b = false
	}
	if a || b {
		fmt.Printf("Docker version is %s\n", Ossoft.DockerVersion)
		fmt.Printf("Docker version min is %s\n", OssoftMin.DockerVersionMin)
		if Ossoft.DockerFlag == 1 {
			fmt.Printf("	Docker无需升级\n")
		} else {
			a := install_docker(Userconfig)
			if a == -1 {
				return -1
			}
		}

		fmt.Printf("DockerCompose version is %s\n", Ossoft.DockerComposeVersion)
		fmt.Printf("DockerCompose version min is %s\n", OssoftMin.DockerComposeVersionMin)
		if Ossoft.DockerFlag == 1 {
			fmt.Printf("	DockerCompose无需升级\n")
		} else {
			a := Installdocker_compose(Userconfig)
			if a == -1 {
				return -1
			}
		}
	}
	return 0
}

func Binary_intapp(Userconfig info.Userconfig_json, InitLog *string) int {
	Ossoft = scan.GetSoftware_binary(Userconfig)
	Hardware = scan.GetHardware(Userconfig)

	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		fmt.Printf("开始前请您手动设置apt源...\n")
		//initApt()
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		fmt.Printf("开始前请您手动设置yum源...\n")
		//initYum()
	}

	if strings.Contains(Hardware.OsInfo, "Kylin") {
		fmt.Printf("开始设置epel源...\n")
		cmd1 := fmt.Sprintf("wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm")
		result1 := command.CommandExec(cmd1)
		if result1 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd1)
			return -1
		}
		cmd2 := fmt.Sprintf("rpm -ivh --nodeps epel-release-latest-7.noarch.rpm")
		result2 := command.CommandExec(cmd2)
		if result2 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd2)
			return -1
		}
		cmd3 := fmt.Sprintf("rm -fr epel-release-latest-7.noarch.rpm")
		result3 := command.CommandExec(cmd3)
		if result3 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd3)
			return -1
		}
	}

	//安装expect
	Install_Expect(Userconfig)
	//安装7z
	install_SevenZ(Userconfig)

	return 0
}

// 设置apt源
func initApt() int {
	//fmt.Printf("%s\n", GlobalInfo.OsInfo)
	cmd := string("sudo ./scripts/set_apt_source_1804.sh")
	fmt.Printf("开始设置apt源...\n")
	result := command.CommandExec(cmd)
	// 通过返回，判断设置结果
	// fmt.Printf("%s\n", result)
	start := strings.Index(string(result), "所有软件包均为最新")
	if start == -1 {
		fmt.Printf("ERROR to 设置apt源\n")
		return -1
	} else {
		fmt.Printf("设置apt源完成\n")
	}

	return 0
}

// 设置yum源
func initYum() int {
	//fmt.Printf("%s\n", GlobalInfo.OsInfo)
	cmd := string("sudo ./scripts/set_yum_source_Centos7.sh")
	// fmt.Printf("开始设置yum源...\n")
	result := command.CommandExec(cmd)
	// 通过返回，判断设置结果
	// fmt.Printf("%s\n", result)
	start := strings.Index(string(result), "元数据缓存已建立")
	if start == -1 {
		fmt.Printf("ERROR to 设置yum源\n")
		return -1
	} else {
		fmt.Printf("设置yum源完成\n")
	}

	return 0
}

func install_git(Userconfig info.Userconfig_json) int {
	fmt.Printf("开始安装Git...\n")
	// 安装Git

	var cmd string
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install git", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install git", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "Kylin") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install git", Userconfig.HostRootPassword)
	}
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过返回，判断设置结果
	fmt.Printf("%s\n", result)

	return 0
}

func install_gcc(Userconfig info.Userconfig_json) int {
	fmt.Printf("开始安装Gcc...\n")
	// 安装Gcc

	var cmd string
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install gcc", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install gcc", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "Kylin") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install gcc", Userconfig.HostRootPassword)
	}
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过返回，判断设置结果
	fmt.Printf("%s\n", result)

	return 0
}

/*
func install_glibc(Userconfig info.Userconfig_json) int {
	fmt.Printf("开始安装glibc...\n")
	// 安装glibc

	var cmd string
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install glibc-source", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install glibc", Userconfig.HostRootPassword)
	}
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过返回，判断设置结果
	fmt.Printf("%s\n", result)

	return 0
}
*/

func install_go(Userconfig info.Userconfig_json) int {
	fmt.Printf("开始安装Go...\n")
	// 安装Go

	var cmd string
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install go", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install go", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "Kylin") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install go", Userconfig.HostRootPassword)
	}

	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过返回，判断设置结果
	fmt.Printf("%s\n", result)

	return 0
}

func install_docker(Userconfig info.Userconfig_json) int {
	fmt.Printf("开始安装docker...\n")
	// 安装Docker
	//var cmd string
	/*	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		cmd1 := fmt.Sprintf("curl -fsSL https://test.docker.com -o test-docker.sh")
		result1 := command.CommandExec(cmd1)
		if result1 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd1)
			return -1
		}
		// 通过返回，判断设置结果
		fmt.Printf("%s\n", result1)

		cmd2 := fmt.Sprintf("echo %s | sudo sh test-docker.sh", Userconfig.HostRootPassword)
		result2 := command.CommandExec(cmd2)
		if result2 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd2)
			return -1
		}
		// 通过返回，判断设置结果
		fmt.Printf("%s\n", result2)
	*/
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {

		cmd1 := fmt.Sprintf("for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done")
		result1 := command.CommandExec(cmd1)
		if result1 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd1)
			return -1
		}

		cmd2 := fmt.Sprintf("echo %s | sudo -S apt-get update", Userconfig.HostRootPassword)
		result2 := command.CommandExec(cmd2)
		if result2 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd2)
			return -1
		}

		cmd3 := fmt.Sprintf("echo %s | sudo -S apt-get install -y ca-certificates curl gnupg", Userconfig.HostRootPassword)
		result3 := command.CommandExec(cmd3)
		if result3 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd3)
			return -1
		}

		cmd4 := fmt.Sprintf("echo %s | sudo -S install -m 0755 -d /etc/apt/keyrings", Userconfig.HostRootPassword)
		result4 := command.CommandExec(cmd4)
		if result4 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd4)
			return -1
		}

		cmd5 := fmt.Sprintf("curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg")
		result5 := command.CommandExec(cmd5)
		if result5 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd5)
			return -1
		}

		cmd6 := fmt.Sprintf("echo %s | sudo -S chmod a+r /etc/apt/keyrings/docker.gpg", Userconfig.HostRootPassword)
		result6 := command.CommandExec(cmd6)
		if result6 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd6)
			return -1
		}

		cmd7 := fmt.Sprintf(`echo \
			"deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
			"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
			sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`)
		result7 := command.CommandExec(cmd7)
		if result7 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd7)
			return -1
		}
		fmt.Printf("Docker已安装\n")

	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		//cmd = fmt.Sprintf("echo %s | sudo -S yum -y install docker", Userconfig.HostRootPassword)
		/*		cmd3 := fmt.Sprintf("curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun")
				result3 := command.CommandExec(cmd3)
				if result3 == nil {
					fmt.Printf("ERROR to exec %s\n", cmd3)
					return -1
				}
		*/

		cmd8 := fmt.Sprintf("echo %s | sudo -S yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine", Userconfig.HostRootPassword)
		result8 := command.CommandExec(cmd8)
		if result8 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd8)
			return -1
		}

		cmd9 := fmt.Sprintf("echo %s | sudo -S yum install -y yum-utils", Userconfig.HostRootPassword)
		result9 := command.CommandExec(cmd9)
		if result9 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd9)
			return -1
		}

		cmd10 := fmt.Sprintf("echo %s | sudo -S yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo", Userconfig.HostRootPassword)
		result10 := command.CommandExec(cmd10)
		if result10 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd10)
			return -1
		}

		cmd11 := fmt.Sprintf("echo %s | sudo -S yum install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin", Userconfig.HostRootPassword)
		result11 := command.CommandExec(cmd11)
		if result11 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd11)
			return -1
		}
		fmt.Printf("Docker已安装\n")
	} else if strings.Contains(Hardware.OsInfo, "Kylin") {
		cmd := fmt.Sprintf("echo %s | sudo -S yum -y install docker", Userconfig.HostRootPassword)
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}
	}
	return 0
}

func install_tree(Userconfig info.Userconfig_json) int {
	//安装tree
	var cmd string
	result := command.CommandExec("tree --version")
	start := strings.Index(string(result), "tree")
	if start == 0 {
		//fmt.Printf("tree已安装\n")
	} else {
		fmt.Printf("开始安装Tree\n")
		if strings.Contains(Hardware.OsInfo, "Ubuntu") {
			cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install tree", Userconfig.HostRootPassword)
		} else if strings.Contains(Hardware.OsInfo, "CentOS") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install tree", Userconfig.HostRootPassword)
		} else if strings.Contains(Hardware.OsInfo, "Kylin") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install tree", Userconfig.HostRootPassword)
		}
		command.CommandExec(cmd)
		fmt.Printf("tree已安装\n")
	}
	return 0
}

func install_SevenZ(Userconfig info.Userconfig_json) int {
	//安装7z
	var cmd string
	x := command.CommandExec("7za --help")
	start1 := strings.Index(string(x), "p7zip")
	if start1 == -1 {
		fmt.Printf("开始安装7z...\n")
		if strings.Contains(Hardware.OsInfo, "Ubuntu") {
			cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install p7zip-full", Userconfig.HostRootPassword)
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to install_7z\n")
				return -1
			} else {
				fmt.Printf("p7zip安装完成\n")
			}
		} else if strings.Contains(Hardware.OsInfo, "CentOS") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install p7zip", Userconfig.HostRootPassword)
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to install_7z\n")
				return -1
			} else {
				fmt.Printf("p7zip安装完成\n")
			}
		} else if strings.Contains(Hardware.OsInfo, "Kylin") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install p7zip", Userconfig.HostRootPassword)
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to install_7z\n")
				return -1
			} else {
				fmt.Printf("p7zip安装完成\n")
			}
		}

	}
	return 0
}

func Install_Make(Userconfig info.Userconfig_json) int {
	//安装make
	var cmd string
	x := command.CommandExec("make -v")
	start1 := strings.Index(string(x), "Make")
	if start1 == -1 {
		fmt.Printf("开始安装make...\n")
		if strings.Contains(Hardware.OsInfo, "Ubuntu") {
			cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install make", Userconfig.HostRootPassword)
			result := command.CommandExec(cmd)
			if result == nil {
				fmt.Printf("ERROR to install_make\n")
				return -1
			} else {
				fmt.Printf("make安装完成\n")
			}
		} else if strings.Contains(Hardware.OsInfo, "CentOS") {

		} else if strings.Contains(Hardware.OsInfo, "Kylin") {

		}

	}

	return 0
}

func Installdocker_compose(Userconfig info.Userconfig_json) int {
	//扫描代码是否已经安装docker-compose
	//var cmd string
	//result := command.CommandExec("docker-compose --version")
	//start := strings.Index(string(result), "docker-compose version")
	//if start == -1 {
	//fmt.Printf("开始下载docker-compose...\n")
	//command.CommandExec("wget https://labfile.oss.aliyuncs.com/courses/980/software/docker-compose-Linux-x86_64")
	/*	command.CommandExec("curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose")
		cmd = fmt.Sprintf("chmod +x /usr/local/bin/docker-compose")
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to install_docker-compose\n")
			return -1
		}
		//} else {
		// 通过返回，判断设置结果
		fmt.Printf("%s\n", result)
		//}
	*/ //(长安链官网安装，由于是国外资源下载不稳定，故在此不做应用)
	fmt.Printf("开始安装docker-compose...\n")
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {

		cmd1 := fmt.Sprintf("echo %s | sudo -S rm /var/cache/apt/archives/lock", Userconfig.HostRootPassword)
		result1 := command.CommandExec(cmd1)
		if result1 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd1)
			return -1
		}

		cmd2 := fmt.Sprintf("echo %s | sudo -S rm /var/lib/dpkg/lock-frontend", Userconfig.HostRootPassword)
		result2 := command.CommandExec(cmd2)
		if result2 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd2)
			return -1
		}
		//cmd1和cmd2防止无法无法获得锁 /var/lib/dpkg/lock-frontend导致安装失败

		cmd3 := fmt.Sprintf("echo %s | sudo -S apt-get update", Userconfig.HostRootPassword)
		result3 := command.CommandExec(cmd3)
		if result3 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd3)
			return -1
		}

		cmd4 := fmt.Sprintf("echo %s | sudo -S apt-get -y install docker-compose", Userconfig.HostRootPassword)
		result4 := command.CommandExec(cmd4)
		if result4 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd4)
			return -1
		}

		fmt.Printf("Docker-compose已安装\n")

	} else if strings.Contains(Hardware.OsInfo, "CentOS") {

		cmd5 := fmt.Sprintf("echo %s | sudo -S yum update", Userconfig.HostRootPassword)
		result5 := command.CommandExec(cmd5)
		if result5 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd5)
			return -1
		}

		cmd6 := fmt.Sprintf("echo %s | sudo -S yum install -y docker-compose-plugin", Userconfig.HostRootPassword)
		result6 := command.CommandExec(cmd6)
		if result6 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd6)
			return -1
		}
		//解除权限，以可以查看版本号
		cmd7 := fmt.Sprintf("chmod -R 777 /usr/ocal/bin/docker-compose")
		result7 := command.CommandExec(cmd7)
		if result7 == nil {
			fmt.Printf("ERROR to exec %s\n", cmd7)
			return -1
		}

	} else if strings.Contains(Hardware.OsInfo, "Kylin") {
		cmd := fmt.Sprintf("echo %s | sudo -S yum install -y docker-compose", Userconfig.HostRootPassword)
		result := command.CommandExec(cmd)
		if result == nil {
			fmt.Printf("ERROR to exec %s\n", cmd)
			return -1
		}
	}
	return 0
}

func install_tmux(Userconfig info.Userconfig_json) int {
	fmt.Printf("开始安装tmux...\n")
	// 安装mux
	var cmd string
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install tmux", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install tmux", Userconfig.HostRootPassword)
	} else if strings.Contains(Hardware.OsInfo, "Kylin") {
		cmd = fmt.Sprintf("echo %s | sudo -S yum -y install tmux", Userconfig.HostRootPassword)
	}
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过返回，判断设置结果
	fmt.Printf("%s\n", result)

	return 0
}

func install_ftp() int {
	// 安装ftp

	var cmd string
	if strings.Contains(Hardware.OsInfo, "Ubuntu") {
		cmd = string("sed -i '/.*set cmd*/cset cmd apt-get' scripts/install_ftp.sh")
		command.CommandExec(cmd)
	} else if strings.Contains(Hardware.OsInfo, "CentOS") {
		cmd = string("sed -i '/.*set cmd*/cset cmd yum' scripts/install_ftp.sh")
		command.CommandExec(cmd)
	} else if strings.Contains(Hardware.OsInfo, "Kylin") {
		cmd = string("sed -i '/.*set cmd*/cset cmd yum' scripts/install_ftp.sh")
		command.CommandExec(cmd)
	}

	cmd = fmt.Sprintf("sed -i '/.*set file_path*/cset file_path %s' scripts/install_ftp.sh", Userconfig.FtpPath)
	command.CommandExec(cmd)

	cmd = fmt.Sprintf("sed -i '/.*set ftp_name*/cset ftp_name %s' scripts/install_ftp.sh", Userconfig.FtpUserName)
	command.CommandExec(cmd)

	cmd = string("sudo ./scripts/install_ftp.sh")
	result := command.CommandExec(cmd)
	if result == nil {
		fmt.Printf("ERROR to exec %s\n", cmd)
		return -1
	}
	// 通过返回，判断设置结果
	fmt.Printf("%s\n", result)

	return 0
}

func Install_Expect(Userconfig info.Userconfig_json) int {
	// 安装Expect

	var cmd string
	y := command.CommandExec("whereis tcl")
	start := strings.Index(string(y), "tcl")
	if start == -1 {
		if strings.Contains(Hardware.OsInfo, "Ubuntu") {
			cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install tcl", Userconfig.HostRootPassword)
		} else if strings.Contains(Hardware.OsInfo, "CentOS") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install tcl", Userconfig.HostRootPassword)
		} else if strings.Contains(Hardware.OsInfo, "Kylin") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install tcl", Userconfig.HostRootPassword)
		}
		command.CommandExec(cmd)
	}
	x := command.CommandExec("expect")
	start1 := strings.Index(string(x), "expect")
	if start1 == -1 {
		if strings.Contains(Hardware.OsInfo, "Ubuntu") {
			cmd = fmt.Sprintf("echo %s | sudo -S apt-get -y install expect", Userconfig.HostRootPassword)
		} else if strings.Contains(Hardware.OsInfo, "CentOS") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install expect", Userconfig.HostRootPassword)
		} else if strings.Contains(Hardware.OsInfo, "Kylin") {
			cmd = fmt.Sprintf("echo %s | sudo -S yum -y install expect", Userconfig.HostRootPassword)
		}
		command.CommandExec(cmd)
	}
	return 0

}
