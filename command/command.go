package command

import (
	"fmt"
	"io/ioutil"
	"os/exec"
)

func CommandExec(command string) []byte {
	//需要执行命令： command
	cmd := exec.Command("/bin/bash", "-c", command)
	// 获取输入
	output, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Println("无法获取命令的标准输出管道", err.Error())
		return nil
	}
	// 执行Linux命令
	if err := cmd.Start(); err != nil {
		fmt.Println("命令执行失败，请检查命令输入是否有误", err.Error())
		return nil
	}
	// 读取输出
	result, err := ioutil.ReadAll(output)
	if err != nil {
		fmt.Println("打印异常，请检查")
		return nil
	}
	if err := cmd.Wait(); err != nil {
		fmt.Println("Wait", err.Error())
		return nil
	}
	return result
}
