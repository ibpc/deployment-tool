package main

import (
	"fmt"
	"net/http"
	"project/display"
	"project/info"
	"project/install"
	"project/scan"

	"github.com/gin-gonic/gin"
	"github.com/thinkerou/favicon"
	"strconv"
)

var Hardware info.Hardwareinfo
var Software info.Ossoftinfo
var HardwareMin info.HardwareinfoMin
var SoftwareMin info.OssoftinfoMin
var Userconfig info.Userconfig_json
var StateSecret_PostHeader info.StateSecret_PostHeader
var NodeDetail_PostHeader info.NodeDetail_PostHeader
var NodeSelect_PostHeader info.NodeSelect_PostHeader
var InitMessage info.InitMessage
var InitLog string

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		// if origin != "" {
		c.Header("Access-Control-Allow-Origin", "*") // 可将将 * 替换为指定的域名
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")
		// }
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}
func main() {
	//Default返回一个默认的路由引擎

	Userconfig = info.GetUserconfigFromIni()
	fmt.Printf("[test]GetUserconfigFromIni: %+v\n", Userconfig)
	install.SetUserConfig(Userconfig)

	r := gin.Default()
	r.Use(Cors())

	r.Use(favicon.New("./chainmakerLOGO.png"))

	r.POST("/user/add", func(context *gin.Context) {
		username := context.PostForm("username")
		password := context.PostForm("password")
		if username == "wangjin" {
			if password == "wangjin" {
				token := "11"
				context.JSON(200, gin.H{
					"code": 200,
					"data": gin.H{"token": token},
					"msg":  "登录成功",
				})
			} else {
				context.JSON(200, gin.H{
					"msg": "密码错误",
				})
			}
		} else {
			context.JSON(200, gin.H{
				"msg": "用户名错误",
			})
		}
	})

	r.POST("/nodeDetail", func(context *gin.Context) {
		// ip := context.PostForm("ip")
		// fmt.Printf("[IO_test]nodeDetail.ip: %s\n", ip)
		if err := context.BindJSON(&NodeDetail_PostHeader); err != nil {
			fmt.Printf("[test]BindJSON err: %s\n", err)
		}

		fmt.Printf("[test]NodeDetail_PostHeader: %+v\n", NodeDetail_PostHeader)

		// json := make(map[string]interface{})
		// context.BindJSON(&json)
		str := NodeDetail_PostHeader.Type
		Userconfig.Development_type = str
		if NodeDetail_PostHeader.Type == "multi" {
			for i := 0; i < 16; i++ {
				if NodeDetail_PostHeader.Nodes[i].IpAddr != "" {
					Userconfig.NodeNum = i + 1
				}
			}
			fmt.Printf("[test]Type=multi NodeNum=%d\n", Userconfig.NodeNum)
			for i := 0; i < Userconfig.NodeNum; i++ {
				Userconfig.Nodes[i].NodeIpAddr1 = NodeDetail_PostHeader.Nodes[i].IpAddr
				Userconfig.Nodes[i].NodeP2PPort1 = NodeDetail_PostHeader.Nodes[i].NodeP2PPort
				Userconfig.Nodes[i].NodeRPCPort1 = NodeDetail_PostHeader.Nodes[i].NodeRPCPort
				Userconfig.Nodes[i].NodeUser1 = NodeDetail_PostHeader.Nodes[i].User
				Userconfig.Nodes[i].NodePassword1 = NodeDetail_PostHeader.Nodes[i].Password
			}
		} else {
			Userconfig.NodeNum = 0
			Userconfig.HostIpAddr = NodeDetail_PostHeader.Host.IpAddr
			Userconfig.HostUser = NodeDetail_PostHeader.Host.User
			Userconfig.HostRootPassword = NodeDetail_PostHeader.Host.Password
			Userconfig.HostP2PPort = NodeDetail_PostHeader.Host.NodeP2PPort
			Userconfig.HostRPCPort = NodeDetail_PostHeader.Host.NodeRPCPort
		}

		install.SetUserConfig(Userconfig)

		fmt.Printf("[test]Userconfig: %+v\n", Userconfig)
	})

	r.POST("/nodeSelect", func(context *gin.Context) {
		// ip := context.PostForm("ip")
		// fmt.Printf("[IO_test]nodeDetail.ip: %s\n", ip)
		if err := context.BindJSON(&NodeSelect_PostHeader); err != nil {
			fmt.Printf("[test]BindJSON err: %s\n", err)
		}

		fmt.Printf("[test]NodeSelect_PostHeader: %+v\n", NodeSelect_PostHeader)

		// json := make(map[string]interface{})
		// context.BindJSON(&json)
		for i := 0; i < 16; i++ {
			if NodeSelect_PostHeader.Nodes[i].IpAddr != "" {
				Userconfig.NodeNum = i + 1
			}
		}

		fmt.Printf("[test]NodeNum=%d\n", Userconfig.NodeNum)
		for i := 0; i < Userconfig.NodeNum; i++ {
			j, err := strconv.Atoi(NodeSelect_PostHeader.Nodes[i].Id)
			_ = err
			Userconfig.Nodes[i].NodeIpAddr = Userconfig.Nodes[j].NodeIpAddr1
			Userconfig.Nodes[i].NodeP2PPort = Userconfig.Nodes[j].NodeP2PPort1
			Userconfig.Nodes[i].NodeRPCPort = Userconfig.Nodes[j].NodeRPCPort1
			Userconfig.Nodes[i].NodeUser = Userconfig.Nodes[j].NodeUser1
			Userconfig.Nodes[i].NodePassword = Userconfig.Nodes[j].NodePassword1
		}

		install.SetUserConfig(Userconfig)

		fmt.Printf("[test]Userconfig: %+v\n", Userconfig)
	})

	r.POST("/stateSecret", func(context *gin.Context) {
		if err := context.BindJSON(&StateSecret_PostHeader); err != nil {
			fmt.Printf("[test]BindJSON err: %s\n", err)
		}
		Userconfig = StateSecret_PostHeader.Config
		Userconfig.Chainmaker_explorer = StateSecret_PostHeader.Config.Chainmaker_explorer
		// 将部分字段解析成Int
		// value, err := strconv.Atoi(Userconfig.DeploymentTypeStr)
		// if err == nil {
		// 	Userconfig.DeploymentType = value
		// }
		// value, err = strconv.Atoi(Userconfig.CopyTypeStr)
		// if err == nil {
		// 	Userconfig.CopyType = value
		// }
		// value, err = strconv.Atoi(Userconfig.IdentityModeStr)
		// if err == nil {
		// 	Userconfig.IdentityMode = value
		// }
		// value, err = strconv.Atoi(Userconfig.RecommendedConfigLevelStr)
		// if err == nil {
		// 	Userconfig.RecommendedConfigLevel = value
		// }
		/*value, err = strconv.Atoi(Userconfig.Chainmaker_explorer )
		if err == nil {
			Userconfig.RecommendedConfigLevel = value
		}
		value, err = strconv.Atoi(Userconfig.Way_start_explore )
		if err == nil {
			Userconfig.RecommendedConfigLevel = value
		}*/
		install.SetUserConfig(Userconfig)

		fmt.Printf("[test]Userconfig: %+v\n", Userconfig)
	})

	// r.POST("/stateSecret", func(context *gin.Context) {
	// 	json := make(map[string]interface{})
	// 	context.BindJSON(&json)

	// 	json1 := json["form"].(map[string]interface{})
	// 	Userconfig = info.GetUserconfigFromMap(json1)

	// 	fmt.Printf("[test]Userconfig: %+v\n", Userconfig)
	// })

	r.GET("/hardware", func(c *gin.Context) {
		hardware := scan.HardwareReturn(Userconfig)
		fmt.Printf("[/hardware]GET: %+v\n", hardware)
		//输出json结果给调用方
		c.JSON(200, gin.H{
			"message": hardware,
			//a
		})
	})
	r.GET("/GetUserconfig", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": Userconfig,
		})
	})
	r.GET("/installsoft", func(c *gin.Context) {
		a := install.Test_initApp(Userconfig, &InitLog)

		if a == 0 {
			c.JSON(200, gin.H{
				"message": "长安链软件依赖更新完毕，可以开始安装长安链",
				//a
			})
		} else {
			c.JSON(200, gin.H{
				"message": "软件依赖更新失败",
				//a
			})
		}
	})
	r.GET("/installchanganlian", func(c *gin.Context) {

		install.Test_initChangAnLian(&InitMessage)

		// c.JSON(200, gin.H{
		// 	"message": InitMessage,
		// 	//a
		// })

	})
	r.GET("/query_install_log", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": InitMessage,
		})
	})
	r.GET("/soft", func(c *gin.Context) {
		soft := scan.OssoftReturn(Userconfig)
		fmt.Printf("[/soft]GET: %+v\n", soft)
		//输出json结果给调用方
		c.JSON(200, gin.H{
			"message": soft,
			//a
		})
	})
	r.GET("/panic", func(c *gin.Context) {
		//输出json结果给调用方
		message := display.Display_panic()
		c.JSON(200, gin.H{
			"message": message,
			//a
		})
	})
	r.GET("/system", func(c *gin.Context) {
		//输出json结果给调用方
		message := display.Display_system()
		c.JSON(200, gin.H{
			"message": message,
			//a
		})
	})
	r.GET("/systemErr", func(c *gin.Context) {
		//输出json结果给调用方
		message := display.Display_systemErr()
		c.JSON(200, gin.H{
			"message": message,
			//a
		})
	})
	r.GET("/StopChainmaker", func(c *gin.Context) {
		//输出json结果给调用方
		message := install.Display_StopChainmaker()
		c.JSON(200, gin.H{
			"message": message,
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}
