BINARY_NAME=main-cmd

build:
	go build -o ${BINARY_NAME} main-cmd.go

run:
	go build -o ${BINARY_NAME} main-cmd.go
	./${BINARY_NAME}

clean:
	go clean
	rm ${BINARY_NAME}